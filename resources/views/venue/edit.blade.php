@extends('layouts.app')
@section('title')
Edit Venue
@endsection
@section('styles')
<style>
.form-group.required .control-label:after { content:"*"; color:red; }
/* Always set the map height explicitly to define the size of the div
</style>​
<link rel="stylesheet" type="text/css" href="">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ asset('css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.fileupload-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('https://www.tinymce.com/css/codepen.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"> 
@endsection
@section('content')
<div class="container ">
  <div class="row">
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok"></span>
      <em> {!! session('flash_message') !!}</em>
    </div>
    @endif
  </div>
</div>
{{--image set the bachground start--}}
<div id="listvenue-image" class="listvenue-image-margin2">
        <div class="container">
            <h2 class="logheading">Edit Your Venue</h2>
        </div>

    </div>
<div class="container">
  <div class="row">
    @if ($errors->any())
    <ul class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
    @endif
    <section id="main-container" class="container inner">
        <div class="row">
            <div id="main-content" class="main-content col-xs-12 col-lg-12 col-md-12">
                <div id="primary" class="content-area">
                    <div id="content" class="site-content" role="main">
                        <article id="post-10509" class="post-10509 page type-page status-publish hentry">
                            <div class="entry-content-page">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="place-submission-form metabox-fields-front space-padding-lr-40 space-padding-tb-30">

                                        {!! Form::model($venue, [
                                            'method' => 'POST',
                                            'url' => ['/venue', $venue->id],
                                            'class' => 'form-horizontal',
                                            'files' => true,
                                            'id' => 'my-form'
                                        ]) !!}
                                        <h1><i class="fas fa-edit"></i>  Edit Your Venue</h1>
                                        <div class="form-group required {{ $errors->has('name') ? ' has-error' : ''}}">
                                            {!! Form::label('name', 'Venue Name', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                {!! Form::text('name', $venue->name, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Venue Name']) !!}
                                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group required{{ $errors->has('max_capacity') ? ' has-error' : ''}}">
                                            {!! Form::label('max_capacity', 'Maximum Capacity', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                {!! Form::number('max_capacity', $venue->max_capacity, ['class' => 'form-control', 'required' => 'required','placeholder'=>'Maximum Capacity']) !!}
                                                {!! $errors->first('max_capacity', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6 required {{ $errors->has('mob_number') ? 'has-error' : ''}}">
                                                {!! Form::label('mob_number', 'Mobile Number', ['class' => 'col-md-4 form-align control-label']) !!}
                                                <div class="col-md-6">
                                                    {!! Form::number('mob_number', $venue->mob_number, ['class' => 'form-control', 'required' => 'required','placeholder'=>'Mobile Number']) !!}
                                                    {!! $errors->first('mob_number', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-5 {{ $errors->has('phone_number') ? 'has-error' : ''}}">
                                                {!! Form::label('phone_number', 'Phone Number', ['class' => 'col-md-4 control-label']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::number('phone_number', $venue->phone_number, ['class' => 'form-control', 'required' => 'required','placeholder'=>'Phone Number']) !!}
                                                    {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group required {{ $errors->has('short_desc') ? 'has-error' : ''}}">
                                            {!! Form::label('short_desc', 'Short Description', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                {!! Form::textarea('short_desc', $venue->short_desc,['class' => 'form-control tinymce-editor-short-desc', 'required' => 'required','placeholder'=>'Short Description', 'maxlength' => '255']) !!}
                                                {!! $errors->first('short_desc', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div> 
                                        <div class="row"></div>
                                        <div class="form-group required {{ $errors->has('long_desc') ? 'has-error' : ''}}">
                                            {!! Form::label('long_desc', 'Long Description', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                {!! Form::textarea('long_desc', $venue->long_desc,['class' => 'form-control tinymce-editor-long-desc', 'required' => 'required','placeholder'=>'Long Description', 'maxlength' => '2000']) !!}
                                                {!! $errors->first('long_desc', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('Images') ? 'has-error' : ''}}">
                                            {!! Form::label('Images', 'Images(can add more than one)', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                    <!--upload form-->
                                                <label class="btn btn-default btn-file upload_button">
                                                    <span>Add</span>
                                                    <input type="file" name="files[]" multiple class="hide" id="fileupload" >
                                                    <input type="hidden" name="image_name" id="file_ids" value="" />
                                                </label>
                                                <!--upload form-->

                                                <!--progress-->
                                                <div class="progress file_progress" style="margin-top: 10px; height: 5px;">
                                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                                         style="width: 0%;">
                                                    </div>
                                                </div>
                                                <!--/progress-->

                                                <!--file uploaded-->
                                                <ul class="list-group uploaded_files" style="margin-top: 10px;">
                                                </ul>
                                                <!--/file uploaded-->
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('venue_video') ? 'has-error' : ''}}">
                                            {!! Form::label('venue_video', 'Video', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                @if(!empty($venue->venueVideo->video_name))
                                                {!! Form::text('venue_video', 'https://www.youtube.com/watch?v='.$venue->venueVideo->video_name, ['class' => 'form-control maxwidthdropdown', 'required' => 'required','placeholder'=>'Input link for videos from Youtube.']) !!}
                                                @else
                                                {!! Form::text('venue_video', null, ['class' => 'form-control maxwidthdropdown', 'required' => 'required','placeholder'=>'Input link for videos from Youtube.']) !!}
                                                @endif
                                                {!! $errors->first('venue_video', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>

                                        <h3><i class="fa fa-bars"></i>  Packages Option (At Least One)</h3>
                                        <input type="button" class="btn btn-default" id="add" value="Add More">
                                        <div class="hidden">
                                            <div class="row row-margin" id="package-hidden">
                                                <div class="form-group required col-md-5 {{ $errors->has('package_name') ? 'has-error' : ''}}">
                                                {!! Form::label('package_name', 'Package Name', ['class' => 'col-md-4 control-label form-align']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('package_name[]', null, ['class' => 'form-control ', 'required' => 'required','placeholder'=>'Package Name']) !!}
                                                        {!! $errors->first('package_name', '<p class="help-block">:message</p>') !!}
                                                    </div>
                                                </div>
                                                <div class="form-group required col-md-5 {{ $errors->has('cost_per_person') ? 'has-error' : ''}}">
                                                    {!! Form::label('cost_per_person', 'Per Person Cost', ['class' => 'col-md-4 control-label']) !!}
                                                    <div class="col-md-5">
                                                        {!! Form::number('cost_per_person[]', null, ['class' => 'form-control', 'required' => 'required','placeholder'=>'Per Person Cost']) !!}
                                                        {!! $errors->first('cost_per_person', '<p class="help-block">:message</p>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row package-element">
                                            @foreach($venue->venuePackages as $venuePackage)
                                            <div class="row element" id="div_{{$venuePackage->id}}">
                                                <div class="row row-margin" id="package">
                                                    <div class="form-group required col-md-5 {{ $errors->has('package_name') ? 'has-error' : ''}}">
                                                        {!! Form::label('package_name', 'Package Name', ['class' => 'col-md-4 control-label form-align']) !!}
                                                        <div class="col-md-7">
                                                            {!! Form::text('package_name[]',$venuePackage->package_name , ['class' => 'form-control ', 'required' => 'required','placeholder'=>'Package Name']) !!}
                                                            {!! $errors->first('package_name', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group required col-md-5 {{ $errors->has('cost_per_person') ? 'has-error' : ''}}">
                                                        {!! Form::label('cost_per_person', 'Per Person Cost', ['class' => 'col-md-4 control-label']) !!}
                                                        <div class="col-md-5">
                                                            {!! Form::number('cost_per_person[]',$venuePackage->cost_per_person , ['class' => 'form-control', 'required' => 'required','placeholder'=>'Per Person Cost']) !!}
                                                            {!! $errors->first('cost_per_person', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                    </div>
                                                    <span id="remove_{{ $venuePackage->id }}" class="remove btn btn-default col-md-1">X</span>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <h3><i class="fas fa-search-plus"></i>  Choose Applicable Options</h3>
                                        <div class="form-group {{ $errors->has('Categories') ? 'has-error' : ''}}">
                                            {!! Form::label('categories', 'Categories', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                @foreach ($venueCategories as $venueCategory)
                                                <div class="col-md-12">
                                                    {!! Form::label ($venueCategory->name, $venueCategory->name,
                                                    ['class' => 'labelcategory-inline text-capitalize']) !!}
                                                    <div class="form-group">
                                                        @foreach ($venueCategory->venueSubCategories as $venueSubCategory)
                                                            @php
                                                                $venueSubCategoriesId[] =  null;
                                                            @endphp
                                                            @foreach($venue->venueSubCategories as $subCategory)
                                                               @php
                                                                 $venueSubCategoriesId[] =  $subCategory->id;
                                                               @endphp
                                                            @endforeach
                                                            @if(in_array($venueSubCategory->id, $venueSubCategoriesId))
                                                            <label>
                                                                <input type="checkbox" class="jquerycheckbox" value="{{$venueSubCategory->id}}" name='venue_sub_categories[]' checked="checked">
                                                                {{$venueSubCategory->name}}
                                                            </label>
                                                            @else
                                                            <label>
                                                                <input type="checkbox" class="jquerycheckbox" value="{{$venueSubCategory->id}}" name='venue_sub_categories[]'>
                                                                {{$venueSubCategory->name}}
                                                            </label>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <!-- ================= Google map ===========  -->
                                        <h3><i class="fa fa-map-marker" aria-hidden="true"></i>  Address</h3>
                                        <div class="form-group {{ $errors->has('address') ? ' has-error' : ''}}">
                                            {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                {!! Form::textarea('address', $venue->venueAddress->address,['class' => 'form-control textarea-style', 'required' => 'required','size' => '30x3','placeholder'=>'Address']) !!}
                                                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <h3><i class="fa fa-location-arrow" aria-hidden="true"></i>  Map Address</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                  <input class='form-control' id='search_address' placeholder='Enter an address or an intersection' type='text'/>
                                                </p>
                                                <p>
                                                  <label>
                                                    within
                                                    <select id='search_radius'>
                                                      <option value='400'>2 blocks</option>
                                                      <option value='805'>1/2 mile</option>
                                                      <option value='1610'>1 mile</option>
                                                      <option value='3220'>2 miles</option>
                                                    </select>
                                                  </label>
                                                  <a class='btn btn-default' id='reset' href='#'>
                                                  <i class='glyphicon glyphicon-repeat'></i>
                                                  Reset
                                                </a>
                                                </p>
                                                <div class="row">
                                                     <div class="form-group col-md-6">
                                                        {!! Form::label('state', 'State', ['class' => 'col-md-6 control-label']) !!}
                                                        <div class="col-md-6">
                                                            <input class="form-control" id="administrative_area_level_1" type="text" name="state" readonly value="{{ $venue->venueAddress->state }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {!! Form::label('city', 'City', ['class' => 'col-md-6 control-label']) !!}
                                                        <div class="col-md-6">
                                                            <input class="form-control" id="locality" type="text" type="text" name="city" readonly value="{{ $venue->venueAddress->city }}">
                                                        </div>
                                                    </div>          
                                                </div>
                                                <div class="row"> 
                                                    <div class="form-group col-md-6">
                                                        {!! Form::label('area', 'Area', ['class' => 'col-md-6 control-label']) !!}
                                                        <div class="col-md-6">
                                                            <input class="form-control" id="sublocality_level_1" type="text" name="area" readonly value="{{ $venue->venueAddress->area }}">
                                                             <select class="form-control" id="PincodesId" name="Area"></select>
                                                        </div>
                                                    </div>       
                                                    <div class="form-group col-md-6">
                                                        {!! Form::label('street_number', 'Street No.', ['class' => 'col-md-6 control-label']) !!}
                                                        <div class="col-md-6">
                                                            <input class="form-control" id="street_number" type="text" name="street_number" value="{{ $venue->venueAddress->street_number }}"> 
                                                        </div>
                                                    </div>
                                                </div>        
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        {!! Form::label('postal_code', 'Pin Code', ['class' => 'col-md-6 control-label']) !!}
                                                        <div class="col-md-6">
                                                            <input class="form-control" id="postal_code" type="text" name="postal_code" readonly value="{{ $venue->venueAddress->postal_code }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        {!! Form::label('country', 'Country', ['class' => 'col-md-6 control-label']) !!}
                                                        <div class="col-md-6">
                                                            <input class="form-control" id="country" type="text" name="country" readonly value="{{ $venue->venueAddress->country }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        {!! Form::label('latitude', 'Latitude', ['class' => 'col-md-6 control-label']) !!}
                                                        <div class="col-md-6">
                                                            <input class='form-control' id='lat' type="text" name="latitude" readonly value="{{ $venue->venueAddress->latitude }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">           
                                                        {!! Form::label('longitude', 'Longitude', ['class' => 'col-md-6 control-label']) !!}
                                                        <div class="col-md-6">
                                                            <input class='form-control' id='long' type="text" name="longitude" readonly value="{{ $venue->venueAddress->longitude }}">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="map-address" id='map_canvas'></div>      
                                            </div>             
                                        </div> 

                                        <!-- ================= Google map ===========  -->

                                        <h3><span class="fa fa-building"></span>  Occasion It can Host</h3>

                                        <div class="form-group {{ $errors->has('Categories') ? 'has-error' : ''}}">
                                            {!! Form::label('Categories', 'Categories', ['class' => 'col-md-2 control-label']) !!}
                                            <div class="col-md-8">
                                                @foreach ($occasions as $occasion)
                                                <div class="col-md-12">
                                                    {!! Form::label($occasion->name, $occasion->name,['class' => '']) !!}
                                                    <div class="form-group">
                                                        @foreach ($occasion->occasions as $occasionSubCategory)
                                                            @php
                                                                 $occasionSubCategoriesId[] = null;
                                                            @endphp
                                                            @foreach($venue->occasions as $occasion)
                                                                @php
                                                                    $occasionSubCategoriesId[] = $occasion->id; 
                                                                @endphp
                                                            @endforeach
                                                            @if(in_array($occasionSubCategory->id, $occasionSubCategoriesId))
                                                            <label>
                                                                <input type="checkbox" class="jquerycheckbox" value="{{$occasionSubCategory->id}}" name='occasions[]' checked="checked">
                                                                {{$occasionSubCategory->name}}
                                                            </label>
                                                            @else
                                                            <label>
                                                                <input type="checkbox" class="jquerycheckbox" value="{{$occasionSubCategory->id}}" name='occasions[]'>
                                                                {{$occasionSubCategory->name}}
                                                            </label>
                                                            @endif                                
                                                        @endforeach
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <h3><i class="fas fa-wine-glass"></i>  Choose Venue Features </h3>
                                        <div class="col-md-row">
                                            <div class="form-group {{ $errors->has('venue_features') ? 'has-error' : ''}}">
                                                {!! Form::label('venue_features', 'Venue Features', ['class' => 'col-md-2 control-label']) !!}
                                                <div class="col-md-8">
                                                    @foreach($venueFeatures as $venueFeature)
                                                        @php
                                                            $venueFeaturesId[] = null;
                                                        @endphp
                                                        @foreach($venue->venueFeatures as $feature)
                                                            @php
                                                                $venueFeaturesId[] = $feature->id;
                                                            @endphp
                                                        @endforeach
                                                        @if(in_array($venueFeature->id, $venueFeaturesId))
                                                        <label>
                                                            <input type="checkbox" class="jquerycheckbox" value="{{$venueFeature->id}}" name='venue_features[]' checked="checked">
                                                            {{$venueFeature->name}}
                                                        </label>
                                                        @else
                                                        <label>
                                                            <input type="checkbox" class="jquerycheckbox" value="{{$venueFeature->id}}" name='venue_features[]'>
                                                            {{$venueFeature->name}}
                                                        </label>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <h3><i class="fas fa-tags"></i>  Choose Tags</h3>
                                        <div class="col-md-row">
                                            <div class="form-group {{ $errors->has('tags') ? 'has-error' : ''}}">
                                                {!! Form::label('tags', 'Tags', ['class' => 'col-md-2 control-label']) !!}
                                                <div class="col-md-8">
                                                    @foreach($tags as $tag)
                                                        @php
                                                            $tagsId[] = null;
                                                        @endphp
                                                        @foreach($venue->venueTags as $venueTag)
                                                            @php
                                                                $tagsId[] = $venueTag->id;
                                                            @endphp
                                                        @endforeach
                                                        @if(in_array($tag->id, $tagsId))
                                                        <label>
                                                            <input type="checkbox" class="jquerycheckbox" value="{{$tag->id}}" name='tags[]' checked="checked">
                                                            {{$tag->name}}
                                                        </label>
                                                        @else
                                                        <label>
                                                            <input type="checkbox" class="jquerycheckbox" value="{{$tag->id}}" name='tags[]'>
                                                            {{$tag->name}}
                                                        </label>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <h3><i class="fas fa-credit-card"></i>  Choose Payment Option</h3>
                                        <div class="col-md-row">
                                            <div class="form-group {{ $errors->has('PaymentOption') ? 'has-error' : ''}}">
                                                {!! Form::label('payment_option', 'Payment Options', ['class' => 'col-md-2 control-label']) !!}
                                                <div class="col-md-8">
                                                    @foreach($paymentOptions as $paymentOption)
                                                        @php
                                                            $paymentOptionsId[] = null;
                                                        @endphp
                                                        @foreach($venue->venuePaymentOptions as $venuePaymentOption)
                                                            @php
                                                                $paymentOptionsId[] = $venuePaymentOption->id;
                                                            @endphp
                                                        @endforeach
                                                        @if(in_array($paymentOption->id, $paymentOptionsId))
                                                        <label>
                                                            <input type="checkbox" class="jquerycheckbox" value="{{$paymentOption->id}}" name='payment_option[]' checked="checked">
                                                            {{$paymentOption->name}}
                                                        </label>
                                                        @else
                                                        <label>
                                                            <input type="checkbox" class="jquerycheckbox" value="{{$paymentOption->id}}" name='payment_option[]'>
                                                            {{$paymentOption->name}}
                                                        </label>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="fa fa-star"></span>  Choose Ameneties</h3>
                                        <div class="col-md-row">
                                            <div class="form-group {{ $errors->has('VenueFacility') ? 'has-error' : ''}}">
                                                {!! Form::label('amenity', 'Amenities', ['class' => 'col-md-2 control-label']) !!}
                                                <div class="col-md-8">
                                                    @foreach($venueAmenities as $venueAmenity)
                                                        @php
                                                            $venueAmenitiesId[] = null;
                                                        @endphp
                                                        @foreach($venue->venueAmenities as $amenity)
                                                            @php
                                                                $venueAmenitiesId[] = $amenity->id;
                                                            @endphp
                                                        @endforeach
                                                        @if(in_array($venueAmenity->id, $venueAmenitiesId))
                                                        <label>
                                                            <input type="checkbox" class="jquerycheckbox" value="{{$venueAmenity->id}}" name='amenities[]' checked="checked">
                                                            {{$venueAmenity->name}}
                                                        </label>
                                                        @else
                                                        <label>
                                                            <input type="checkbox" class="jquerycheckbox" value="{{$venueAmenity->id}}" name='amenities[]'>
                                                            {{$venueAmenity->name}}
                                                        </label>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update Venue',
                                                ['class' => 'btn btn-primarycreate  btn-default']) !!}
                                            </div>
                                        </div>
                                        {!! Form::close() !!}

                                    </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
  </div>
</div>
@endsection
@section('scripts')

<!--=============== pincode Ajax call ================-->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script> 
  <script type="text/javascript">
     $(function () {
      $('#PincodesId').hide();
     });
  </script>
<!--=============== //pincode Ajax call ================-->

<!--============ Text Editor Js =============-->
<script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
<script type="text/javascript">
  tinymce.init({
  selector: '.tinymce-editor-short-desc',
  height: 150,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'],
  max_chars: 255, // max. allowed chars
    setup: function (ed) {
        var allowedKeys = [8, 37, 38, 39, 40, 46]; // backspace, delete and cursor keys
        ed.on('keydown', function (e) {
            if (allowedKeys.indexOf(e.keyCode) != -1) return true;
            if (tinymce_getContentLength() + 1 > this.settings.max_chars) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            return true;
        });
        ed.on('keyup', function (e) {
            tinymce_updateCharCounter(this, tinymce_getContentLength());
        });
    },
    init_instance_callback: function () { // initialize counter div
        $('#' + this.id).prev().append('<div class="char_count" style="text-align:right"></div>');
        tinymce_updateCharCounter(this, tinymce_getContentLength());
    },
    paste_preprocess: function (plugin, args) {
        var editor = tinymce.get(tinymce.activeEditor.id);
        var len = editor.contentDocument.body.innerText.length;
        var text = $(args.content).text();
        if (len + text.length > editor.settings.max_chars) {
            alert('Pasting this exceeds the maximum allowed number of ' + editor.settings.max_chars + ' characters.');
            args.content = '';
        } else {
            tinymce_updateCharCounter(editor, len + text.length);
        }
    }
});
tinymce.init({
  selector: '.tinymce-editor-long-desc',
  height: 150,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'],
  max_chars: 2000, // max. allowed chars
    setup: function (ed) {
        var allowedKeys = [8, 37, 38, 39, 40, 46]; // backspace, delete and cursor keys
        ed.on('keydown', function (e) {
            if (allowedKeys.indexOf(e.keyCode) != -1) return true;
            if (tinymce_getContentLength() + 1 > this.settings.max_chars) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            return true;
        });
        ed.on('keyup', function (e) {
            tinymce_updateCharCounter(this, tinymce_getContentLength());
        });
    },
    init_instance_callback: function () { // initialize counter div
        $('#' + this.id).prev().append('<div class="char_count" style="text-align:right"></div>');
        tinymce_updateCharCounter(this, tinymce_getContentLength());
    },
    paste_preprocess: function (plugin, args) {
        var editor = tinymce.get(tinymce.activeEditor.id);
        var len = editor.contentDocument.body.innerText.length;
        var text = $(args.content).text();
        if (len + text.length > editor.settings.max_chars) {
            alert('Pasting this exceeds the maximum allowed number of ' + editor.settings.max_chars + ' characters.');
            args.content = '';
        } else {
            tinymce_updateCharCounter(editor, len + text.length);
        }
    }
});

function tinymce_updateCharCounter(el, len) {
    $('#' + el.id).prev().find('.char_count').text(len + '/' + el.settings.max_chars);
}

function tinymce_getContentLength() {
    return tinymce.get(tinymce.activeEditor.id).contentDocument.body.innerText.length;
}
</script>
<!--============ //Text Editor Js =============-->

<!--=============== checkbox script =============-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
$( ".jquerycheckbox" ).checkboxradio();
} );
</script>
<!--=============== //checkbox script =============-->

<!--=============== google map script ============-->
<script type="text/javascript" src="{{ asset('js/jquery.address.js') }}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyAjYIJDSpRo90YUDZNtLnSCTmuMHfLMAlo"></script>
<script type="text/javascript" src="{{ asset('js/maps_lib.js') }}"></script>
<script type='text/javascript'>
//<![CDATA[
$(window).resize(function () {
  var h = $(window).height(),
  offsetTop = 220; // Calculate the top offset

  $('#map_canvas').css('height', (h - offsetTop));
  }).resize();

  $(function() {
    var myMap = new MapsLib({
      fusionTableId:      "1m4Ez9xyTGfY2CU6O-UgEcPzlS0rnzLU93e4Faa0",
      googleApiKey:       "AIzaSyAjYIJDSpRo90YUDZNtLnSCTmuMHfLMAlo",
      locationColumn:     "geometry",
      map_center:         [{{$venue->venueAddress->latitude}}, {{$venue->venueAddress->longitude}}],
      defaultZoom:        8,
      addrMarkerImage:    "{{ asset('images/spotlight-poi2.png') }}",
      locationScope:      ""

    });
    document.getElementById('search_address').value = "";
    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('search_address'));
    autocomplete.setComponentRestrictions(
                {'country': 'in'});
    google.maps.event.addListener(autocomplete, 'place_changed',function(){
      myMap.doSearch();
      var place = autocomplete.getPlace();
      var address = place.address_components;
      var street, city,country, state, zip;
      document.getElementById('street_number').value = "";
      document.getElementById('sublocality_level_1').value = "";
      document.getElementById('locality').value = "";
      document.getElementById('administrative_area_level_1').value = "";
      document.getElementById('postal_code').value = "";
      document.getElementById('country').value = "";
      document.getElementById('lat').value = "";
      document.getElementById('long').value = ""; 

      var latitude = place.geometry.location.lat(); // latitude of location
      document.getElementById('lat').value = latitude;
      var longitude = place.geometry.location.lng(); // longitude of location
      document.getElementById('long').value = longitude; 
      address.forEach(function(component) {
        var types = component.types;
          if (types.indexOf('street_number') > -1) 
            {
              street = component.long_name;
              document.getElementById('street_number').value = street;
            }
          if (types.indexOf('sublocality_level_1') > -1) 
            {
              area = component.long_name;
              document.getElementById('sublocality_level_1').value = area;
            }
          if (types.indexOf('locality') > -1) 
            {
              city = component.long_name;
              document.getElementById('locality').value = city;
            }
          if (types.indexOf('administrative_area_level_1') > -1) 
            {
              state = component.long_name;
              document.getElementById('administrative_area_level_1').value = state;
            }
          if (types.indexOf('postal_code') > -1) 
            {
              zip = component.long_name;
              document.getElementById('postal_code').value = zip;
            }
          if (types.indexOf('country') > -1) 
            {
              country = component.long_name;
              document.getElementById('country').value = country;
            }
      });
    $(function () {
     var value = $('#sublocality_level_1').val();
     if(value == '')
      {
        $('#sublocality_level_1').hide();
         $('#PincodesId').show();
         $('#PincodesId').select2({
          placeholder: 'Search area..',
          ajax: {
              url: '{{ route('pincode.getPincodes') }}',
              dataType: 'json',
              data: function (params) {
                  return {
                      q: $.trim(params.term)
                  };
              },
              processResults: function (data) {
                  return {
                      results: data
                  };
              },
              cache: true
          }
        });
     }
     else
         $('#PincodesId').hide(); 
      
  });
  });

  $('#search_radius').change(function(){
    myMap.doSearch();
  });

  $('#reset').click(function(){
    myMap.reset();
    return false;
  });
});
//]]>
</script>
<!--=============== //google map script ============-->

<!--=============== File Upload Script =============-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.iframe-transport/1.0.1/jquery.iframe-transport.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.17.0/js/jquery.fileupload.min.js"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fileupload-image.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fileupload-process.js') }}"></script>
<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script>
    $(function () {
        var filList;
        $('#fileupload').fileupload({
            url: "{{\URL::route('files.imageUpload')}}",
            dataType: 'json',
            add: function (e, data) {
                var filename = data.files[0].name;
                filenameID = filename.replace(/[^a-z0-9\s]/gi, '').replace(/[_.\s]/g, '-');

                if ($.inArray(filename, filList) !== -1) {
                    alert("Filename already exist");
                    return false;
                }

                filList = [filename];

                //on click to upload
                $('.file_progress').show();

                data.context = $('.upload_button').find('span').text('Uploading...');

                var uploadResponse = data.submit()
                    .error(function (uploadResponse, textStatus, errorThrown) {
                        alert("Error: " + textStatus + " | " + errorThrown);
                        return false;
                    });

            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress-bar').css('width', progress + '%');
            },
            done: function (e, data)
            {
                e.preventDefault();

                var filename = data.files[0].name;
                var filenameID = filename.replace(/[^a-z0-9\s]/gi, '').replace(/[_.\s]/g, '-');

                var file_list = "";
                var link = '';

                $.each(data.result.files, function (index, file)
                {
                    link = '<a target="_blank" href="'+file.url+'">'+file.name+'</a>';
                    file_list += '<li class="list-group-item"><button class="btn btn-xs pull-right btn-danger remove_file">X</button> '+link+' </li>';
                    if ($('#file_ids').val() != '') {
                    $('#file_ids').val($('#file_ids').val() + ',');
                    }
                    $('#file_ids').val($('#file_ids').val() + file.name);
                });

                $(".uploaded_files").append(file_list);

                $('.file_progress').hide();
                $('.progress-bar').css('width', '0%');
                $('.upload_button').find('span').text('Add');
            },
        });

        $('.uploaded_files').on("click", ".remove_file", function () {
            $(this).closest("li").remove();
            return false;
        });
    });
</script>
<!--=============== //File Upload Script =============-->
<script type="text/javascript">
$(document).ready(function(){
  $(".element span:first").remove();
 // Add new element
 $("#add").click(function(){
  var input = $("#package-hidden").html();
  // Finding total number of elements added
  var total_element = $(".element").length;
 
  // last <div> with element class id
  var lastid = $(".element:last").attr("id");
  var split_id = lastid.split("_");
  var nextindex = Number(split_id[1]) + 1;

  var max = 5;
  // Check total number elements
  if(total_element < max ){
   // Adding new div container after last occurance of element class
   $(".element:last").after("<div class='row element' id='div_"+ nextindex +"'></div>");
 
   // Adding element to <div>
   $("#div_" + nextindex).append(input+"<span id='remove_" + nextindex + "' class='remove btn btn-default col-md-1'>X</span>");
 
  }
 });

 // Remove element
 $('.package-element').on('click','.remove',function(){
 
  var id = this.id;
  var split_id = id.split("_");
  var deleteindex = split_id[1];

  // Remove <div> with id
  $("#div_" + deleteindex).remove();

 }); 
});
</script>

<!--=============== Form Validation Script =============-->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js', '#my-form')}}"></script>
{!! $validator !!}
<!--=============== //Form Validation Script =============-->
@endsection
