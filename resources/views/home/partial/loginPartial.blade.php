<ul class="list-inline pull-left">
    <li>
        <a href="{{ url('/login') }}" class="opal-user-login">
            <span class="fa fa-user"></span> Login</a>
    </li>
    <li>
        <a href="{{ url('/register') }}" class="opal-user-register">
            <span class="fa fa-pencil"></span> Register</a>
    </li>

</ul>

