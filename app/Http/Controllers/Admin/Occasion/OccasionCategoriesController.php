<?php

namespace App\Http\Controllers\Admin\Occasion;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\OccasionCategory;
use Illuminate\Http\Request;

class OccasionCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $occasioncategories = OccasionCategory::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $occasioncategories = OccasionCategory::paginate($perPage);
        }

        return view('admin.occasion-categories.index', compact('occasioncategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.occasion-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        OccasionCategory::create($requestData);

        return redirect('admin/occasion-categories')->with('flash_message', 'OccasionCategory added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $occasioncategory = OccasionCategory::findOrFail($id);

        return view('admin.occasion-categories.show', compact('occasioncategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $occasioncategory = OccasionCategory::findOrFail($id);

        return view('admin.occasion-categories.edit', compact('occasioncategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $occasioncategory = OccasionCategory::findOrFail($id);
        $occasioncategory->update($requestData);

        return redirect('admin/occasion-categories')->with('flash_message', 'OccasionCategory updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        OccasionCategory::destroy($id);

        return redirect('admin/occasion-categories')->with('flash_message', 'OccasionCategory deleted!');
    }
}
