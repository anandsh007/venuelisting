@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div>
            <h3>Venue Address {{ $venueaddress->id }}</h3>
            <div>

                <a href="{{ url('/admin/venue-addresses') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/admin/venue-addresses/' . $venueaddress->id . '/edit') }}" title="Edit VenueAddress"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/venueaddresses', $venueaddress->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete VenueAddress',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $venueaddress->id }}</td>
                            </tr>
                            <tr><th> Address </th><td> {{ $venueaddress->address }} </td></tr><tr><th> City </th><td> {{ $venueaddress->city }} </td></tr><tr><th> State </th><td> {{ $venueaddress->state }} </td></tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
