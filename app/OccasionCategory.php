<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OccasionCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'occasion_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function occasions()
    {
        return $this->hasMany('App\Occasion');
    }

    
}
