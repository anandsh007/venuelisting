<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\VenueEnquiry;
use Illuminate\Http\Request;
use App\Occasion;

class EnquiryController extends Controller
{

  public function getOccasion(Request $request)
	{
	   
	 
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $occasions = Occasion::where('name','like','%'.$term.'%')->get();

        $formatted_occasions = [];

        foreach ($occasions as $occasion) {
            $formatted_occasions[] = ['id' => $occasion->id, 'text' => $occasion->name];
        }

        return \Response::json($formatted_occasions);
	       
	}

    public function submitEnquiry(Request $request)
    {
        $this->validate($request, [
            'name' => 'max:100|required',
            'email' => 'max:100|email|required',
            'number' => 'digits_between:10,11|required',
            'date' => 'date',
            'number_of_guests' => 'numeric',
            'budget' => 'numeric',
            'occasion_id' => 'integer|exists:occasions,id',
            'venue_id' => 'integer|exists:venues,id'
        ]);
        $requestData = $request->all();
        
        VenueEnquiry::create($requestData);

        return redirect()->back()->with('flash_message', 'Blog added!')->withInput();
    }

}