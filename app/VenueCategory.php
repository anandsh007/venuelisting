<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function venueSubCategories()
    {
        return $this->belongsToMany('App\VenueSubCategory','venue_category_sub_categories');
    }
    
}
