@extends('layouts.app')
@section('title')
Create Venue
@endsection
@section('styles')
<style>
.form-group.required .control-label:after { content:"*"; color:red; }
/* Always set the map height explicitly to define the size of the div*/
.filepond--drop-label {
  color: #4c4e53;
}

.filepond--label-action {
  text-decoration-color: #babdc0;
}

.filepond--panel-root {
  border-radius: 2em;
  background-color: #edf0f4;
  height: 1em;
}

.filepond--item-panel {
  background-color: #595e68;
}

.filepond--drip-blob {
  background-color: #7f8a9a;
}
</style>​
<link rel="stylesheet" type="text/css" href="">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="{{ asset('css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.fileupload-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('https://www.tinymce.com/css/codepen.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"> 
@endsection
@section('content')
<div class="container ">
  <div class="row">
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok"></span>
      <em> {!! session('flash_message') !!}</em>
    </div>
    @endif
  </div>
</div>
{{--image set the bachground start--}}
<div id="listvenue-image" class="listvenue-image-margin2">
        <div class="container">
            <h2 class="logheading">List Your Venue</h2>
        </div>

    </div>
<div class="container">
  <div class="row">
    @if ($errors->any())
    <ul class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
    @endif
    <section id="main-container" class="container inner">
        <div class="row">
            <div id="main-content" class="main-content col-xs-12 col-lg-12 col-md-12">
                <div id="primary" class="content-area">
                    <div id="content" class="site-content" role="main">
                        <article id="post-10509" class="post-10509 page type-page status-publish hentry">
                            <div class="entry-content-page">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="place-submission-form metabox-fields-front space-padding-lr-40 space-padding-tb-30">
                                          {!! Form::open(['route' => 'venue.store', 'class' => 'form-horizontal', 'files' => true, 'method' => 'post','id' => 'my-form']) !!}
                                          <h1><span class="fa fa-plus"></span>  Add Your Venue</h1>
                                          @include ('venue.form')
                                          {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
  </div>
</div>
@endsection
@section('scripts')

<!--=============== pincode Ajax call ================-->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script> 
  <script type="text/javascript">
     $(function () {
      $('#PincodesId').hide();
     });
  </script>
<!--=============== //pincode Ajax call ================-->

<!--============ Text Editor Js =============-->
<script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
<script type="text/javascript">
  tinymce.init({
  selector: '.tinymce-editor-short-desc',
  height: 150,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'],
  max_chars: 255, // max. allowed chars
    setup: function (ed) {
        var allowedKeys = [8, 37, 38, 39, 40, 46]; // backspace, delete and cursor keys
        ed.on('keydown', function (e) {
            if (allowedKeys.indexOf(e.keyCode) != -1) return true;
            if (tinymce_getContentLength() + 1 > this.settings.max_chars) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            return true;
        });
        ed.on('keyup', function (e) {
            tinymce_updateCharCounter(this, tinymce_getContentLength());
        });
    },
    init_instance_callback: function () { // initialize counter div
        $('#' + this.id).prev().append('<div class="char_count" style="text-align:right"></div>');
        tinymce_updateCharCounter(this, tinymce_getContentLength());
    },
    paste_preprocess: function (plugin, args) {
        var editor = tinymce.get(tinymce.activeEditor.id);
        var len = editor.contentDocument.body.innerText.length;
        var text = $(args.content).text();
        if (len + text.length > editor.settings.max_chars) {
            alert('Pasting this exceeds the maximum allowed number of ' + editor.settings.max_chars + ' characters.');
            args.content = '';
        } else {
            tinymce_updateCharCounter(editor, len + text.length);
        }
    }
});
tinymce.init({
  selector: '.tinymce-editor-long-desc',
  height: 150,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'],
  max_chars: 2000, // max. allowed chars
    setup: function (ed) {
        var allowedKeys = [8, 37, 38, 39, 40, 46]; // backspace, delete and cursor keys
        ed.on('keydown', function (e) {
            if (allowedKeys.indexOf(e.keyCode) != -1) return true;
            if (tinymce_getContentLength() + 1 > this.settings.max_chars) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            return true;
        });
        ed.on('keyup', function (e) {
            tinymce_updateCharCounter(this, tinymce_getContentLength());
        });
    },
    init_instance_callback: function () { // initialize counter div
        $('#' + this.id).prev().append('<div class="char_count" style="text-align:right"></div>');
        tinymce_updateCharCounter(this, tinymce_getContentLength());
    },
    paste_preprocess: function (plugin, args) {
        var editor = tinymce.get(tinymce.activeEditor.id);
        var len = editor.contentDocument.body.innerText.length;
        var text = $(args.content).text();
        if (len + text.length > editor.settings.max_chars) {
            alert('Pasting this exceeds the maximum allowed number of ' + editor.settings.max_chars + ' characters.');
            args.content = '';
        } else {
            tinymce_updateCharCounter(editor, len + text.length);
        }
    }
});

function tinymce_updateCharCounter(el, len) {
    $('#' + el.id).prev().find('.char_count').text(len + '/' + el.settings.max_chars);
}

function tinymce_getContentLength() {
    return tinymce.get(tinymce.activeEditor.id).contentDocument.body.innerText.length;
}
</script>
<!--============ //Text Editor Js =============-->

<!--=============== checkbox script =============-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
$( ".jquerycheckbox" ).checkboxradio();
} );
</script>
<!--=============== //checkbox script =============-->

<!--=============== google map script ============-->
<script type="text/javascript" src="{{ asset('js/jquery.address.js') }}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyAjYIJDSpRo90YUDZNtLnSCTmuMHfLMAlo"></script>
<script type="text/javascript" src="{{ asset('js/maps_lib.js') }}"></script>
<script type='text/javascript'>
//<![CDATA[
$(window).resize(function () {
  var h = $(window).height(),
  offsetTop = 220; // Calculate the top offset

  $('#map_canvas').css('height', (h - offsetTop));
  }).resize();

  $(function() {
    var myMap = new MapsLib({
      fusionTableId:      "1m4Ez9xyTGfY2CU6O-UgEcPzlS0rnzLU93e4Faa0",
      googleApiKey:       "AIzaSyAjYIJDSpRo90YUDZNtLnSCTmuMHfLMAlo",
      locationColumn:     "geometry",
      map_center:         [20.593684, 80.95661949999999],
      defaultZoom:        4,
      addrMarkerImage:    "{{ asset('images/spotlight-poi2.png') }}",
      locationScope:      ""

    });
    document.getElementById('search_address').value = "";
    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('search_address'));
    autocomplete.setComponentRestrictions(
                {'country': 'in'});
    google.maps.event.addListener(autocomplete, 'place_changed',function(){
      myMap.doSearch();
      var place = autocomplete.getPlace();
      var address = place.address_components;
      var street, city,country, state, zip;
      document.getElementById('street_number').value = "";
      document.getElementById('sublocality_level_1').value = "";
      document.getElementById('locality').value = "";
      document.getElementById('administrative_area_level_1').value = "";
      document.getElementById('postal_code').value = "";
      document.getElementById('country').value = "";
      document.getElementById('lat').value = "";
      document.getElementById('long').value = ""; 

      var latitude = place.geometry.location.lat(); // latitude of location
      document.getElementById('lat').value = latitude;
      var longitude = place.geometry.location.lng(); // longitude of location
      document.getElementById('long').value = longitude; 
      address.forEach(function(component) {
        var types = component.types;
          if (types.indexOf('street_number') > -1) 
            {
              street = component.long_name;
              document.getElementById('street_number').value = street;
            }
          if (types.indexOf('sublocality_level_1') > -1) 
            {
              area = component.long_name;
              document.getElementById('sublocality_level_1').value = area;
            }
          if (types.indexOf('locality') > -1) 
            {
              city = component.long_name;
              document.getElementById('locality').value = city;
            }
          if (types.indexOf('administrative_area_level_1') > -1) 
            {
              state = component.long_name;
              document.getElementById('administrative_area_level_1').value = state;
            }
          if (types.indexOf('postal_code') > -1) 
            {
              zip = component.long_name;
              document.getElementById('postal_code').value = zip;
            }
          if (types.indexOf('country') > -1) 
            {
              country = component.long_name;
              document.getElementById('country').value = country;
            }
      });
    $(function () {
     var value = $('#sublocality_level_1').val();
     if(value == '')
      {
        $('#sublocality_level_1').hide();
         $('#PincodesId').show();
         $('#PincodesId').select2({
          placeholder: 'Search area..',
          ajax: {
              url: '{{ route('pincode.getPincodes') }}',
              dataType: 'json',
              data: function (params) {
                  return {
                      q: $.trim(params.term)
                  };
              },
              processResults: function (data) {
                  return {
                      results: data
                  };
              },
              cache: true
          }
        });
     }
     else
         $('#PincodesId').hide(); 
      
  });
  });

  $('#search_radius').change(function(){
    myMap.doSearch();
  });

  $('#reset').click(function(){
    myMap.reset();
    return false;
  });
});
//]]>
</script>
<!--=============== //google map script ============-->

<!--=============== File Upload Script =============-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.iframe-transport/1.0.1/jquery.iframe-transport.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.17.0/js/jquery.fileupload.min.js"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fileupload-image.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fileupload-process.js') }}"></script>
<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script>
    $(function () {
        var filList;
        $('#fileupload').fileupload({
            url: "{{\URL::route('files.imageUpload')}}",
            dataType: 'json',
            add: function (e, data) {
                var filename = data.files[0].name;
                filenameID = filename.replace(/[^a-z0-9\s]/gi, '').replace(/[_.\s]/g, '-');

                if ($.inArray(filename, filList) !== -1) {
                    alert("Filename already exist");
                    return false;
                }

                filList = [filename];

                //on click to upload
                $('.file_progress').show();

                data.context = $('.upload_button').find('span').text('Uploading...');

                var uploadResponse = data.submit()
                    .error(function (uploadResponse, textStatus, errorThrown) {
                        alert("Error: " + textStatus + " | " + errorThrown);
                        return false;
                    });

            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress-bar').css('width', progress + '%');
            },
            done: function (e, data)
            {
                e.preventDefault();

                var filename = data.files[0].name;
                var filenameID = filename.replace(/[^a-z0-9\s]/gi, '').replace(/[_.\s]/g, '-');

                var file_list = "";
                var link = '';

                $.each(data.result.files, function (index, file)
                {
                    link = '<a target="_blank" href="'+file.url+'">'+file.name+'</a>';
                    file_list += '<li class="list-group-item"><button class="btn btn-xs pull-right btn-danger remove_file">X</button> '+link+' </li>';
                    if ($('#file_ids').val() != '') {
                    $('#file_ids').val($('#file_ids').val() + ',');
                    }
                    $('#file_ids').val($('#file_ids').val() + file.name);
                });

                $(".uploaded_files").append(file_list);

                $('.file_progress').hide();
                $('.progress-bar').css('width', '0%');
                $('.upload_button').find('span').text('Add');
            },
        });

        $('.uploaded_files').on("click", ".remove_file", function () {
            $(this).closest("li").remove();
            return false;
        });
    });
</script>
<!--=============== //File Upload Script =============-->
<script type="text/javascript">
$(document).ready(function(){

 // Add new element
 $("#add").click(function(){
  var input = $("#package").html();
  // Finding total number of elements added
  var total_element = $(".element").length;
 
  // last <div> with element class id
  var lastid = $(".element:last").attr("id");
  var split_id = lastid.split("_");
  var nextindex = Number(split_id[1]) + 1;

  var max = 5;
  // Check total number elements
  if(total_element < max ){
   // Adding new div container after last occurance of element class
   $(".element:last").after("<div class='row element' id='div_"+ nextindex +"'></div>");
 
   // Adding element to <div>
   $("#div_" + nextindex).append(input+"<span id='remove_" + nextindex + "' class='remove btn btn-default col-md-1'>X</span>");
 
  }
 
 });

 // Remove element
 $('.package-element').on('click','.remove',function(){
 
  var id = this.id;
  var split_id = id.split("_");
  var deleteindex = split_id[1];

  // Remove <div> with id
  $("#div_" + deleteindex).remove();

 }); 
});
</script>
<!--=============== Form Validation Script =============-->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js', '#my-form')}}"></script>
{!! $validator !!}
<!--=============== //Form Validation Script =============-->
@endsection