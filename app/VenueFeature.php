<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueFeature extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue_features';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','font_awesome_icon'];

    public function venues()
    {
      return $this->belongsToMany('App\Venue','venue_venue_features')->withTimestamps();
    }

    
}
