@extends('layouts.app')

@section('styles')

    <link rel="stylesheet" href="{{ URL::asset('css/kingcomposer.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" type="text/css">
    <style>
       .progress-item span {
            color: rgba(0, 0, 0, 0.92);
            /*font-weight: 700;*/
            text-transform: uppercase;
        }
/*================================== parallax css ===========*/
       /*.kc-css-250453 {
           background: transparent url(images/apple-iphone-books-desk.jpg)
           0% 0%/cover no-repeat fixed;
       }
       .kc-css-676349 .kc_title{
           color: #FFFFFF;
       }
       .kc-css-980767 .kc-cta-desc h2 {
           color: #ffffff;
       }
       .kc-css-980767 .kc-cta-desc .kc-cta-text {
           color: rgba(255, 255, 255, 0.72);
       }
       .team-v2 .team-body {
           padding: 19px;
           background: #fff;
       }*/
    </style>
@endsection
@section('content')
<div class="">
<div class="row">
    <section id="opal-breadscrumb" class="opal-breadscrumb" style="">
        <div class="container">
            <h2 class="navheading">FAQ</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('home.aboutUs')}}">Home</a> </li><span></span>
                <li>FAQ</li>
            </ol>
        </div>
    </section>               
    <section id="main-container" class="container inner">
    <div class="row">

        <div id="main-content" class="main-content col-xs-12 col-lg-12 col-md-12">
            <div id="primary" class="content-area">
                <div id="content" class="site-content" role="main">

                    <article id="post-1846" class="post-1846 page type-page status-publish hentry">
                        <div class="entry-content-page">
                            <div class="kc_clfw"></div>
                            <section class="kc-elm kc-css-938573 kc_row">
                                <div class="kc-row-container  kc-container">
                                    <div class="kc-wrap-columns">
                                        <div class="kc-elm kc-css-599861 kc_col-sm-12 kc_column kc_col-sm-12">
                                            <div class="kc-col-container">
                                                <div class="element-block-heading style-v2">
                                                    <div class="inner">
                                                        <h2 class="heading">Our FAQ</h2>

                                                    </div>
                                                </div>
                                                <div id=" " class="kc-elm kc-css-668841 kc_row kc_row_inner highlight">
                                                    <div class="kc-elm kc-css-771935 kc_col-sm-6 kc_column_inner kc_col-sm-6">
                                                        <div class="kc_wrapper kc-col-inner-container">
                                                            <div data-allowopenall="true" class="kc-elm kc-css-817786 kc_accordion_wrapper">
                                                                <div class="kc-elm kc-css-379061 kc_accordion_section group ">
                                                                    <h3 class="kc_accordion_header ui-accordion-header"><span class="ui-accordion-header-icon ui-icon"></span><a href="#how-many-times-do-i-have-to-tell-you-a-few-ways" data-prevent="scroll"><i class=""></i>  What is VenueListing?</a></h3>
                                                                    <div class="kc_accordion_content ui-accordion-content kc_clearfix">
                                                                        <div class="kc-panel-body">
                                                                            <div class="kc-elm kc-css-134728 kc_text_block">
                                                                                <p>Progressively generate synergistic total linkage through cross-media intellectual capital. Enthusiastically parallel task team building e-tailers without standards compliant initiatives. Progressively monetize client-centric outsourcing with excellent communities.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-allowopenall="true" class="kc-elm kc-css-800658 kc_accordion_wrapper">
                                                                <div class="kc-elm kc-css-969774 kc_accordion_section group ">
                                                                    <h3 class="kc_accordion_header ui-accordion-header"><span class="ui-accordion-header-icon ui-icon"></span><a href="#what-is-do-i-have-to-tell-you-a-few-lorem" data-prevent="scroll"><i class=""></i> What is do i have to tell you a few lorem?</a></h3>
                                                                    <div class="kc_accordion_content ui-accordion-content kc_clearfix">
                                                                        <div class="kc-panel-body">
                                                                            <div class="kc-elm kc-css-267193 kc_text_block">
                                                                                <p>Progressively generate synergistic total linkage through cross-media intellectual capital. Enthusiastically parallel task team building e-tailers without standards compliant initiatives. Progressively monetize client-centric outsourcing with excellent communities.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-allowopenall="true" class="kc-elm kc-css-328710 kc_accordion_wrapper">
                                                                <div class="kc-elm kc-css-122060 kc_accordion_section group ">
                                                                    <h3 class="kc_accordion_header ui-accordion-header"><span class="ui-accordion-header-icon ui-icon"></span><a href="#i-have-a-technical-problem-or-support-issue-i-need-resolved-who-do-i-email" data-prevent="scroll"><i class=""></i> I have a technical problem or support issue I need resolved, who do I email?</a></h3>
                                                                    <div class="kc_accordion_content ui-accordion-content kc_clearfix">
                                                                        <div class="kc-panel-body">
                                                                            <div class="kc-elm kc-css-155480 kc_text_block">
                                                                                <p>Progressively generate synergistic total linkage through cross-media intellectual capital. Enthusiastically parallel task team building e-tailers without standards compliant initiatives. Progressively monetize client-centric outsourcing with excellent communities.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="kc-elm kc-css-108263 kc_col-sm-6 kc_column_inner kc_col-sm-6">
                                                        <div class="kc_wrapper kc-col-inner-container">
                                                            <div data-allowopenall="true" class="kc-elm kc-css-179519 kc_accordion_wrapper">
                                                                <div class="kc-elm kc-css-697666 kc_accordion_section group ">
                                                                    <h3 class="kc_accordion_header ui-accordion-header"><span class="ui-accordion-header-icon ui-icon"></span><a href="#what-other-services-are-you-compatible-with" data-prevent="scroll"><i class=""></i> What other services are you compatible with?</a></h3>
                                                                    <div class="kc_accordion_content ui-accordion-content kc_clearfix">
                                                                        <div class="kc-panel-body">
                                                                            <div class="kc-elm kc-css-330250 kc_text_block">
                                                                                <p>Progressively generate synergistic total linkage through cross-media intellectual capital. Enthusiastically parallel task team building e-tailers without standards compliant initiatives. Progressively monetize client-centric outsourcing with excellent communities.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-allowopenall="true" class="kc-elm kc-css-162449 kc_accordion_wrapper">
                                                                <div class="kc-elm kc-css-166592 kc_accordion_section group ">
                                                                    <h3 class="kc_accordion_header ui-accordion-header"><span class="ui-accordion-header-icon ui-icon"></span><a href="#are-you-hiring" data-prevent="scroll"><i class=""></i> Are you hiring?</a></h3>
                                                                    <div class="kc_accordion_content ui-accordion-content kc_clearfix">
                                                                        <div class="kc-panel-body">
                                                                            <div class="kc-elm kc-css-516634 kc_text_block">
                                                                                <p>Progressively generate synergistic total linkage through cross-media intellectual capital. Enthusiastically parallel task team building e-tailers without standards compliant initiatives. Progressively monetize client-centric outsourcing with excellent communities.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <!-- .entry-content -->
                    </article>
                    <!-- #post-## -->

                </div>
                <!-- #content -->
            </div>
            <!-- #primary -->

        </div>
        <!-- #main-content -->

    </div>
</section>
</div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/kingcomposer.min.js') }}"></script>
@endsection