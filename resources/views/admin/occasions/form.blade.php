<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-12 bmd-label-floating']) !!}
    <div class="col-md-12">
        {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('font_awesome_icon') ? 'has-error' : ''}}">
    {!! Form::label('font_awesome_icon', 'Icon', ['class' => 'col-md-12 bmd-label-floating']) !!}
    <div class="col-md-12">
       <div class="input-group no-border"> 
        {!! Form::text('font_awesome_icon', null, ('required' == 'required') ? ['class' => 'form-control icp icp-auto','data-placement'=>'bottomRight','value'=>'fas fa-archive', 'required' => 'required'] : ['class' => 'form-control icp icp-auto','value'=>'fas fa-archive','data-placement'=>'bottomRight']) !!}
        <span class="input-group-addon btn btn-white btn-round btn-just-icon"></span>
       </div>
        {!! $errors->first('font_awesome_icon', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('occasion_category_id') ? 'has-error' : ''}}">
    {!! Form::label('occasion_category_id', 'Category', ['class' => 'col-md-12 bmd-label-floating']) !!}
    <div class="col-md-12">
        {!! Form::select('occasion_category_id',$occasionCategoryId, null, ('required' == 'required') ? ['class' => 'js-example-basic-multiple form-control','multiple'=>'multiple', 'required' => 'required'] : ['class' => 'js-example-basic-multiple form-control','multiple'=>'multiple']) !!}
        {!! $errors->first('occasion_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="custom-file-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-4">
        {!! Form::file('image', ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-12 col-md-12">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
