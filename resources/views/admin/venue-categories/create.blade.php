@extends('layouts.backend')
@section('content')
@section('title','Venue Categories')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/admin/venue-categories') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Create New Venue Category</h4>
                    </div>
                    <div class="card-body">
                    {!! Form::open(['url' => '/admin/venue-categories', 'class' => 'form-horizontal', 'files' => true]) !!}

                    @include ('admin.venue-categories.form')

                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
