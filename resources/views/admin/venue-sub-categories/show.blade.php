@extends('layouts.backend')

@section('content')
@section('title','Venue Sub Categories')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/admin/venue-sub-categories') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/admin/venue-sub-categories/' . $venueSubCategories->id . '/edit') }}" title="Edit Venue Sub Category"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/venue-sub-categories', $venueSubCategories->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete Venue Sub-Category',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Venue Sub Category {{ $venueSubCategories->id }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $venueSubCategories->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $venueSubCategories->name }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
