<?php

namespace App\Http\Controllers\Admin\Venue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VenueImage;
use Illuminate\Http\Request;

class VenueImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $venueimages = VenueImage::where('venue_id', 'LIKE', "%$keyword%")
                ->orWhere('image_name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venueimages = VenueImage::paginate($perPage);
        }

        return view('admin.venue-images.index', compact('venueimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.venue-images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        VenueImage::create($requestData);

        return redirect('admin/venue-images')->with('flash_message', 'VenueImage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $venueimage = VenueImage::findOrFail($id);

        return view('admin.venue-images.show', compact('venueimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $venueimage = VenueImage::findOrFail($id);

        return view('admin.venue-images.edit', compact('venueimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $venueimage = VenueImage::findOrFail($id);
        $venueimage->update($requestData);

        return redirect('admin/venue-images')->with('flash_message', 'VenueImage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        VenueImage::destroy($id);

        return redirect('admin/venue-images')->with('flash_message', 'VenueImage deleted!');
    }
}
