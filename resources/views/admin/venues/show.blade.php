@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    <div>
        <h3>Venue {{ $venue->id }}</h3>
        <div>
            <a href="{{ url('/admin/venues') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/admin/venues/' . $venue->id . '/edit') }}" title="Edit Venue"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
            {!! Form::open([
            'method'=>'DELETE',
            'url' => ['admin/venues', $venue->id],
            'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-sm',
            'title' => 'Delete Venue',
            'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $venue->id }}</td>
                        </tr>
                        <tr><th> Name </th><td> {{ $venue->name }} </td></tr><tr><th> Long Desc </th><td> {{ $venue->long_desc }} </td></tr><tr><th> Short Desc </th><td> {{ $venue->short_desc }} </td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection