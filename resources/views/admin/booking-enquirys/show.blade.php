@extends('layouts.backend')
@section('content')
@section('title','Bookng Enquiry')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/admin/booking-enquirys') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <!--<a href="{{ url('/admin/booking-enquirys/' . $bookingEnquiry->id . '/edit') }}" title="Edit bookingenquirys"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/booking-enquirys', $bookingEnquiry->id],
                'style' => 'display:inline'
                ]) !!}
                {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', array(
                'type' => 'submit',
                'class' => 'btn btn-danger btn-sm',
                'title' => 'Delete bookingenquirys',
                'onclick'=>'return confirm("Confirm delete?")'
                ))!!}-->
                {!! Form::close() !!}
                <br/>
                <br/>
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Booking Enquiry {{ $bookingEnquiry->id }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $bookingEnquiry->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Name </th>
                                        <td> {{ $bookingEnquiry->name }} </td>
                                    </tr>
                                    <tr>
                                    	<th>Email</th>
                                    	<td> {{ $bookingEnquiry->email}} </td>
                                    </tr>
                                    <tr>
                                    	<th>Contact NO</th>
                                    	<td> {{ $bookingEnquiry->number}} </td>
                                    </tr>
                                    <tr>
                                    	<th>Number of Guest</th>
                                    	<td> {{ $bookingEnquiry->number_of_guest}} </td>
                                    </tr>
                                    <tr>
                                    	<th>Budget</th>
                                    	<td> {{ $bookingEnquiry->budget}} </td>
                                    </tr>
                                    <tr>
                                    	<th>Occasion</th>
                                    	<td> {{ $bookingEnquiry->occasion_id}} </td>
                                    </tr>
                                    <tr>
                                    	<th>Date</th>
                                    	<td> {{ $bookingEnquiry->date}} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
