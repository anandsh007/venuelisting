<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venue extends Model
{
    // use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venues';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'long_desc', 'short_desc', 'max_capacity','mob_number','phone_number'];
    
    /***********relationship*************/
    
    public function venueAmenities()
    {
        return $this->belongsToMany('App\VenueAmenity','venue_venue_amenities')->withTimestamps();
    }
    public function venuePaymentOptions()
    {
        return $this->belongsToMany('App\PaymentOption','venue_payment_options')->withTimestamps();
    }
    public function venuePackages()
    {
        return $this->hasMany('App\VenuePackage','venue_id');
    }
    public function venueSubCategories()
    {
        return $this->belongsToMany('App\VenueSubCategory','venue_venue_subcategories')->withTimestamps();
    }
    public function occasions()
    {
        return $this->belongsToMany('App\Occasion','venue_occasions')->withTimestamps();
    }
    public function venueFeatures()
    {
        return $this->belongsToMany('App\VenueFeature','venue_venue_features')->withTimestamps();
    }
    public function venueAddress()
    {
        return $this->hasOne('App\VenueAddress','venue_id');
    }
    public function venueVideo()
    {
        return $this->hasOne('App\VenueVideo','venue_id');
    }
    public function venueImages()
    {
        return $this->hasMany('App\VenueImage','venue_id');
    }
    public function venueTags()
    {
        return $this->belongsToMany('App\Tag','venue_tags')->withTimestamps();
    }
    public function reviews(){
           return $this->hasMany('App\Review','venue_id');
    }
    public function venueEnquiries()
    {
        return $this->hasMany('App\VenueEnquiry','venue_id');
    }
    /***********///relationship*************/
}
