<?php

namespace App\Http\Controllers\Admin;

use App\Venue;
use App\VenueEnquiry;
use Carbon\Carbon;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
       /* $booking =  array();
        $i = 0;
        $totalDays = 49;
        for($i = 0; $i < 7; $i++)
        {
            $subtractDays = 49 - (7 * $i);
            $startDateTime = Carbon::now('Asia/Kolkata')->subDays($subtractDays);
            $startDate = $startDateTime->format("Y-m-d H:m:s");
            $endDateTime = $startDateTime->addDays(7);
            $endDate = $endDateTime->format("Y-m-d H:m:s");
            $count = VenueEnquiry::where('created_at','>',$startDate)
                                ->where('created_at','<=',$endDate)
                                ->count();
            $weekNumber = $i + 1;
            $arrayName['week'] = $weekNumber;
            $arrayName['no_of_enquiry'] = $count; 
            $booking[] = $arrayName;

            $weekNumber = $i + 1;
            $arrayNumber['Number'] = $weekNumber;
            $arrayName['no_of_enquiry'] = $count;
            $booking[] = $arrayNumber;
        }
        $label = array_column($booking, 'week'); 
        $series = array_column($booking,'Number');
       //print_r($label);
      //die(); */

         $venueCount =Venue::count();
        $bookingCount =VenueEnquiry::count();
   
        return view('admin.dashboard',compact('venueCount','bookingCount'));
    }

    public function userProfile()
    {
    	return view('admin.profile');
    }
}
