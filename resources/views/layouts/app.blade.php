<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1"> @yield('meta')

  <title>@yield('title')</title>
  <!-- Fonts -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
  {{--<link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>--}}
  <link href="https://fonts.googleapis.com/css?family=Heebo|Roboto" rel="stylesheet">

  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ URL::asset('css/mdb.css') }}" type="text/css">
  <!-- bootstrap css -->
  <link rel="shortcut icon" href="{{ URL::asset('favicon.png') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/navbar.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ URL::asset('css/common.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ URL::asset('css/footer.css') }}" type="text/css"> 
  <link rel="stylesheet" href="{{ URL::asset('css/kingcomposer.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ URL::asset('css/opallisting.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css">
  @yield('styles')
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" type="text/css">
</head>
<!-- Start Navbar -->
<body>
<!--Navbar-->
<nav class="navbar navbar-fixed-top custom-navbar navbar-transparent-background">
  <div class="container-fluid"  >
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand waves-effect waves-light" href="{{route('home.home')}}">
          <img src="{{url('/images/VenueListing3.png')}}" width="170px" alt="Budget Teknologies - Delivering Value For Money" />
      </a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        <li><a href="{{route('home.home')}}" class="waves-effect waves-light">Home</a></li>
        <li><a href="{{route('home.aboutUs')}}" class="waves-effect waves-light">About Us</a></li>
        <li><a href="{{route('home.contactUs')}}" class="waves-effect waves-light">Contact Us</a></li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" role="button"
             aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Personal</a></li>
            <li><a href="#">Business</a></li>
            <li><a href="#">Vendors</a></li>
            <li><a href="{{route('home.pricing')}}">Pricing</a></li>
            <li><a href="{{route('home.faq')}}">FAQ</a></li>
            <li><a href="{{route('home.error404')}}">404 Error</a></li>
          </ul>
        </li>       
        <li><a href="{{route('venue.create')}}" class="waves-effect waves-light">
                <span class="fa fa-pencil-square-o"></span>&nbsp;Add a Venue</a></li>       
      </ul>
      {{--=============================login==========--}}
      <ul class="nav navbar-nav navbar-right login-margin-right">
          @if (Auth::guest())
            <li><a href="{{ url('/login') }}"  class='navbar-nopadding-link'>
                <span class="fa fa-user"></span>&nbsp;Login</a></li>
            <li><a href="{{ url('/register') }}" class='navbar-nopadding-link'>
                <span class="fas fa-sign-in-alt"></span>&nbsp;SignUp</a></li>

          @else
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ url('/admin') }}">
                    <i class="fas fa-tachometer-alt"></i>&nbsp;Dahboard</a>
                </li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {!! Form::token() !!}
                </form>
                <li><a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>&nbsp;Logout</a></li>

              </ul>

            </li>
          @endif
      </ul>
       {{--=============================//login==========--}}
    </div>
  </div>
</nav>
<!--/.Navbar-->

<div class="body-content">
  @yield('content')
</div>
{{--
<div class=" container-fluid">--}}
<div class="">
  @include('shared.footer')
</div>
<!--    JavaScripts -->
<script type="text/javascript" src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/mdb.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/kingcomposer.min.js') }}"></script>
<!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<!-- test code js-->
<!-- test code js-->
<script>
    $(document).ready(function(){
        $(window).scroll(function() { // check if scroll event happened
            if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
                $(".custom-navbar").removeClass("navbar-transparent-background");
                $(".custom-navbar").addClass("navbar-white-background");
            } else {
                $(".custom-navbar").removeClass("navbar-white-background");
                $(".custom-navbar").addClass("navbar-transparent-background");
            }
        });
    });
</script>
@yield('scripts')
</body>
</html>