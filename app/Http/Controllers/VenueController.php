<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Venue;
use App\VenueAmenity;
use App\Occasion;
use App\OccasionCategory;
use App\VenueCategory;
use App\VenueSubCategory;
use App\VenueFeature;
use App\VenueImage;
use App\VenueVideo;
use App\VenueAddress;
use App\PaymentOption;
use App\VenuePackage;
use App\Tag;
use App\Review;
use Session;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Entrust;

class VenueController extends Controller
{

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
            'name' => 'max:100|required',
            'max_capacity' => 'integer|required',
            'long_desc' => 'max:2000|required',
            'short_desc' => 'max:1000|required',
            'address' => 'max:4000|required',
            'mob_number' => 'digits:10|required',
            'phone_number' => 'digits:10',
            'package_name' => 'max:100|required',
            'cost_per_person' => 'required',
            'street_number' => 'max:255|required',
            'venue_video' => ['regex:/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/','nullable']
    ];
    protected $customAttributes = [
        'name' => 'Venue Name',
        'max_capacity' => 'Maximum Capacity',
        'long_desc' => 'Long Description',
        'short_desc' => 'Short Description',
        'address' => 'Address',
        'mob_number' => 'Mobile Number',
        'phone_number' => 'Phone Number',
        'package_name' => 'Package Name',
        'cost_per_person' => 'Cost per Person',
        'venue_video' => 'Venue Video'
    ];

   
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $venue = Venue::where('email', 'LIKE', "%$keyword%")
                ->orWhere('message', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venue = Venue::paginate($perPage);
        }

        return view('venue.index', compact('venue'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
       $validator = JsValidator::make($this->validationRules,[],$this->customAttributes);
       $occasions = OccasionCategory::with('occasions')->get();
       $venueCategories = VenueCategory::with('venueSubCategories')->get();
       $venueAmenities = VenueAmenity::get();
       $paymentOptions = PaymentOption::get();
       $venueFeatures = VenueFeature::get();
       $tags = Tag::get();
       
        return view('venue.list',compact('occasions','paymentOptions','venueCategories','venueAmenities','venueFeatures','tags','validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), $this->validationRules,[],$this->customAttributes);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }
        $requestData = $request->all(); 
        $venues = Venue::create($requestData);
        if($request->has('venue_video') )
        {
           if(!empty($request->get('venue_video')))
           {
               $videoUrl = $request->venue_video;
               parse_str( parse_url( $videoUrl, PHP_URL_QUERY ), $arrayOfVars );
               $request->venue_video = $arrayOfVars['v'];
           } 
           $venueVideo = VenueVideo::create(['video_name'=> $request->venue_video]);
           $venues->venueVideo()->save($venueVideo);
        }
        if($request->has('amenities'))
        {
            $venues->venueAmenities()->sync($request->amenities);
        }
        if($request->has('venue_sub_categories'))
        {
            $venues->venueSubCategories()->sync($request->venue_sub_categories);
        }
        if($request->has('payment_option'))
        {
            $venues->venuePaymentOptions()->sync($request->payment_option); 
        }
        if($request->has('venue_features'))
        {
            $venues->venueFeatures()->sync($request->venue_features);
        }
        if($request->has('occasions'))
        {
           $venues->occasions()->sync($request->occasions);
        }
        if($request->has('tags'))
        {
           $venues->venueTags()->sync($request->tags);
        }
        $venueAddresses = VenueAddress::create(['address'=>$request->address,'street_number'=>$request->street_number,'area'=>$request->area,'city'=>$request->city,'state'=>$request->state,'country'=>$request->country,'postal_code'=>$request->postal_code,'latitude'=>$request->latitude,'longitude'=>$request->longitude]);
        $venues->venueAddress()->save($venueAddresses);
        if($request->has('cost_per_person'))
        {
            $packageNames = $request->package_name;
            foreach ($request->cost_per_person as  $key=>$cost) 
            {
                if(array_key_exists($key ,$packageNames) && $cost != 0)
                {
                    $venuePackages = VenuePackage::create(['package_name'=>$packageNames[$key],'cost_per_person'=>$cost]);
                    $venues->venuePackages()->save($venuePackages);
                }
            }
        }
         
        return redirect('venue')->with('flash_message', 'Venue added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // $venue = Venue::findOrFail($id);
        $venue = Venue::with('venueAmenities', 'venuePaymentOptions', 'venuePackages','venueSubCategories','occasions', 'venueFeatures', 'venueAddress', 'venueVideo','venueImages','venueTags','reviews')
                ->where('id', $id)->firstOrFail();

            if($venue->reviews != null && !($venue->reviews->isEmpty())){
                $ratings = $venue->reviews->toArray();
                $ratingSum = array_sum(array_column($ratings, 'rating'));
                $ratingCount = count(array_column($ratings, 'rating'));
                $avgRating = (float) ($ratingSum/$ratingCount);
                $venue->avgRating = number_format($avgRating, 2, '.', ',');
            }   
            else{
                $venue->avgRating = 0;
            }
                // print_r($venue);
                // die();
        return view('venue.show', compact('venue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $validator = JsValidator::make($this->validationRules,[],$this->customAttributes);
        $venue = Venue::with('venueAmenities', 'venuePaymentOptions', 'venuePackages','venueSubCategories','occasions', 'venueFeatures', 'venueAddress', 'venueVideo','venueImages','venueTags','reviews')
                ->where('id', $id)->firstOrFail();
        // print_r($venue);
        // die();
        $occasions = OccasionCategory::with('occasions')->get();
        $venueCategories = VenueCategory::with('venueSubCategories')->get();
        $venueAmenities = VenueAmenity::get();
        $paymentOptions = PaymentOption::get();
        $venueFeatures = VenueFeature::get();
        $tags = Tag::get();
        return view('venue.edit', compact('venue','occasions','paymentOptions','venueCategories','venueAmenities','venueFeatures','tags','validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), $this->validationRules,[],$this->customAttributes);
        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }
        $requestData = $request->all();
        // print_r($requestData);
        // die();
        $venue = Venue::findOrFail($id);  
        if($request->has('venue_video'))
        {
           $videoUrl = $request->venue_video;
           parse_str( parse_url( $videoUrl, PHP_URL_QUERY ), $arrayOfVars );
           $request->venue_video = $arrayOfVars['v'];
           $venue->venueVideo()->update(['video_name'=> $request->venue_video]);
        }
        $venue->venueAmenities()->sync($request->amenities);
        $venue->venueSubCategories()->sync($request->venue_sub_categories);
        $venue->venuePaymentOptions()->sync($request->payment_option);
        $venue->venueFeatures()->sync($request->venue_features);
        $venue->occasions()->sync($request->occasions);
        $venue->venueTags()->sync($request->tags);
        $venue->venueAddress()->update(['address'=>$request->address,'street_number'=>$request->street_number,'area'=>$request->area,'city'=>$request->city,'state'=>$request->state,'country'=>$request->country,'postal_code'=>$request->postal_code,'latitude'=>$request->latitude,'longitude'=>$request->longitude]);
        if($request->has('cost_per_person') && !empty($request->cost_per_person))
        {
            $venue->venuePackages()->delete();
            $costPerPerson = $request->get('cost_per_person');
            $packageNames = [];
            if($request->has('package_name'))
            {
                $packageName = $request->get('package_name');
                if(!empty($packageName))
                {
                    $packageNames = $packageName;
                }
            }
            foreach($costPerPerson as $key=>$cost)
            {
                if(array_key_exists($key ,$packageNames) && !empty($cost))
                {
                    $venue->venuePackages()->create(['package_name' => $packageNames[$key],
                        'cost_per_person'=> $cost]);
                    
                }
            }
        }
        $venue->update($requestData);
        return redirect('venue')->with('flash_message', 'Venue updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Venue::destroy($id);

        return redirect('venue')->with('flash_message', 'Venue deleted!');
    }

}