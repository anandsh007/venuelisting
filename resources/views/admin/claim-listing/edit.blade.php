@extends('layouts.backend')

@section('content')
@section('title','Claim Listing')
  <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/admin/claim-listing') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                   <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Edit Claim Listing {{ $claimlisting->id }}</h4>
                    </div>
                    <div class="card-body">
                {!! Form::model($claimlisting, [
                    'method' => 'PATCH',
                    'url' => ['/admin/claim-listing', $claimlisting->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}

                @include ('admin.claim-listing.form', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}
                 </div>
                 </div>
                 </div>
            </div>
        </div>
    </div>
@endsection
