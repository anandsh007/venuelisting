@extends('layouts.backend')

@section('content')
   @section('title','Claim Listing')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/admin/claim-listing') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/admin/claim-listing/' . $claimlisting->id . '/edit') }}" title="Edit ClaimListing"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/claimlisting', $claimlisting->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete ClaimListing',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>
                 <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Claim Listing {{ $claimlisting->id }}</h4>
                    </div>
                    <div class="card-body">
                 <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $claimlisting->id }}</td>
                            </tr>
                            <tr><th> Email </th><td> {{ $claimlisting->email }} </td></tr><tr><th> Message </th><td> {{ $claimlisting->message }} </td></tr>
                        </tbody>
                    </table>
                              </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
