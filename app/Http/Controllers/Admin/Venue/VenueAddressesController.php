<?php

namespace App\Http\Controllers\Admin\Venue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VenueAddress;
use Illuminate\Http\Request;

class VenueAddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $venueaddresses = VenueAddress::where('address', 'LIKE', "%$keyword%")
                ->orWhere('city', 'LIKE', "%$keyword%")
                ->orWhere('state', 'LIKE', "%$keyword%")
                ->orWhere('latitude', 'LIKE', "%$keyword%")
                ->orWhere('longitude', 'LIKE', "%$keyword%")
                ->orWhere('country', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venueaddresses = VenueAddress::paginate($perPage);
        }

        return view('admin.venue-addresses.index', compact('venueaddresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.venue-addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        VenueAddress::create($requestData);

        return redirect('admin/venue-addresses')->with('flash_message', 'VenueAddress added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $venueaddress = VenueAddress::findOrFail($id);

        return view('admin.venue-addresses.show', compact('venueaddress'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $venueaddress = VenueAddress::findOrFail($id);

        return view('admin.venue-addresses.edit', compact('venueaddress'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $venueaddress = VenueAddress::findOrFail($id);
        $venueaddress->update($requestData);

        return redirect('admin/venue-addresses')->with('flash_message', 'VenueAddress updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        VenueAddress::destroy($id);

        return redirect('admin/venue-addresses')->with('flash_message', 'VenueAddress deleted!');
    }
}
