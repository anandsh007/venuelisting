<?php

namespace App\Http\Controllers\Admin\Occasion;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Occasion;
use Carbon\Carbon;
use App\OccasionCategory;
use Illuminate\Http\Request;

class OccasionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $occasions = Occasion::where('name', 'LIKE', "%$keyword%")
                ->orWhere('occasion_category_id', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $occasions = Occasion::paginate($perPage);
        }

        return view('admin.occasions.index', compact('occasions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $occasionCategoryId = OccasionCategory::pluck('name','id')->toArray();
        return view('admin.occasions.create',compact('occasionCategoryId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
         $this->validate($request, [
           
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $requestData = $request->all();
        
       $occasion = Occasion::create($requestData);
        $time = Carbon::now();
        $timestamp= $time->format('dmHis');
        if($request->hasFile('image'))
         {
            $file= $timestamp .'_'. $request->file('image')->getClientOriginalName();
            $request->file('image')->move("uploads/occasion/images/",$file);
            $occasion->image = $file;
         }
         $occasion->save();
        return redirect('admin/occasions')->with('flash_message', 'Occasion added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $occasion = Occasion::findOrFail($id);

        return view('admin.occasions.show', compact('occasion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $occasion = Occasion::findOrFail($id);
        $occasionCategoryId = OccasionCategory::pluck('name','id')->toArray();

        return view('admin.occasions.edit', compact('occasion','occasionCategoryId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
           
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $requestData = $request->all();
        
        $occasion = Occasion::findOrFail($id);
         $time = Carbon::now();
        $timestamp= $time->format('dmHis');
        if($request->hasFile('image'))
         {
            $file= $timestamp .'_'. $request->file('image')->getClientOriginalName();
            $request->file('image')->move("uploads/occasion/images/",$file);
            $requestData['image'] = $file;
         }
        $occasion->update($requestData);

        return redirect('admin/occasions')->with('flash_message', 'Occasion updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Occasion::destroy($id);

        return redirect('admin/occasions')->with('flash_message', 'Occasion deleted!');
    }
}
