<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['venue_id', 'venue_image'];

    public function venues()
    {
        return $this->belongsTo('App\Venue','venue_id');
    }
}
