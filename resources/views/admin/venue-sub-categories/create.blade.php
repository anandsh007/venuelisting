@extends('layouts.backend')
@section('styles')
<link href="{{ asset('css/admin/fontawesome-iconpicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/font-awesome-picker-custom.css') }}" rel="stylesheet">
@endsection
@section('content')
@section('title','Venue Sub-Categories')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/admin/venue-sub-categories') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Create New Venue Sub-Category</h4>
                    </div>
                    <div class="card-body">
                    {!! Form::open(['url' => '/admin/venue-sub-categories', 'class' => 'form-horizontal', 'files' => true]) !!}

                    @include ('admin.venue-sub-categories.form')

                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('js/fontawesome-iconpicker.min.js') }}"></script>
<script>
  $(function () {
    
      $('.icp-auto').iconpicker();     
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".js-example-basic-multiple").select2();
});
</script>
@endsection
