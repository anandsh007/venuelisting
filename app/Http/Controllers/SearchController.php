<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Venue;
use App\VenueAmenity;
use App\Occasion;
use App\OccasionCategory;
use App\VenueCategory;
use App\VenueSubCategory;
use App\VenueFeature;
use App\VenueImage;
use App\VenueVideo;
use App\VenueAddress;
use App\PaymentOption;
use App\VenuePackage;
use App\Tag;
use App\Review;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request)
     {
        $venues = Venue::with('venueAmenities', 'venuePaymentOptions', 'venuePackages','venueSubCategories','occasions', 'venueFeatures', 'venueAddress', 'venueVideo','venueImages','venueTags','reviews')
                ->join('venue_occasions', 'venue_occasions.venue_id', '=', 'venues.id')
                ->join('occasions', 'occasions.id', '=', 'venue_occasions.occasion_id')
                ->join('venue_addresses','venue_addresses.venue_id', '=' , 'venues.id')
                ->join('venue_venue_amenities', 'venue_venue_amenities.venue_id', '=', 'venues.id')
                ->join('venue_amenities', 'venue_amenities.id', '=', 'venue_venue_amenities.venue_amenity_id')
                ->join('venue_venue_features', 'venue_venue_features.venue_id', '=', 'venues.id')
                ->join('venue_features', 'venue_features.id', '=', 'venue_venue_features.venue_feature_id')
                ->select('venues.*')->groupBy('venues.id')->distinct();
        $perPage = 12;
        $occasions;
        if($request->occasionId != null)
        {
            $occasionId = $request->occasionId;
            $venues->where('occasions.id', $occasionId);
        }
        if($request->latitude != null && $request->longitude != null)
        {
            if($request->radius !=null)
            {
               $radius = $request->radius;
            }
            else
            {
               $radius = 10;
            }

            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $venues->whereIn('venue_addresses.id',VenueAddress::select(DB::raw('*, ( 6367 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                ->having('distance', '<', $radius)
                ->orderBy('distance')
                ->pluck('id')
                ->toArray());
            // $venues->where('6367 * acos( cos( radians('.$latitude.') ) * cos( radians( venue_addresses.latitude ) ) * cos( radians( venue_addresses.longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( venue_addresses.latitude ) ) )','<', $radius);

        }       
        if($request->keywords != null)
        {
            $venues->where('venues.name','like','%'.$request->keywords.'%');
        }
        $amenitiesId = array();    
        if($request->amenities != null)
        {
            $amenitiesId[] = $request->amenities;
            $venues->whereIn('venue_amenities.id', $amenitiesId);
        }
        $featuresId = array();
        if($request->venue_features != null)
        {
            $featuresId[] = $request->venue_features;
            $venues->whereIn('venue_features.id', $featuresId);
        }  
        $venues = $venues->paginate($perPage);
        // print_r($venues);
        // die();     
        $occasionId = Occasion::pluck('name','id')->toArray();
        $venueAmenities = VenueAmenity::get();
        $venueFeatures = VenueFeature::get();
        return view('search.index',compact('occasionId','venueAmenities','venueFeatures','venues'));  
    }
} 
