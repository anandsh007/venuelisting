<?php

namespace App\Http\Controllers;
use App\Testimonial;
use App\VenueSubCategory;
use App\Occasion;
use App\Venue;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $exif = exif_read_data('images/GeoJot 2019-01-17 173012 small.jpg', 'IFD0');
        // echo $exif===false ? "No header data found.<br />\n" : "Image contains headers<br />\n";

        // $exif = exif_read_data('images/GeoJot 2019-01-17 173012 small.jpg', 0, true);
        // echo "test2.jpg:<br />\n";
        // // foreach ($exif as $key => $section) {
        // //     foreach ($section as $name => $val) {
        // //         echo "$key.$name: $val<br />\n";
        // //     }
        // // }
        // print_r($exif);
        // die();
        $featuredVenues = Venue::with('venueAmenities', 'venuePaymentOptions', 'venuePackages','venueSubCategories','occasions', 'venueFeatures', 'venueAddress', 'venueVideo','venueImages','venueTags','reviews')
                ->select('venues.*')
                ->join('featured_venues','featured_venues.venue_id','=','venues.id')
                ->get();
          foreach ($featuredVenues as $featuredVenue) 
          {
            if($featuredVenue->reviews != null && !($featuredVenue->reviews->isEmpty()))
            {
                $ratings = $featuredVenue->reviews->toArray();
                $ratingSum = array_sum(array_column($ratings, 'rating'));
                $ratingCount = count(array_column($ratings, 'rating'));
                $avgRating = (float) ($ratingSum/$ratingCount);
                $featuredVenue->avgRating = number_format($avgRating, 2, '.', ',');
            }   
            else
            {
                $featuredVenue->avgRating = 0;
            }
          }

        // $recentVenues = Venue::with(['venueImages', 'featuries', 'facilities','packages',
        //     'venueSubSubCategories', 'paymentOptions','ratings'])
        //         ->join('Districts', 'Venues.DistrictsId', '=', 'Districts.id')
        //         ->leftJoin('Pincodes', 'Venues.PincodesId', '=', 'Pincodes.id')
        //         ->join('VenueImages', 'Venues.Id', '=', 'VenueImages.VenueId')
        //         ->select('Venues.*')->distinct()
        //         ->orderBy('Venues.Created_at')->take(8)->get();

        // foreach ($recentVenues as $recentVenue)
        // {
        //     if($recentVenue->ratings!= null && !($recentVenue->ratings->isEmpty())){
        //         $ratings = $recentVenue->ratings->toArray();
        //         $ratingSum = array_sum(array_column($ratings, 'Rating'));
        //         $ratingCount = count(array_column($ratings, 'Rating'));
        //         $recentVenue->AvgRating = (int) ($ratingSum/$ratingCount);
        //     }
        //     else{
        //         $recentVenue->AvgRating = 0;
        //     }
        // }

        // $venueCategories = VenueCategory::with('venueSubCategories')
        //         ->with('venueSubSubCategories');
        $venueSubCategories = VenueSubCategory::all();
        $subCategories = VenueSubCategory::pluck('name','id')->toArray();
        $occasions = Occasion::take(6)->get();
        $occasionId = Occasion::pluck('name','id')->toArray();
        $testimonial = Testimonial::all();
        $count = sizeof($testimonial);

        return view('home.home', compact('testimonial','count','venueSubCategories','occasionId','featuredVenues','occasions'));
    }
    
    public function contactUs()
      {
              return view('home.contactus');
      }
    public function aboutUs()
      {
              return view('home.aboutus');
      }
    public function faq()
      {
              return view('home.faq');
      }
    public function pricing()
      {
              return view('home.pricing');
      }
    public function error404()
      {
              return view('home.error-404');
      }
    
}
