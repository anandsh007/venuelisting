<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occasion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'occasions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','font_awesome_icon', 'occasion_category_id','image'];

    public function occasionCategories()
    {
       return $this->belongsTo('App\OccasionCategory');
    }
    public function venues()
    {
        return $this->belongsToMany('App\Venue','venue_occasions');
    }
     public function venueEnquiries()
    {
        return $this->hasMany('App\Venue','venue_id');
    }
}
