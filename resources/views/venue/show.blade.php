@extends('layouts.app')
@section('styles')
    <link rel='dns-prefetch' href='//maps.googleapis.com' />
     {{-- <link rel="stylesheet" href="{{ URL::asset('css/showvenue.css') }}" type="text/css"> --}}
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rating/1.4.0/bootstrap-rating.min.css" rel="stylesheet" type="text/css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" type="text/css">
     <link rel="stylesheet" href="{{ URL::asset('css/opallisting.css') }}" type="text/css">
     <link rel="stylesheet" href="{{ URL::asset('css/kingcomposer.min.css') }}" type="text/css">
     <link rel="stylesheet" href="{{ URL::asset('css/prettyPhoto.css') }}" type="text/css">
     <link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.css') }}" type="text/css">
     <link rel="stylesheet" href="{{ URL::asset('css/owl.theme.css') }}" type="text/css">
     <link rel="stylesheet" href="{{ URL::asset('css/comment.css') }}" type="text/css">
     <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/magnific-popup.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('css/contact-form-styles.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css') }}">
     <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css">

     
      {{--<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-slider.min.css') }}" type="text/css"> --}}
<style>
.navbar-transparent-background{
    background:rgba(0, 0, 0, 0.5);
}
</style>
@endsection
@section('content')
    @php
      $fiveStar = $fourStar = $threeStar = $twoStar = $oneStar = $count = 0;
    @endphp
    @if(!$venue->reviews->isEmpty())
        @foreach($venue->reviews as $review)
            @php
               $count = $count + 1;
            @endphp
            @if($review->rating == 5)
              @php
                  $fiveStar = $fiveStar + 1;
              @endphp
            @elseif($review->rating == 4)
              @php
                 $fourStar = $fourStar + 1;
              @endphp
            @elseif($review->rating == 3)
              @php
                 $threeStar = $threeStar + 1;
              @endphp
            @elseif($review->rating == 2)
              @php
                 $twoStar = $twoStar + 1;
              @endphp
            @else
              @php
                $oneStar = $oneStar + 1;
              @endphp
            @endif
        @endforeach
        @php
         $perFive =  number_format((float)($fiveStar/$count*100), 2, '.', ',');
         $perFour = number_format((float)($fourStar/$count*100), 2, '.', ',');
         $perThree = number_format((float)($threeStar/$count*100), 2, '.', ',');
         $perTwo = number_format((float)($twoStar/$count*100), 2, '.', ',');
         $perOne = number_format((float)($oneStar/$count*100), 2, '.', ',');
        @endphp
    @else
        @php
        $perFive = 0.00;
        $perFour = 0.00;
        $perThree = 0.00;
        $perTwo = 0.00;
        $perOne = 0.00;
        @endphp
    @endif
    @php
        $avint = intval($venue->avgRating);
        $avdec1 = $avdec2 = abs($venue->avgRating - $avint);
    @endphp
<section id="main" class="site-main">

    <div class="opallisting-single-header">

        <div id="section-slider" class="section">
            <!-- Gallery -->

            <div class="owl-carousel-play opallisting-header-slider">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <img width="1920" height="700" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-3.jpg" alt="" /> </div>
                    <div class="item">
                        <img width="1920" height="700" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-1.jpg" alt="" /> </div>
                    <div class="item">
                        <img width="1920" height="700" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-2.jpg" alt="" /> </div>
                </div>
                <div class="container">
                    <div class="opallisting-place-meta">
                        <header>
                            <h1>{{ $venue->name }}</h1>
                            <p><i class="fas fa-map-marker-alt"></i> {{ $venue->venueAddress['area']}}, {{ $venue->venueAddress['city']}}, {{ $venue->venueAddress['state']}} </p>
                            <div class="rating pull-left">

                                <div class="comment-rating-wrap">
                                    <div class="rating" title="Rated 4 Star for listing">
                                        <span style="width:80%">
                                            @for( $i = 0; $i<5; $i++ )
                                                @if($i < $avint)
                                                    <i class="fas fa-star"></i>
                                                @elseif($avdec1 > 0)
                                                    <i class="fas fa-star-half-alt"></i>
                                                    @php 
                                                    $avdec1 = 0;
                                                    @endphp  
                                                @else
                                                    <i class="far fa-star"></i>
                                                @endif
                                            @endfor
                                        </span>
                                    </div>
                                    <div class="listing-count-reviews">({{ $count }} Review)</div>
                                </div>
                            </div>
                            <div class="favorite-button pull-right">
                                <a href="#" data-icon="star" data-id="11486" data-nonce="3e6cdc7f75" class="favorite  opallisting-need-login">
                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                    <span>Add to Wishlist</span>
                                </a>
                            </div>
                        </header>
                        <footer>
                            <div class="pull-left">
                                <a href="#">
                                    <img alt='' src='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png' srcset='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png 2x' class='avatar avatar-100 photo' height='100' width='100' />
                                </a>
                            </div>
                            <div class="pull-right">
                                <div class="claim-wrapper pull-left">

                                    <!-- BUTTON -->
                                    <a href="#opallisting-claim-place-id-11486" class="opallisting-place-claim-button submit">
                                        <i class="fas fa-hand-point-right"></i> Claim Listing</a>

                                    <!-- FORM -->
                                    <form id="opallisting-claim-place-id-11486" method="post" action="#" class="opallisting-claim-form opallisting-form white-popup-block mfp-hide">

                                        <h3 class="box-heading">Claim Listing</h3>

                                        <p class="">
                                            <input type="email" name="email" class="form-control" value="" placeholder="Email Address" />
                                        </p>

                                        <p class="">
                                            <textarea name="content" rows="4" cols="50" class="form-control textarea-style" placeholder="Messages"></textarea>
                                        </p>
                                        <p class="">
                                            <button type="submit" class="btn btn-primary submit">Submit claim</button>
                                        </p>

                                    </form>
                                </div>
                                <div class="share-wrapper pull-right">

                                    <div class="opallisting-popup">
                                        <a href="#" class="popup-header">
                                            <i class="fa fa-share" aria-hidden="true"></i> Share </a>
                                        <div class="pop-content white-popup-block mfp-hide">
                                            <a href="#" class="popup-close"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            <div class="md-overlay"></div>
                                            <ul class="share-list-container">
                                                <li>
                                                    <a href="#" class="opallisting-fb-share" data-text="Roy Thomson Hall"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="opallisting-google-share" data-text="Roy Thomson Hall"><i class="fa fa-google" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="opallisting-twitter-share" data-text="Roy Thomson Hall"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="opallisting-pinterest-share" data-text="Roy Thomson Hall"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#opallisting-email-share-form5b5b0eca6ebdf" class="opallisting-email-share" data-text="Roy Thomson Hall"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <form action="#" class="opallisting-email-share-form mfp-hide opallisting-form" method="POST" id="opallisting-email-share-form5b5b0eca6ebdf">
                                        <h3 class="box-heading">Share this place to your friends.</h3>
                                        <p class="form-row">
                                            <input type="email" name="recevice-email" id="recevice-email" placeholder="Friend Email" />
                                        </p>
                                        <p class="form-row">
                                            <input type="text" name="name" id="name" placeholder="Name" value="" />
                                        </p>
                                        <p class="form-row">
                                            <input type="email" name="email" id="email" placeholder="Your Email" value="" />
                                        </p>
                                        <p class="form-row">
                                            <textarea name="message" placeholder="Messages"></textarea>
                                        </p>
                                        <p class="form-row">
                                            <input type="hidden" id="opallisting-share-email-nonce" name="opallisting-share-email-nonce" value="4654e2b0cd" />
                                            <input type="hidden" name="_wp_http_referer" value="/wordpress/rentme/places/roy-thomson-hall/" />
                                            <input type="hidden" name="action" value="opallisting_share_place_via_email" />
                                            <button type="submit" class="opallisting-submit submit btn btn-primary">Send Message</button>
                                        </p>
                                    </form>
                                </div>
                                <div class="review-wrapper">
                                    <a href="#commentform" class="opallisting-review-button btn btn-primary">Write a Review</a>
                                        <form action="{{ route('review.store', $venue->id) }}" method="post" class="opallisting-comment-form white-popup-block mfp-hide" id="commentform">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="venue_id" value="{{ $venue->id }}">
                                            <p class="form-row comment-form-author">
                                                <div class="starrr col-md-8 "> 
                                                   Rating <input class="rating" name="rating" type="number" id="rating">
                                                </div>
                                            </p>
                                            <p class="form-row comment-form-author">
                                                <input class="form-control" name="reviewer_name" type="text" value="{{ old('reviewer_name') }}" placeholder="Your name" size="30" aria-required="true" />
                                            </p>
                                            <p class="form-row comment-form-title">
                                                <input class="form-control" name="summarize" type="text" value="{{ old('summarize') }}" placeholder="Summarize your opinion or hightlight interesting detail" />
                                            </p>
                                            <p class="form-row comment-form-comment">
                                                <textarea class="form-control textarea-style" name="text" cols="45" rows="8" aria-required="true" placeholder="Tell about your experience or leave a tip for others"></textarea>
                                            </p>
                                            <p class="form-submit">
                                                <input type="submit" class="submit" value="Submit" />
                                            </p>
                                        </form>                                            
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>

        {{-- <div id="section-map" class="section hide">
            <!-- Map -->

            <div class="owl-carousel-play opallisting-header-slider">

                <div class="opallisting-map-preview-wrapper">
                    <div id="opallisting-map-preview" class=" not-init" style="width:100%;height:600px;" data-lat="54.800685" data-lng="-4.130859" data-zoom="12"></div>
                    <ul class="opallisting-map-controls">
                        <li>
                            <a href="#" id="opallisting-toggle-geolocation" class="control geo-location opallisting-tiptip" data-tip="Geo Location">
                                <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="opallisting-toggle-lock-map" data-text="Lock" class="control lock opallisting-tiptip" data-tip="Toggle Lock">
                                <i class="fa fa-lock"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="opallisting-toggle-draw" class="control opallisting-tiptip" data-tip="Draw on Map">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="opallisting-toggle-direction" class="opallisting-tiptip" data-tip="Directions">
                                <i class="fa fa-exchange" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>

                    <div id="opallisting-directions-wrapper" class="opallisting-map-panel">
                        <form method="POST">
                            <header>
                                <a href="#" id="opallisting-close-directions-panel" data-tip="Close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>
                                <ul class="panel-actions">
                                    <li>
                                        <a href="#" data-mode="BICYCLING" class="opallisting-tiptip active" data-tip="Bicycling">
                                            <i class="fa fa-bicycle" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-mode="DRIVING" class="opallisting-tiptip" data-tip="Driving">
                                            <i class="fa fa-car" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-mode="TRANSIT" class="opallisting-tiptip" data-tip="Transit">
                                            <i class="fa fa-bus" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div class="form-body">
                                    <p class="form-row">
                                        <input type="text" name="start" id="start-point" placeholder="Enter starting point ... " />
                                    </p>
                                </div>
                            </header>
                            <ul id="opallisting-place-api-results">

                            </ul>
                        </form>
                    </div>

                    <form class="opallisting-save-search-form-overlay mfp-with-anim mfp-hide opallisting-form" id="opallisting-save-places-selection">
                        <header>
                            <h4 class="box-heading">Save this search</h4>
                        </header>
                        <div class="body">
                            <p class="form-row">
                                <input type="text" name="name" placeholder="Enter search name" />
                            </p>
                            <p class="form-row">
                                <input type="hidden" id="opallisting-save-nonce" name="opallisting-save-nonce" value="4d6bb61815" />
                                <input type="hidden" name="_wp_http_referer" value="/wordpress/rentme/places/roy-thomson-hall/" />
                                <input type="hidden" name="action" value="opallisting_save_search" />
                                <button type="submit" class="opallisting-submit submit btn btn-primary">Save</button>
                                <a href="#" id="opallisting-view-places-selection" class="opallisting-view-places-selection submit btn btn-primary">View Place Selection</a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="section-street-view" class="section hide">
            <!-- Map Street Views -->

            <div class="opallisting-header-map-street-view">

                <div class="opallisting-map-street-view-wrapper">
                    <div id="opallisting-map-street-view" class=" not-init" style="width:100%;height:600px;" data-class="opallisting-map-street-view" data-lat="54.800685" data-lng="-4.130859" data-zoom="12"></div>
                </div>
            </div>
        </div>

        <ul class="header-actions">
            <li>
                <a href="#" data-type="slider" class="active">
                    <i class="fa fa-camera" aria-hidden="true"></i>
                </a>
            </li>
            <li>
                <a href="#" data-type="map">
                    <i class="fa fa-map" aria-hidden="true"></i>
                </a>
            </li>
            <li>
                <a href="#" data-type="street-view">
                    <i class="fa fa-street-view" aria-hidden="true"></i>
                </a>
            </li>
        </ul>

    </div> --}}

    <section id="main-container" class="site-main container" role="main">
        <div class="row">

            <div class="row">
                <main id="primary" class="content content-area col-md-8 col-sm-8 col-xs-12">
                    <div class="single-opallisting-container">

                        <article id="post-11486" itemscope itemtype="http://schema.org/Place" class="version-2 post-11486 opallisting_place type-opallisting_place status-publish has-post-thumbnail hentry place_category-bars place_category-bbq place_category-bed-breakfast place_category-footwear place_category-hostels opallisting_tags-bar opallisting_tags-club opallisting_tags-foods opallisting_tags-vegetables opallisting_location-itali opallisting_amenities-daily-specials opallisting_amenities-reservations opallisting_amenities-restaurant opallisting_amenities-take-out">

                            <nav class="opallisting-tab-v2">

                                <ul class="opallisiting-nav-tabs">
                                    <li class="tab active">
                                        <a href="#place-description">
                                            <i class="fas fa-pen-nib"></i> Description </a>
                                    </li>
                                    <li class="tab">
                                        <a href="#place-video">
                                            <i class="fa fa-film" aria-hidden="true"></i> Video </a>
                                    </li>
                                   {{--  <li class="tab">
                                        <a href="#place-matterport">
                                                360º View                   </a>
                                    </li> --}}
                                    <li class="tab">
                                        <a href="#place-review">
                                            <i class="fas fa-star"></i></i> Reviews </a>
                                    </li>
                                </ul>

                            </nav>

                            <div class="content-box place-description active" id="place-description">
                                <div class="row opal-row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="place-content">
                                            <h3 class="place-title box-heading">{{ $venue->name }}</h3>
                                            <span id="venue-title">@php echo $venue->short_desc @endphp</span>
                                            <span id="venue-desc">@php echo $venue->long_desc @endphp </span>
                                            <div class="content-inline-box row opal-row">
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="list-group place-amenities opallisting-box">
                                                        <h4 class="list-group-item-heading">Maximum Capacity</h4>
                                                        <div class="list-group-item-text">
                                                            <div class="row opal-row">
                                                                    <div class="col-lg-4 col-md-4 active">
                                                                        <i class="fas fa-users"></i> {{ $venue->max_capacity }} 
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="list-group place-amenities opallisting-box">
                                                        <h4 class="list-group-item-heading">Categories</h4>
                                                        <div class="list-group-item-text">
                                                            <div class="row opal-row">
                                                                @foreach($venue->venueSubCategories as $venueSubCategory)
                                                                    <div class="col-lg-4 col-md-4 active">
                                                                        <i class="{{ $venueSubCategory->font_awesome_icon }}"></i> {{ $venueSubCategory->name }} 
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="list-group place-amenities opallisting-box">
                                                        <h4 class="list-group-item-heading">Amenities</h4>
                                                        <div class="list-group-item-text">
                                                            <div class="row opal-row">
                                                                @foreach($venue->venueAmenities as $venueAmenity)
                                                                    <div class="col-lg-4 col-md-4 active">
                                                                        <i class="{{ $venueAmenity->font_awesome_icon}}"></i> {{ $venueAmenity->name }} 
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="list-group place-amenities opallisting-box">
                                                        <h4 class="list-group-item-heading">Occasion It Host</h4>
                                                        <div class="list-group-item-text">
                                                            <div class="row opal-row">
                                                                @foreach($venue->occasions as $occasion)
                                                                    <div class="col-lg-4 col-md-4 active">
                                                                        <i class="{{ $occasion->font_awesome_icon }}"></i> {{ $occasion->name }} 
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="list-group place-amenities opallisting-box">
                                                        <h4 class="list-group-item-heading">Packages</h4>
                                                        <div class="list-group-item-text">
                                                            <div class="row opal-row">
                                                                @foreach($venue->venuePackages as $venuePackage)
                                                                <div class="col-lg-6 col-md-6 active">
                                                                    <span><i class="fa fa-check"></i>  {{$venuePackage->package_name}} : </span>
                                                                    <strong> <i class="fas fa-rupee-sign"></i> {{ $venuePackage->cost_per_person}} per person</strong>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="list-group place-amenities opallisting-box">
                                                        <h4 class="list-group-item-heading">Payment Options</h4>
                                                        <div class="list-group-item-text">
                                                            <div class="row opal-row">
                                                                @foreach($venue->venuePaymentOptions as $venuePaymentOption)
                                                                    <div class="col-lg-4 col-md-4 active">
                                                                        <i class="{{ $venuePaymentOption->font_awesome_icon }}"></i> {{ $venuePaymentOption->name }} 
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-12 col-xs-12">
                                                    <div class="place-tags">
                                                        <h4>Tags</h4>
                                                        @foreach($venue->venueTags as $venueTag)
                                                        <a href="#" title="{{ $venueTag->name }}">
                                                        {{ $venueTag->name }}           
                                                        </a>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- .place-content -->
                                    </div>
                                </div>
                            </div>
                            <div class="content-box place-video" id="place-video">
                                <div class="row opal-row">
                                    <div class="col-md-12 col-xs-12">

                                        <div class="place-video-session listing-panel" data-target="">
                                            <h3>Video</h3>
                                            <div class="box-info">
                                                <iframe width="810" height="456" src="https://www.youtube.com/embed/{{ $venue->venueVideo['video_name'] }}?feature=oembed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="content-box place-matterport" id="place-matterport">
                                <div class="row opal-row">
                                    <div class="col-md-12 col-xs-12">

                                        <div class="matterport">
                                            <h3 class="box-heading">360º View</h3>
                                            <iframe width="860" height="480" src="https://my.matterport.com/show/?m=uRGXgoiYk9f&#038;play=1&#038;qs=1"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="content-box place-review" id="place-review">
                                <div class="row opal-row">
                                    <div class="col-md-12 col-xs-12">
                                        <div id="opallisting-reviews">

                                            <div class="review-section row" id="opallisting-comments">
                                                <div class="col-md-12 col-xs-12 col-sm-12">
                                                    <header>
                                                        <h3 class="room-single-title">{{ $count }} Reviews                    
                                                            <a href="#opallisting-review-form-wrapper" class="opallisting-toggle-review write-review">Write a review</a>
                                                        </h3>
                                                    </header>
                                                </div>

                                                <div class="col-md-12 col-xs-12 col-sm-12">
                                                    <div class="review-section-container">
                                                        <div class="rating-summery">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <ul class="rating-box">
                                                                     
                                                                        <li>
                                                                            <span class="label">5</span>
                                                                            <span class="bar">
                                                                                <span style="width: {{$perFive}}%"></span>
                                                                            </span>
                                                                            <span class="percent">{{$perFive}}%</span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="label">4</span>
                                                                            <span class="bar">
                                                                                <span style="width: {{$perFour}}%"></span>
                                                                            </span>
                                                                            <span class="percent">{{$perFour}}%</span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="label">3</span>
                                                                            <span class="bar">
                                                                                <span style="width: {{$perThree}}%"></span>
                                                                            </span>
                                                                            <span class="percent">{{$perThree}}%</span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="label">2</span>
                                                                            <span class="bar">
                                                                                <span style="width: {{ $perTwo }}%"></span>
                                                                            </span>
                                                                            <span class="percent">{{ $perTwo }}%</span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="label">1</span>
                                                                            <span class="bar">
                                                                                <span style="width: {{ $perOne }}%"></span>
                                                                            </span>
                                                                            <span class="percent">{{ $perOne }}%</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="overall">
                                                                        <span class="averger">{{ $venue->avgRating }}</span>
                                                                        <div class="holder">
                                                                            Overall rating
                                                                            <p class="info">
                                                                                <div class="rating" title="">
                                                                                    <span style="width:80%">
                                                                                        @for( $i = 0; $i<5; $i++ )
                                                                                            @if($i < $avint)
                                                                                                <i class="fas fa-star"></i>
                                                                                            @elseif($avdec2 > 0)
                                                                                                <i class="fas fa-star-half-alt"></i>
                                                                                                @php 
                                                                                                $avdec2 = 0;
                                                                                                @endphp  
                                                                                            @else
                                                                                                <i class="far fa-star"></i>
                                                                                            @endif
                                                                                        @endfor
                                                                                    </span>
                                                                                </div>
                                                                                <span class="rating-based">based on all ratings</span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews">
                                                            <ol class="comment-list">
                                                                <li class="comment byuser comment-author-admin bypostauthor even thread-even depth-1 comment" id="li-comment-194">

                                                                    <div id="comment-194" class="comment_container">
                                                                      @if(!$venue->reviews->isEmpty())
                                                                        @foreach($venue->reviews as $review)
                                                                        <div class="comment-author">
                                                                            <div class="avatar avatar-40 photo">
                                                                               {{$review->reviewer_name[0]}}
                                                                            </div>  
                                                                        </div>

                                                                        <div class="comment-text">
                                                                            <strong class="author">{{ $review->reviewer_name }}</strong>

                                                                            <div class="meta">
                                                                                <time datetime="2017-04-27T07:49:12+00:00">
                                                                                    {{ $review->created_at }} </time>
                                                                                <div class="rating" title="Rated {{ $review->rating }} Star">
                                                                                    <span style="width:80%">
                                                                                        @for( $i = 0; $i<5; $i++ )
                                                                                            @if($i < $review->rating)
                                                                                                <i class="fas fa-star"></i>
                                                                                            @else
                                                                                                <i class="far fa-star"></i>
                                                                                            @endif
                                                                                        @endfor
                                                                                    </span>
                                                                                </div>
                                                                            </div>

                                                                            <span class="comment-title">"{{ $review->summarize }}"</span>
                                                                            <div class="comment-content">
                                                                                <p>{{ $review->text }}</p>
                                                                            </div>
                                                                        </div>
                                                                        @endforeach
                                                                        @else
                                                                        <div class="comment-text">
                                                                           <strong class="author">No Reviews!!</strong>
                                                                        </div>                                                        
                                                                        @endif
                                                                    </div>
                                                                </li>
                                                                </li>
                                                                <!-- #comment-## -->
                                                            </ol>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="review-section hide" id="opallisting-review-form-wrapper">
                                                <h3 class="room-single-title">Ratings and Reviews <a href="#opallisting-comments" class="opallisting-toggle-review close">Close</a></h3>
                                                <div class="review-section-container">
                                                    <div id="opallisting-review-form">
                                                        <div id="respond" class="comment-respond">
                                                            <h3 id="reply-title" class="comment-reply-title"> <small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel reply</a></small></h3>
                                                                {!! Form::open(['route' => ['review.store',$venue->id],'role' => 'form', 'class' => 'opallisting-comment-form','method'=>'POST','files' => true]) !!}
                                                                <div class="form-group">
                                                                    <div class="col-md-8 ">
                                                                        {!! Form::hidden('venue_id', $venue->id, ['class'=>'form-control']) !!} 
                                                                    </div>
                                                                </div>
                                                                 <p class="form-row comment-form-author">
                                                                    <div class="starrr col-md-8 "> 
                                                                       Rating <input class="rating" name="rating" type="number" id="rating">
                                                                    </div>
                                                                </p>
                                                                 <p class="form-row comment-form-author">
                                                                    <input id="author" class="form-control" name="reviewer_name" type="text" value="" placeholder="Your name" size="30" aria-required="true" />
                                                                </p>
                                                                <p class="form-row comment-form-title">
                                                                    <input id="title" class="form-control" name="summarize" type="text" value="" placeholder="Summarize your opinion or hightlight interesting detail" />
                                                                </p>
                                                                <p class="form-row comment-form-comment">
                                                                    <textarea id="comment" class="form-control textarea-style" name="text" cols="45" rows="8" aria-required="true" placeholder="Tell about your experience or leave a tip for others"></textarea>
                                                                </p>
                                                                <p class="form-submit">     
                                                                    <input type="submit" id="submit" class="submit" value="Submit" />
                                                                </p>
                                                            {!! Form::close() !!}
                                                        </div>
                                                        <!-- #respond -->
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="clear clearfix"></div>

                        </article>
                        <!-- #post-# -->

                        <h3 class="box-heading">You may also like</h3>
                       <div class="multiple-items">
                           <div class="item place-item">
                                <article itemscope="itemscope" itemtype="http://schema.org/Place" class="place-grid place-grid-v1">
                                    <header>
                                        <div class="place-box-image">
                                            <a href="#" class="place-box-image-inner">
                                                <img width="495" height="500" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/place-48-495x500.jpg" class="attachment-rentme_place_thumbnail size-rentme_place_thumbnail wp-post-image" alt="" /> </a>
                                        </div>

                                        <div class="place-actions">

                                            <a href="#" class="author-link"><img alt='' src='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png' srcset='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png 2x' class='avatar avatar-96 photo' height='96' width='96' /><span>admin</span></a>
                                            <a href="#" data-icon="heart" data-id="11487" data-nonce="4ba9518de2" class="favorite  opallisting-need-login">
                                                <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </header>

                                    <div class="place-content">

                                        <h4 class="place-title"><a href="#" rel="bookmark">The Reservoir Lounge</a></h4>

                                        <div class="place-address">
                                            <i class="fa fa-map-marker"></i> Gummersbach
                                        </div>

                                        <div class="place-type">
                                            <span class="fa fa-plane "></span>
                                            <span>Travel</span>
                                        </div>
                                        <div class="place-meta">

                                            <div class="comment-rating-wrap">
                                                <div class="comment-rating" title="Rated 0 Star for listing">
                                                    <span style="width:0%"></span>
                                                </div>
                                                <div class="listing-count-reviews">(0 Reviews)</div>
                                            </div>
                                            <span class="current-status  opening">
                                            Opening</span> 
                                        </div>
                                    </div>
                                    <!-- .place-content -->

                                    <meta itemprop="url" content="http://venusdemo.com/wordpress/rentme/places/the-reservoir-lounge/" />

                                </article>
                                <!-- #post-## -->
                            </div>
                            <div class="item place-item">
                                <article itemscope="itemscope" itemtype="http://schema.org/Place" class="place-grid place-grid-v1">

                                    <header>
                                        <div class="place-box-image">
                                            <a href="#" class="place-box-image-inner">
                                                <img width="495" height="500" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/place-46-495x500.jpg" class="attachment-rentme_place_thumbnail size-rentme_place_thumbnail wp-post-image" alt="" /> </a>
                                        </div>

                                        <div class="place-actions">

                                            <a href="#" class="author-link"><img alt='' src='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png' srcset='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png 2x' class='avatar avatar-96 photo' height='96' width='96' /><span>admin</span></a>
                                            <a href="#" data-icon="heart" data-id="11485" data-nonce="4ba9518de2" class="favorite  opallisting-need-login">
                                                <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </header>

                                    <div class="place-content">

                                        <h4 class="place-title"><a href="#" rel="bookmark">Ed Mirvish Theatre</a></h4>

                                        <div class="place-address">
                                            <i class="fa fa-map-marker"></i> 244 Victoria Street, London</div>

                                        <div class="place-type">
                                            <span class="fa fa-plane "></span>
                                            <span>Travel</span>
                                        </div>
                                        <div class="place-meta">

                                            <div class="comment-rating-wrap">
                                                <div class="comment-rating" title="Rated 0 Star for listing">
                                                    <span style="width:0%"></span>
                                                </div>
                                                <div class="listing-count-reviews">(0 Reviews)</div>
                                            </div>
                                            <span class="current-status  opening">
                                            Opening</span> 
                                        </div>
                                    </div>
                                    <!-- .place-content -->

                                    <meta itemprop="url" content="http://venusdemo.com/wordpress/rentme/places/ed-mirvish-theatre/" />

                                </article>
                                <!-- #post-## -->
                            </div>
                            <div class="item place-item">
                                <article itemscope="itemscope" itemtype="http://schema.org/Place" class="place-grid place-grid-v1">

                                    <header>
                                        <span class="featured">Featured</span>
                                        <div class="place-box-image">
                                            <a href="#" class="place-box-image-inner">
                                                <img width="495" height="500" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/place-45-495x500.jpg" class="attachment-rentme_place_thumbnail size-rentme_place_thumbnail wp-post-image" alt="" /> </a>
                                        </div>

                                        <div class="place-actions">

                                            <a href="#" class="author-link"><img alt='' src='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png' srcset='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png 2x' class='avatar avatar-96 photo' height='96' width='96' /><span>admin</span></a>
                                            <a href="#" data-icon="heart" data-id="11484" data-nonce="4ba9518de2" class="favorite  opallisting-need-login">
                                                <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </header>

                                    <div class="place-content">

                                        <h4 class="place-title"><a href="#" rel="bookmark">Art Gallery of Ontario</a></h4>

                                        <div class="place-address">
                                            <i class="fa fa-map-marker"></i> Ukraine Consulate</div>

                                        <div class="place-type">
                                            <span class="fa fa-plane "></span>
                                            <span>Travel</span>
                                        </div>
                                        <div class="place-meta">
                                            <div class="comment-rating-wrap">
                                                <div class="comment-rating" title="Rated 0 Star for listing">
                                                    <span style="width:0%"></span>
                                                </div>
                                                <div class="listing-count-reviews">(0 Reviews)</div>
                                            </div>
                                            <span class="current-status  opening">
                                            Opening</span> 
                                        </div>
                                    </div>
                                    <!-- .place-content -->

                                    <meta itemprop="url" content="http://venusdemo.com/wordpress/rentme/places/art-gallery-of-ontario/" />

                                </article>
                                <!-- #post-## -->
                            </div>
                            <div class="item place-item">
                                <article itemscope="itemscope" itemtype="http://schema.org/Place" class="place-grid place-grid-v1">

                                    <header>
                                        <div class="place-box-image">
                                            <a href="#" class="place-box-image-inner">
                                                <img width="495" height="500" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/03/place-24-495x500.jpg" class="attachment-rentme_place_thumbnail size-rentme_place_thumbnail wp-post-image" alt="" /> </a>
                                        </div>
                                        <div class="place-actions">
                                            <a href="#" class="author-link"><img alt='' src='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png' srcset='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png 2x' class='avatar avatar-96 photo' height='96' width='96' /><span>admin</span></a>
                                            <a href="#" data-icon="heart" data-id="11063" data-nonce="4ba9518de2" class="favorite  opallisting-need-login">
                                                <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </header>
                                    <div class="place-content">
                                        <h4 class="place-title"><a href="#" rel="bookmark">Spring Break Hot Spots</a></h4>
                                        <div class="place-address">
                                            <i class="fa fa-map-marker"></i> Japan Centre, Shaftesbury Avenue</div>
                                        <div class="place-type">
                                            <span class="fa fa-plane "></span>
                                            <span>Travel</span>
                                        </div>
                                        <div class="place-meta">
                                            <div class="comment-rating-wrap">
                                                <div class="comment-rating" title="Rated 5 Star for listing">
                                                    <span style="width:100%"></span>
                                                </div>
                                                <div class="listing-count-reviews">(1 Review)</div>
                                            </div>
                                            <span class="current-status  opening">
                                            Opening</span> 
                                        </div>
                                    </div>
                                    <!-- .place-content -->
                                    <meta itemprop="url" content="http://venusdemo.com/wordpress/rentme/places/spring-break-hot-spots/" />

                                </article>
                                <!-- #post-## -->
                            </div>
                       </div>
                    </div>
                </main>
                <!-- .site-main -->
                <aside class="sidebar sidebar-right col-md-4 col-sm-4 col-xs-12" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
                    <div id="opallisting_single_place_location_widget-3" class="widget widget_opallisting_single_place_location_widget">
                        <h2 class="widgettitle">Location</h2>
                        <div class="opallisting-box place-map-section widget-content">
                            <div class="place-map-section">
                                <div id="map_canvas">
                                    </div>
                                <div class="overlay">
                                    <ul class="sub-controls">
                                        <li>
                                            <a href="#" data-mode="BICYCLING">
                                                <i class="fa fa-bicycle" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-mode="DRIVING">
                                                <i class="fa fa-car" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-mode="TRANSIT">
                                                <i class="fa fa-bus" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="box-content">
                                <ul>
                                   {{--  <li>
                                        <a href="#" class="opallisting-show-direction">
                                            <i class="fa fa-location-arrow" aria-hidden="true"></i> Show directions </a>
                                    </li> --}}
                                    <li>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i> {{ $venue->venueAddress['address'] }} </li>
                                    <li class="phone">
                                        <i class="fa fa-phone" aria-hidden="true"></i> {{ $venue->mob_number }} </li>
                                    <li class="fax">
                                        <i class="fa fa-fax" aria-hidden="true"></i> {{ $venue->phone_number }} </li>
                                    <li class="email">
                                        <i class="fa fa-envelope" aria-hidden="true"></i> youremail@gmail.com </li>
                                </ul>
                            </div>

                            <ul class="social-meta">
                                <li>
                                    <a href="#" class="facebook">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="twitter">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="instagram">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="google-plus">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="youtube">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div id="opallisting_single_place_widget-3" class="widget widget_opallisting_single_place_widget">
                        <h2 class="widgettitle">Photo Gallery</h2>
                        <div class="opallisting-place-gallery widget-content">
                            <div class="row">

                                <div class="col-md-4">
                                    <a class="item" href="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-3.jpg">
                                        <img width="1920" height="700" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-3.jpg" class="attachment- size-" alt="" /> </a>
                                </div>

                                <div class="col-md-4">
                                    <a class="item" href="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-1.jpg">
                                        <img width="1920" height="700" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-1.jpg" class="attachment- size-" alt="" /> </a>
                                </div>

                                <div class="col-md-4">
                                    <a class="item" href="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-2.jpg">
                                        <img width="1920" height="700" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/slide-2.jpg" class="attachment- size-" alt="" /> </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div id="opallisting_open_hours_widget-4" class="widget widget_opallisting_open_hours_widget">
                        <div class="place-open-hours widget-content">
                            <h2 class="widgettitle">
                                <i class="far fa-clock"></i> Today <span class="status opening">Opening</span>
                                <span class="time"> 08:00 - 18:00 </span>
                            </h2>
                            <table class="place-open-hours-table">
                                <thead>
                                    <tr>
                                        <th>Day</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="">
                                        <td>Monday</td>
                                        <td>08:00 - 17:00</td>
                                    </tr>
                                    <tr class="">
                                        <td>Tuesday</td>
                                        <td>08:00 - 17:00</td>
                                    </tr>
                                    <tr class="">
                                        <td>Wednesday</td>
                                        <td>08:00 - 18:00</td>
                                    </tr>
                                    <tr class="">
                                        <td>Thursday</td>
                                        <td>08:00 - 18:00</td>
                                    </tr>
                                    <tr class="highlight">
                                        <td>Friday</td>
                                        <td>08:00 - 18:00</td>
                                    </tr>
                                    <tr class="">
                                        <td>Saturday</td>
                                        <td>08:00 - 17:30</td>
                                    </tr>
                                    <tr class="">
                                        <td>Sunday</td>
                                        <td>08:00 - 18:00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="opallisting_user_contact_widget-3" class="widget widget_opallisting_user_contact_widget">
                        <h2 class="widgettitle">Booking Enquiry</h2>
                        <div class="widget-content">

                            <form action="{{ route('enquiry.submitEnquiry') }}" class="" data-id="" method="post">
                                    {{ csrf_field() }}
                                <input type="hidden" class="" name="venue_id" value="{{ $venue->id }}" />
                                <p class="form-row {{ $errors->has('name') ? 'has-error' : ''}}" id="name-field">
                                    <label for="name">
                                        Name <abbr>*</abbr> </label>
                                    <input type="text" class="" name="name" id="name" value="{{ old('name') }}" placeholder="Your name" />
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </p>
                                <p class="form-row {{ $errors->has('email') ? 'has-error' : ''}}" id="email-field">
                                    <label for="email">
                                        Email <abbr>*</abbr> </label>
                                    <input type="text" class="" name="email" id="email" value="{{ old('email') }}" placeholder="Your@email.com" />
                                    {!! $errors->first('email', '<p class="help-block danger">:message</p>') !!}
                                </p>
                                <p class="form-row {{ $errors->has('number') ? 'has-error' : ''}}" id="subject-field">
                                    <label for="number">
                                        Contact No. <abbr>*</abbr> </label>
                                    <input type="text" class="" name="number" id="number" value="{{ old('number') }}" placeholder="Your Contact No." />
                                    {!! $errors->first('number', '<p class="help-block">:message</p>') !!}
                                </p>
                                <p class="form-row {{ $errors->has('number_of_gusets') ? 'has-error' : ''}}" id="subject-field">
                                    <label for="guest">
                                        Number of Guest <abbr>*</abbr> </label>
                                    <input type="number" class="" name="number_of_guests" id="guest" value="{{ old('number_of_guests') }}" placeholder="Number of Guest" />
                                    {!! $errors->first('number_of_gusets', '<p class="help-block">:message</p>') !!}
                                </p>
                                <p class="form-row {{ $errors->has('budget') ? 'has-error' : ''}}" id="subject-field">
                                    <label for="budget">
                                        Budget( <i class="fas fa-rupee-sign"></i> ) <abbr>*</abbr> </label>
                                    <input type="number" class="" name="budget" id="budget" value="{{ old('budget') }}" placeholder="Your Budget" />
                                    {!! $errors->first('budget', '<p class="help-block">:message</p>') !!}
                                </p>
                                <p class="form-row {{ $errors->has('occasion_id') ? 'has-error' : ''}}">
                                    <label for="OccasionId">Occasion</label>
                                    <select class="form-control" id="OccasionId" name="occasion_id"></select>
                                    {!! $errors->first('occasion_id', '<p class="help-block">:message</p>') !!}          
                                </p>
                                <p class="form-row {{ $errors->has('date') ? 'has-error' : ''}}" id="subject-field">
                                    <label for="subject">
                                        Date <abbr>*</abbr> </label>
                                    <input type="date" class="" name="date" id="subject" value="{{ old('date') }}" placeholder="Enter Occasion Date" />
                                    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
                                </p>
                                <p class="form-row submit">
                                    <button type="submit" class="btn btn-primary btn-block submit">Send Enquiry</button>
                                </p>

                            </form>

                        </div>
                    </div>
                </aside>
            </div>
        </div>

    </section>
    <!-- .content-area -->

</section>
@endsection
@section('scripts')
<script src="{{ URL::asset('js/rating.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.multiple-items').slick({
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 3,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/kingcomposer.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/map.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.form.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/script.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/scripts.js') }}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyAjYIJDSpRo90YUDZNtLnSCTmuMHfLMAlo"></script>
<script type="text/javascript" src="{{ asset('js/maps_lib.js') }}"></script>
<script type='text/javascript'>
//<![CDATA[
$(window).resize(function () {
  var h = $(window).height(),
  offsetTop = 220; // Calculate the top offset

  $('#map_canvas').css('height', (h - offsetTop));
  }).resize();

  $(function() {
    var myMap = new MapsLib({
      fusionTableId:      "1m4Ez9xyTGfY2CU6O-UgEcPzlS0rnzLU93e4Faa0",
      googleApiKey:       "AIzaSyAjYIJDSpRo90YUDZNtLnSCTmuMHfLMAlo",
      locationColumn:     "geometry",
      map_center:         [{{$venue->venueAddress['latitude']}},{{$venue->venueAddress['longitude']}}],
      defaultZoom:        14,
      addrMarkerImage:    "{{ asset('images/spotlight-poi2.png') }}",
      locationScope:      ""

    });
});
</script>
<script type="text/javascript">
    var owl = $('.owl-carousel');
    owl.owlCarousel({
            margin: 10,
            //loop: true,
            autoplay: true,
            smartSpeed: 500,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 1
                },
                992: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }

        })
      $('.venue-places').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })
$('.opallisting-place-claim-button').magnificPopup({
type:'inline',
midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});
$('.opallisting-review-button').magnificPopup({
type:'inline',
midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});
$('.popup-header').magnificPopup({
type:'inline',
midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});
</script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-rating-input.js') }}"></script>
<script>
    
$('.starrr').starrr({
  change: function(e, value){
    $('input[name="value"]').val(value);
  }
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script> 
<script>
    var occasionId;
    $('#OccasionId').select2({
          placeholder: 'Type Occassion..',
          ajax: {
              url: '{{ route('enquiry.getOccasion') }}',
              dataType: 'json',
              data: function (params) {
                  return {
                      q: $.trim(params.term)
                  };
              },
              processResults: function (data) {
                  return {
                      results: data
                  };
              },
              cache: true
          }
        });
</script>

@endsection
