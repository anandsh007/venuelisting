<?php

namespace App\Http\Controllers\Admin\Venue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VenueFeature;
use Illuminate\Http\Request;

class VenueFeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $venueFeatures = VenueFeature::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venueFeatures = VenueFeature::paginate($perPage);
        }

        return view('admin.venue-features.index', compact('venueFeatures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.venue-features.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        VenueFeature::create($requestData);

        return redirect('admin/venue-features')->with('flash_message', 'Venue Feature added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $venueFeatures = VenueFeature::findOrFail($id);

        return view('admin.venue-features.show', compact('venueFeatures'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $venueFeatures = VenueFeature::findOrFail($id);

        return view('admin.venue-features.edit', compact('venueFeatures'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $venueFeatures = VenueFeature::findOrFail($id);
        $venueFeatures->update($requestData);

        return redirect('admin/venue-features')->with('flash_message', 'Venue Feature updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        VenueFeature::destroy($id);

        return redirect('admin/venue-features')->with('flash_message', 'Venue Feature deleted!');
    }
}
