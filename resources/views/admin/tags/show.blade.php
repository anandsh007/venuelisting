@extends('layouts.backend')

@section('content')
    <div class="container">
        <div>
            <h3>Tag {{ $tag->id }}</h3>
            <div>
                <a href="{{ url('/admin/tags') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/admin/tags/' . $tag->id . '/edit') }}" title="Edit Tag"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/tags', $tag->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete Tag',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $tag->id }}</td>
                            </tr>
                            <tr><th> Name </th><td> {{ $tag->name }} </td></tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
