<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Review;
use App\Venue;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{

  public function store(Request $request)
	{
	   
	    $this->validate($request, [
			'text' => 'max:500|required',
			'venue_id' => 'integer|exists:venues,id',
			'rating' => 'numeric|required',
            'reviewer_name' => 'max:255|required',
            'summarize' => 'max:100|required'
		]);
        $requestData = $request->all();
        Review::create($requestData);
         return redirect()->route('venue.show',['id' => $request->venue_id]);
	       
	}

}