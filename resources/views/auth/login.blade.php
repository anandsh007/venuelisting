@extends('layouts.app')

@section('styles')
  <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.min.css" 
        type="text/css">

@endsection

@section('content')
<div id="listvenue-image" class="listvenue-image-margin">
        <div class="container">
            <h2 class="logheading">Login</h2>
        </div>

    </div>
    @for($i=0; $i<=5; $i++)
        <br>
    @endfor
<div class="container kk">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading logpanel">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" id="login-form">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="ex: myname@example.com">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="*******">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <span>
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                    <label>
                                    <a class="nav-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                                    </label>
                                    </span>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                              <label for="password" class="col-md-4 control-label"></label>
                        <div class="col-md-6">
                          {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Login', 
                             ['class' => 'btn btn-primarylogin btn-default']) !!}
                             
                             <li class='btn btn-default waves-effect waves-lightn'><a style="color: white;" href="{{ url('/register') }}">SignUp</a></li>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">                                
                            <div class="text-center">
                                 <a class="btn btn-social btn-lg btn-google" 
                                    onclick="location.href='{{ url('login/google') }}';">
                                     <i class="fab fa-google"></i>Sign in with Google
                                 </a>
                            </div>
                            </div>
                        </div>
                             <div class="form-group">
                                   <div class="text-center">
                                 <a class="btn btn-social btn-lg btn-facebook" 
                                    onclick="location.href='{{ url('login/facebook') }}';">
                                     <i class="fab fa-facebook-f"></i>Sign in with Facebook
                                 </a>
                            </div>                                 
                             </div>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection