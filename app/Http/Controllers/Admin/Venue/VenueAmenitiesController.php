<?php

namespace App\Http\Controllers\Admin\Venue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VenueAmenity;
use Illuminate\Http\Request;

class VenueAmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $venueamenities = VenueAmenity::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venueamenities = VenueAmenity::paginate($perPage);
        }

        return view('admin.venue-amenities.index', compact('venueamenities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.venue-amenities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        VenueAmenity::create($requestData);

        return redirect('admin/venue-amenities')->with('flash_message', 'VenueAmenity added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $venueamenity = VenueAmenity::findOrFail($id);

        return view('admin.venue-amenities.show', compact('venueamenity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $venueamenity = VenueAmenity::findOrFail($id);

        return view('admin.venue-amenities.edit', compact('venueamenity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $venueamenity = VenueAmenity::findOrFail($id);
        $venueamenity->update($requestData);

        return redirect('admin/venue-amenities')->with('flash_message', 'VenueAmenity updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        VenueAmenity::destroy($id);

        return redirect('admin/venue-amenities')->with('flash_message', 'VenueAmenity deleted!');
    }
}
