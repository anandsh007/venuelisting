<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueSubCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue_sub_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','font_awesome_icon'];

    public function venueCategories()
    {
        return $this->belongsToMany('App\VenueCategory','venue_category_sub_categories')->withTimestamps();
    }

    public function venueVenueSubCategories()
    {
        return $this->belongsToMany('App\Venue','venue_venue_subcategories')->withTimestamps();
    }
}
