@extends('layouts.backend')
@section('content')
@section('title','Venue Categories')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @include('shared.message')
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Venue Categories</h4>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/venue-categories/create') }}" class="btn btn-success btn-sm" title="Add New VenueCategory">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/venue-categories', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group no-border">
                            <input type="text" value="" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                        {!! Form::close() !!}
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead class="text-primary">
                                    <tr>
                                        <th>#</th><th>Name</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($venuecategories as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>
                                            <a href="{{ url('/admin/venue-categories/' . $item->id) }}" title="View VenueCategory"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/admin/venue-categories/' . $item->id . '/edit') }}" title="Edit VenueCategory"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/venue-categories', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'Delete VenueCategory',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $venuecategories->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection