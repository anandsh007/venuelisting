@extends('layouts.app')

@section('styles')

    <link rel="stylesheet" href="{{ URL::asset('css/kingcomposer.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css">
    {{--<link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/owl.theme.css') }}" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" type="text/css"> --}}
    <style>
       /*.progress-item span {
            color: rgba(0, 0, 0, 0.92);
            /*font-weight: 700;*/
            text-transform: uppercase;
        }*/
/*================================== parallax css ===========*/
      /* .kc-css-250453 {
           background: transparent url(images/apple-iphone-books-desk.jpg)
           0% 0%/cover no-repeat fixed;
       }
       .kc-css-676349 .kc_title{
           color: #FFFFFF;
       }
       .kc-css-980767 .kc-cta-desc h2 {
           color: #ffffff;
       }
       .kc-css-980767 .kc-cta-desc .kc-cta-text {
           color: rgba(255, 255, 255, 0.72);
       }
       .team-v2 .team-body {
           padding: 19px;
           background: #fff;
       }*/
    </style>
@endsection
@section('content')
<div class="">
<div class="row">
    <section id="opal-breadscrumb" class="opal-breadscrumb" style="">
        <div class="container">
            <h2 class="navheading">Pricing</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('home.aboutUs')}}">Home</a> </li><span></span>
                <li>Pricing</li>
            </ol>
        </div>
    </section>
    <section class="kc-elm kc-css-467137 kc_row pricing-page">
    <div class="kc-row-container  kc-container">
        <div class="kc-wrap-columns">
            <div class="kc-elm kc-css-26518 kc_col-sm-4 kc_column kc_col-sm-4">
                <div class="kc-col-container">
                    <div class="pricing pricing-v3  ">
                        <div class="pricing-header">
                            <div class="pricing-header-inner">
                                <h4 class="plan-title">
                <span>Basic</span>
            </h4>
                                <div class="featured-image"><img src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2016/09/icon-1.png" alt="Basic"></div>

                                <div class="plan-price-wrap">
                                    <div class="plan-price-block">
                                        <div class="plan-price">
                                            <div class="plan-price-inner">
                                                <span class="plan-currency"></span>
                                                <span class="plan-figure">Free</span>
                                                <p>month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="pricing-body">
                            <div class="description">Standard listing submission, active for 30 days.</div>
                            <div class="plain-info">
                                <div><strong>One</strong> Listing</div>
                                <div><strong>30 Days</strong> Availability</div>
                                <div><strong>Standard</strong> Listing</div>
                                <div><strong>Limited</strong> Support</div>
                            </div>
                        </div>
                        <div class="pricing-footer">
                            <a class="btn btn-md btn-default radius-6x btn-3d" href="http://yourdomain.com">SignUp</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kc-elm kc-css-685300 kc_col-sm-4 kc_column kc_col-sm-4">
                <div class="kc-col-container">
                    <div class="pricing pricing-v3  ">
                        <div class="pricing-header">
                            <div class="pricing-header-inner">
                                <h4 class="plan-title">
                <span>Professional</span>
            </h4>
                                <div class="featured-image"><img src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2016/09/icon-2.png" alt="Professional"></div>

                                <div class="plan-price-wrap">
                                    <div class="plan-price-block">
                                        <div class="plan-price">
                                            <div class="plan-price-inner">
                                                <span class="plan-currency"></span>
                                                <span class="plan-figure">$49.00</span>
                                                <p>month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="pricing-body">
                            <div class="description">One Time Fee for one listing, highlighted in the search results.</div>
                            <div class="plain-info">
                                <div><strong>One</strong> Listing</div>
                                <div><strong>Unlimited</strong> Availability</div>
                                <div><strong>Featured</strong> In the Results</div>
                                <div><strong>24/7</strong> Support</div>
                            </div>
                        </div>
                        <div class="pricing-footer">
                            <a class="btn btn-md btn-default radius-6x btn-3d" href="http://yourdomain.com">SignUp</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kc-elm kc-css-251136 kc_col-sm-4 kc_column kc_col-sm-4">
                <div class="kc-col-container">
                    <div class="pricing pricing-v3  ">
                        <div class="pricing-header">
                            <div class="pricing-header-inner">
                                <h4 class="plan-title">
                <span>Business</span>
            </h4>
                                <div class="featured-image"><img src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2016/09/icon-3.png" alt="Business"></div>

                                <div class="plan-price-wrap">
                                    <div class="plan-price-block">
                                        <div class="plan-price">
                                            <div class="plan-price-inner">
                                                <span class="plan-currency"></span>
                                                <span class="plan-figure">$59.00</span>
                                                <p>month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="pricing-body">
                            <div class="description">Subscription Based for unlimited listings and availability.</div>
                            <div class="plain-info">
                                <div><strong>Unlimited</strong> Listings</div>
                                <div><strong>Unlimited</strong> Availability</div>
                                <div><strong>Featured</strong> In the Results</div>
                                <div><strong>24/7</strong> Support</div>
                            </div>
                        </div>
                        <div class="pricing-footer">
                            <a class="btn btn-md btn-default radius-6x btn-3d" href="http://yourdomain.com">SignUp</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/kingcomposer.min.js') }}"></script>
@endsection
   