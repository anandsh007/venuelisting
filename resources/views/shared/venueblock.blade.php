<div class=''>
    <a href="{{URL::to('/showvenue')}}/{{preg_replace('/[^A-Za-z0-9]/', '',
                $venue->Name)}}-{{ preg_replace('/[^A-Za-z0-9]/', '', $venue->Address)}}-{{$venue->id}}">

        {{--<div class="inner-block hover" >--}}
             {{--<div id="">--}}

                <div class="header">
                  
                      @if(!empty($venue->venueImages))

                        @foreach($venue->venueImages as $venueImage)
                        @endforeach
                            <img class="ns-img" src="../storage{!!$venueImage->ImagePath!!}"
                                 alt="{{$venueImage->ImageName}}" >
                        
                        {{--@endforeach --}}
                        @else:
                        <div class="alert">No records found.</div>
                        @endif

                   
<!--                    <div class="navsWrapper">
                        <div id="ninja-slider-prev"></div>
                        <div id="ninja-slider-next"></div>
                    </div>-->
                </div>

            <div class="box-block-view">

                <h4 class="place-title-block hover-block-text">
                    <a href="" rel="bookmark">{{$venue->Name}}</a></h4>

                <div class="place-address-block">
                    <i class="fa fa-map-marker"></i>&nbsp;{{$venue->Address}}</div>

                <div class="place-bottom-block">
                    <div class="comment-rating-block-wrap-block">
                        <div class="comment-rating-block" title="Rated 0 Star for listing">
                            @for( $i = 0; $i<5; $i++ )
                                @if($i < $venue->AvgRating)
                                    <i class="fa fa-star"  aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-star-o"  aria-hidden="true"></i>
                                @endif
                            @endfor
                        </div>
                        <div class="listing-count-reviews-block"> @if($venue->MaximumCapacity != '')
                                <div>
                                    <i class="fa fa-user icon-size-venue-block"style="padding-left: 20px;"></i> {{$venue->MaximumCapacity}}
                                    @else
                                        <br>
                                    @endif
                                    {{--<span class="opening-blcok-element"style="padding-left: 20px;">--}}
                    @if(!empty($venue->packages))

                                    @foreach($venue->packages->slice(0,1) as $package)
                                        <span class="opening-blcok-element fa fa-rupee"
                                              style="padding-left: 10px;">&nbsp;{{$package->Cost}}
                                        </span>
                                    @endforeach
                                @else
                                    <br>
                                @endif
                                 {{--</span>--}}
                                </div>
                            {{--@else--}}
                                {{--<br>--}}
                            {{--@endif--}}
                            {{--<span class="opening-blcok-element fa fa-inr">--}}
                    {{--@if(!empty($venue->packages))--}}

                                    {{--@foreach($venue->packages as $package)--}}
                                        {{--<span class=""> {{$package->Cost}}</span>--}}
                                    {{--@endforeach--}}
                                {{--@else--}}
                                    {{--<br>--}}
                                {{--@endif--}}
                {{--</span>--}}
                        </div>
                    </div>
                </div>
            </div>
            {{--====================================================--}}
            {{--<div class="venue-text-block row">--}}
                {{--<div class="col-md-8 text-left">--}}
                    {{--<div class="home1 ">{{$venue->Name}}</div>--}}
                        {{--<div class="star home2">--}}
                        {{--@for( $i = 0; $i<5; $i++ )--}}
                            {{--@if($i < $venue->AvgRating)--}}
                                {{--<i class="fa fa-star"  aria-hidden="true"></i>--}}
                            {{--@else--}}
                                {{--<i class="fa fa-star-o"  aria-hidden="true"></i>--}}
                            {{--@endif--}}
                        {{--@endfor--}}
                       {{--</div>--}}
                        {{--<div class="home2">  <span class="fa fa-map-marker"> </span> --}}
                            {{--{{$venue->Address}}</div>--}}


                    {{--</div>--}}
                {{--<div class="col-md-4 text-right">--}}

                    {{--<br> --}}
                     {{--@if($venue->MaximumCapacity != '')--}}
                         {{--<div>--}}
                             {{--<i class="fa fa-user icon-size-venue-block"></i> {{$venue->MaximumCapacity}}--}}
                         {{--</div>--}}
                     {{--@else--}}
                         {{--<br> --}}
                     {{--@endif--}}

                     {{--<span class="fa fa-inr"> </span>--}}
                     {{--@if(!empty($venue->packages))              --}}

                     {{--@foreach($venue->packages as $package)--}}
                       {{--<span class=""> {{$package->Cost}}</span>--}}
                     {{--@endforeach --}}
                     {{--@else--}}
                         {{--<br> --}}
                     {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}


        {{--</div>--}}
        {{--</div>--}}
    </a>
</div>
