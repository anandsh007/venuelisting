<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueEnquiry extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue_enquiries';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','email','number','budget','date','number_of_guests','venue_id','occasion_id'];

    public function venues()
    {
      return $this->belongsTo('App\Venue','venue_id');
    }
    
    public function occasions()
    {
      return $this->belongsTo('App\Occasion','occasion_id');
    }

    
}