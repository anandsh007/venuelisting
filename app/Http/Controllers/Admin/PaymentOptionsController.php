<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PaymentOption;
use Illuminate\Http\Request;

class PaymentOptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $paymentOptions = PaymentOption::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $paymentOptions = PaymentOption::paginate($perPage);
        }

        return view('admin.payment-options.index', compact('paymentOptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.payment-options.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        PaymentOption::create($requestData);

        return redirect('admin/payment-options')->with('flash_message', 'Payment Option added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paymentOptions = PaymentOption::findOrFail($id);

        return view('admin.payment-options.show', compact('paymentOptions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paymentOptions = PaymentOption::findOrFail($id);

        return view('admin.payment-options.edit', compact('paymentOptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $paymentOptions = PaymentOption::findOrFail($id);
        $paymentOptions->update($requestData);

        return redirect('admin/payment-options')->with('flash_message', 'Payment Option updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PaymentOption::destroy($id);

        return redirect('admin/payment-options')->with('flash_message', 'Payment Option deleted!');
    }
}
