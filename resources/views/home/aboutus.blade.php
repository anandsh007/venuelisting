@extends('layouts.app')
@section('styles')
<link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/owl.theme.css') }}" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" type="text/css">
<style>
.progress-item span {
color: rgba(0, 0, 0, 0.92);
/*font-weight: 700;*/
text-transform: uppercase;
}
/*================================== parallax css ===========*/
.kc-css-250453 {
background: transparent url(images/apple-iphone-books-desk.jpg)
0% 0%/cover no-repeat fixed;
}
.kc-css-676349 .kc_title{
color: #FFFFFF;
}
.kc-css-980767 .kc-cta-desc h2 {
color: #ffffff;
}
.kc-css-980767 .kc-cta-desc .kc-cta-text {
color: rgba(255, 255, 255, 0.72);
}
.team-v2 .team-body {
padding: 19px;
background: #fff;
}
</style>
@endsection
@section('content')
<div class="">
    <div class="row">
        <section id="opal-breadscrumb" class="opal-breadscrumb" style="">
            <div class="container">
                <h2 class="navheading">About Us</h2>
                <ol class="breadcrumb">
                    <li><a href="{{route('home.aboutUs')}}">Home</a> </li><span></span>
                    <li>About Us</li>
                </ol>
            </div>
        </section>
        <section id="main-container" class="container-fluid inner">
            <div class="row">
                <div id="main-content" class="main-content col-xs-12 col-lg-12 col-md-12">
                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <article id="post-9808" class="post-9808 page type-page status-publish hentry">
                                <div class="entry-content-page">
                                    <div class="kc_clfw"></div>
                                    <section class="kc-elm kc-css-575820 kc_row" style="margin-top: -100px;">
                                        <div class="kc-row-container  kc-container">
                                            <div class="kc-wrap-columns">
                                                <div class="kc-elm kc-css-315328 kc_col-sm-12 kc_column kc_col-sm-12">
                                                    <div class="kc-col-container">
                                                        <div class="element-block-heading style-v3">
                                                            <div class="inner">
                                                                <h2 class="heading">Our Great Story</h2>
                                                                <h4 class="sub-heading">See and visit interesting places. Share experiences with your friends. Simply.</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="kc-elm kc-css-708725 kc_row">
                                        <div class="kc-row-container  kc-container">
                                            <div class="kc-wrap-columns">
                                                <div class="kc-elm kc-css-74865 kc_col-sm-4 kc_column kc_col-sm-4">
                                                    <div class="kc-col-container">
                                                        <div class="kc-elm kc-css-484882 kc_text_block">
                                                            <p>VenueListing.com is an online venue booking portal with the widest range of venues available in Delhi, Gurgaon, Noida, Faridabad, and Ghaziabad. At VenueListing, we connect our customers with the best venues in their city based on their unique requirements so that they can get the ideal deal for their event. We believe that online venue booking is just as personal as offline venue booking, except much simpler and our goal is to provide hassle-free online venue booking experience to our customers, all the way from browsing and shortlisting venues to booking the final one.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kc-elm kc-css-955800 kc_col-sm-8 kc_column kc_col-sm-8">
                                                    <div class="kc-col-container">
                                                        <div class="kc-elm kc-css-216001 kc_shortcode kc_progress_bars " data-style="1">
                                                            <div class="progress-item"><span class="label">Venues</span>
                                                            <div class="kc-ui-progress-bar kc-ui-progress-bar1 kc-progress-bar kc-ui-container">
                                                                <div class="kc-ui-progress kc-ui-progress1" style="background-color: #c5d244;width: 73%" data-value="73" data-speed="2000">
                                                                    <div class="ui-label">
                                                                        <span class="value" style="color: #ffffff">73%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="progress-item"><span class="label">Vendors</span>
                                                        <div class="kc-ui-progress-bar kc-ui-progress-bar1 kc-progress-bar kc-ui-container">
                                                            <div class="kc-ui-progress kc-ui-progress1" style="background-color: #c5d244;width: 95%" data-value="95" data-speed="2000">
                                                                <div class="ui-label">
                                                                    <span class="value" style="color: #ffffff">95%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progress-item"><span class="label">Cities</span>
                                                    <div class="kc-ui-progress-bar kc-ui-progress-bar1 kc-progress-bar kc-ui-container">
                                                        <div class="kc-ui-progress kc-ui-progress1" style="background-color: #c5d244;width: 61%" data-value="61" data-speed="2000">
                                                            <div class="ui-label">
                                                                <span class="value" style="color: #ffffff">61%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-item"><span class="label">Customers</span>
                                                <div class="kc-ui-progress-bar kc-ui-progress-bar1 kc-progress-bar kc-ui-container">
                                                    <div class="kc-ui-progress kc-ui-progress1" style="background-color: #c5d244;width: 82%" data-value="82" data-speed="2000">
                                                        <div class="ui-label">
                                                            <span class="value" style="color: #ffffff">82%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="kc-elm kc-css-581077 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-215926 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="element-block-heading style-v3">
                                            <div class="inner">
                                                <h2 class="heading">Our Team</h2>
                                                <h4 class="sub-heading">See and visit interesting places. Share experiences with your friends. Simply.</h4>
                                            </div>
                                        </div>
                                        <div class="team-collection">
                                            <div class="col-md-12" style="text-align: center;margin-left: 86px;">
                                                <div class="row">
                                                    <div id="carousel-766489701" class="owl-carousel-play" data-ride="owlcarousel">
                                                        <div class="owl-carousel  owl-theme" data-slide="4" data-pagination="true" data-navigation="true" style="opacity: 1; display: block;">
                                                            <div>
                                                                <div style="display: block;">
                                                                    <div class="owl-item col-md-3" style="width: 260px;">
                                                                        <div class="item">
                                                                            <!-- start items -->
                                                                            <div class="team-v2">
                                                                                <div class="team-header">
                                                                                    <a href="">
                                                                                    <img width="450" height="450" src="{{asset('images/team-1.jpg')}}" class="attachment-full size-full wp-post-image" alt=""> </a>
                                                                                    <div class="bo-social-icons">
                                                                                        <a class="bo-social-white radius-x" href="#"> <i class="fab fa-facebook-f"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"><i class="fab fa-twitter"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"><i class="fab fa-pinterest-p"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"> <i class="fab fa-google"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="team-body">
                                                                                    <div class="team-body-content">
                                                                                        <h3 class="team-name"><a href="">Arunima Singh</a></h3>
                                                                                        <p>Marketing Executive</p>
                                                                                    </div>
                                                                                    <div class="team-info">
                                                                                        <div class="team-email">
                                                                                            <a href="">arunima@gmail.com</a>
                                                                                        </div>
                                                                                        <div class="team-phone">
                                                                                        9635966545 </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="team-des">
                                                                                    <p>Morbi ac neque at mi elementum gravida et vitae elit. Etiam ullamcorper auctor orci, id luctus felis laoreet a. Nam nec nulla sit amet tellus pretium rhoncus</p>
                                                                                </div>
                                                                            </div>
                                                                            <!-- end items -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="owl-item col-md-3" style="width: 260px;">
                                                                        <div class="item">
                                                                            <!-- start items -->
                                                                            <div class="team-v2">
                                                                                <div class="team-header">
                                                                                    <a href="">
                                                                                    <img width="450" height="450" src="{{asset('images/team-2.jpg')}}" class="attachment-full size-full wp-post-image" alt=""> </a>
                                                                                    <div class="bo-social-icons">
                                                                                        <a class="bo-social-white radius-x" href="#"> <i class="fab fa-facebook-f"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"><i class="fab fa-twitter"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"><i class="fab fa-pinterest-p"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"> <i class="fab fa-google"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="team-body">
                                                                                    <div class="team-body-content">
                                                                                        <h3 class="team-name"><a href="">Aparna Shukla</a></h3>
                                                                                        <p>sales Executive</p>
                                                                                    </div>
                                                                                    <div class="team-info">
                                                                                        <div class="team-email">
                                                                                            <a href="mailto:ashishasthana95@domain.com">aparna95@gmail.com</a>
                                                                                        </div>
                                                                                        <div class="team-phone">
                                                                                        9865963256</div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="team-des">
                                                                                    <p>Morbi ac neque at mi elementum gravida et vitae elit. Etiam ullamcorper auctor orci, id luctus felis laoreet a. Nam nec nulla sit amet tellus pretium rhoncus.</p>
                                                                                </div>
                                                                            </div>
                                                                            <!-- end items -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="owl-item col-md-3" style="width: 260px;">
                                                                        <div class="item">
                                                                            <!-- start items -->
                                                                            <div class="team-v2">
                                                                                <div class="team-header">
                                                                                    <a href="">
                                                                                    <img width="450" height="450" src="{{asset('images/team-3.jpg')}}" class="attachment-full size-full wp-post-image" alt=""> </a>
                                                                                    <div class="bo-social-icons">
                                                                                        <a class="bo-social-white radius-x" href="#"> <i class="fab fa-facebook-f"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"><i class="fab fa-twitter"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"><i class="fab fa-pinterest-p"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"> <i class="fab fa-google"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="team-body">
                                                                                    <div class="team-body-content">
                                                                                        <h3 class="team-name"><a href="">Ashish Goel</a></h3>
                                                                                        <p>CEO</p>
                                                                                    </div>
                                                                                    <div class="team-info">
                                                                                        <div class="team-email">
                                                                                            <a href="">ashish@gmail.com</a>
                                                                                        </div>
                                                                                        <div class="team-phone">
                                                                                        9465856325</div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="team-des">
                                                                                    <p>Morbi ac neque at mi elementum gravida et vitae elit. Etiam ullamcorper auctor orci, id luctus felis laoreet a. Nam nec nulla sit amet tellus pretium rhoncus.</p>
                                                                                </div>
                                                                            </div>
                                                                            <!-- end items -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="owl-item col-md-3" style="width: 260px;">
                                                                        <div class="item">
                                                                            <!-- start items -->
                                                                            <div class="team-v2">
                                                                                <div class="team-header">
                                                                                    <a href="">
                                                                                    <img width="450" height="450" src="{{asset('images/team-4.jpg')}}" class="attachment-full size-full wp-post-image" alt=""> </a>
                                                                                    <div class="bo-social-icons">
                                                                                        <a class="bo-social-white radius-x" href="#"> <i class="fab fa-facebook-f"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"><i class="fab fa-twitter"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"><i class="fab fa-pinterest-p"></i> </a>
                                                                                        <a class="bo-social-white radius-x" href="#"> <i class="fab fa-google"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="team-body">
                                                                                    <div class="team-body-content">
                                                                                        <h3 class="team-name"><a href="">Anand Verma</a></h3>
                                                                                        <p>Area Manager</p>
                                                                                    </div>
                                                                                    <div class="team-info">
                                                                                        <div class="team-email">
                                                                                            <a href="">anand@gmail.com</a>
                                                                                        </div>
                                                                                        <div class="team-phone">
                                                                                        9632145685</div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="team-des">
                                                                                    <p>Morbi ac neque at mi elementum gravida et vitae elit. Etiam ullamcorper auctor orci, id luctus felis laoreet a. Nam nec nulla sit amet tellus pretium rhoncus. </p>
                                                                                </div>
                                                                            </div>
                                                                            <!-- end items -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section data-kc-equalheight="true" data-kc-equalheight-align="middle" class="kc-elm kc-css-250453 kc_row relative" style="margin-top: 43px;">
                        <div class="kc-row-container  kc-container  container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-84877 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div id=" " class="kc-elm kc-css-472748 kc_row kc_row_inner">
                                            <div class="kc-elm kc-css-574081 kc_col-sm-12 kc_column_inner kc_col-sm-12">
                                                <div class="kc_wrapper kc-col-inner-container">
                                                    <div class="kc-elm kc-css-935432" style="height: 90px; clear: both; width:100%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kc-elm kc-css-616208 kc_row kc_row_inner">
                                            <div class="kc-elm kc-css-960303 kc_col-sm-12 kc_column_inner kc_col-sm-12">
                                                <div class="kc_wrapper kc-col-inner-container">
                                                    <div class="kc-elm kc-css-676349 kc-title-wrap align-center align-center">
                                                        <h2 class="kc_title title_style_1">WHAT OUR CLIENTS SAY</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kc-elm kc-css-888608 kc_row kc_row_inner">
                                            <div class="kc-elm kc-css-967186 kc_col-sm-12 kc_column_inner kc_col-sm-12">
                                                <div class="kc_wrapper kc-col-inner-container">
                                                    <div class="kc-elm kc-css-980767 kc-call-to-action kc-cta-2 kc-is-button">
                                                        <div class="kc-cta-desc">
                                                            <h2>Story About Us</h2>
                                                            <div class="kc-cta-text">VenueListing.com makes finding the perfect venue for your next event quick and easy. Search our comprehensive database of venues across the UK with meeting and function rooms available to hire for private and corporate events.Each year thousands of events are booked via The Big Venue Book;from meetings in Manchester to Christmas parties in Cheltenham, look no further your perfect venue is sure to be found here.</div>
                                                        </div>
                                                        <div class="kc-cta-button"><a href="#">Start Now <span class="kc-cta-icon"><i class="fa-thumbs-o-up"></i></span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kc-elm kc-css-999725" style="height: 90px; clear: both; width:100%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="kc-elm kc-css-112594 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-106881 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="kc-elm kc-css-518945 kc-title-wrap align-center align-center">
                                            History
                                            <h2 class="kc_title">Company History</h2>
                                        </div>
                                        <div class="element-timelife">
                                            <div class="inner">
                                                <div class="entry-timeline">
                                                    <div class="timelife-head">
                                                        <i class="fa fa-calendar icon" aria-hidden="true"></i>
                                                        <div class="timelife-date">
                                                            <span></span>
                                                            <span>2014</span>
                                                        </div>
                                                    </div>
                                                    <div class="timelife-content">
                                                        <h4>We have an idea</h4>
                                                        <p class="timelife-description">
                                                        Since the 1500s, when a printer jumbled a galley of type to create a type specimen book, Lorem Ipsum has been the industry standard for dummy text. Today, a variety of software can create random text that resembles Lorem Ipsum. For example, Apples Pages and Keynote software use scrambled placeholder text. And Lorem Ipsum is featured on Google Docs, WordPress, and Microsoft Office Word.  </p>
                                                    </div>
                                                </div>
                                                <div class="entry-timeline">
                                                    <div class="timelife-head">
                                                        <i class="fa fa-calendar icon" aria-hidden="true"></i>
                                                        <div class="timelife-date">
                                                            <span></span>
                                                            <span>2015</span>
                                                        </div>
                                                    </div>
                                                    <div class="timelife-content">
                                                        <h4>Bugs Everywhere</h4>
                                                        <p class="timelife-description">
                                                        Whether you're organizing a corporate event or a private party, VenuListing has a wide selection of amazing event spaces with pictures and all the information you need. Use the search function to find the perfect place for you. In addition to regular settings, we have many unconventional venues to make your day truly unique! </p>
                                                    </div>
                                                </div>
                                                <div class="entry-timeline">
                                                    <div class="timelife-head">
                                                        <i class="fa fa-calendar icon" aria-hidden="true"></i>
                                                        <div class="timelife-date">
                                                            <span></span>
                                                            <span>2016</span>
                                                        </div>
                                                    </div>
                                                    <div class="timelife-content">
                                                        <h4>The difficult part:tie ups</h4>
                                                        <p class="timelife-description">
                                                        Whether you're organizing a corporate event or a private party, VenuListing has a wide selection of amazing event spaces with pictures and all the information you need. Use the search function to find the perfect place for you. In addition to regular settings, we have many unconventional venues to make your day truly unique! </p>
                                                    </div>
                                                </div>
                                                <div class="entry-timeline">
                                                    <div class="timelife-head">
                                                        <i class="fa fa-calendar icon" aria-hidden="true"></i>
                                                        <div class="timelife-date">
                                                            <span></span>
                                                            <span>2015</span>
                                                        </div>
                                                    </div>
                                                    <div class="timelife-content">
                                                        <h4>Hurrah!!! We are live</h4>
                                                        <p class="timelife-description">
                                                        Let’s say you’re drafting the ultimate content marketing strategy. Lorem Ipsum is placeholder text that stands in for meaningful content. It allows designers to focus on getting the graphical elements such as typography, font, and page layout in place first, before you move forward with the rest of your strategy. Before publication, you replace the Lorem Ipsum text with your polished, high quality content. </p>
                                                    </div>
                                                </div>
                                                <div class="entry-timeline">
                                                    <div class="timelife-head">
                                                        <i class="fa fa-calendar icon" aria-hidden="true"></i>
                                                        <div class="timelife-date">
                                                            <span></span>
                                                            <span>2016</span>
                                                        </div>
                                                    </div>
                                                    <div class="timelife-content">
                                                        <h4>Significant Improvement</h4>
                                                        <p class="timelife-description">
                                                        Let’s say you’re drafting the ultimate content marketing strategy. Lorem Ipsum is placeholder text that stands in for meaningful content. It allows designers to focus on getting the graphical elements such as typography, font, and page layout in place first, before you move forward with the rest of your strategy. Before publication, you replace the Lorem Ipsum text with your polished, high quality content. </p>
                                                    </div>
                                                </div>
                                                <div class="entry-timeline">
                                                    <div class="timelife-head">
                                                        <i class="fa fa-calendar icon" aria-hidden="true"></i>
                                                        <div class="timelife-date">
                                                            <span></span>
                                                            <span>2016</span>
                                                        </div>
                                                    </div>
                                                    <div class="timelife-content">
                                                        <h4>Prototype</h4>
                                                        <p class="timelife-description">
                                                        Let’s say you’re drafting the ultimate content marketing strategy. Lorem Ipsum is placeholder text that stands in for meaningful content. It allows designers to focus on getting the graphical elements such as typography, font, and page layout in place first, before you move forward with the rest of your strategy. Before publication, you replace the Lorem Ipsum text with your polished, high quality content. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </article>
        </div>
    </div>
</div>
</div>
</section>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ URL::asset('js/owl.carousal.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/slider.min.js') }}"></script>
@endsection