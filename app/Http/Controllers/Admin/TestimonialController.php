<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Testimonial;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $testimonial = Testimonial::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $testimonial = Testimonial::paginate($perPage);
        }

        return view('admin.testimonials.index', compact('testimonial'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'reviewer_name' => 'required|max:255',
            'reviewer_designation' => 'required|max:255',
            'review' => 'required|max:255',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
		]);
        $requestData = $request->all();
        
        $testimonial = Testimonial::create($requestData);
        $time = Carbon::now();
        $timestamp= $time->format('dmHis');
        if($request->hasFile('image'))
         {
            $file= $timestamp .'_'. $request->file('image')->getClientOriginalName();
            $request->file('image')->move("uploads/testimonial/images/",$file);
            $testimonial->image = $file;
         }
         $testimonial->save();
        return redirect('admin/testimonial')->with('flash_message', 'Testimonial added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        return view('admin.testimonials.show', compact('testimonial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        return view('admin.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'reviewer_name' => 'required|max:255',
            'reviewer_designation' => 'required|max:255',
            'review' => 'required|max:255',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
		]);
        $requestData = $request->all();       
        $testimonial = Testimonial::findOrFail($id);
        $time = Carbon::now();
        $timestamp= $time->format('dmHis');
        if($request->hasFile('image'))
         {
            $file= $timestamp .'_'. $request->file('image')->getClientOriginalName();
            $request->file('image')->move("uploads/testimonial/images/",$file);
            $requestData['image'] = $file;
         }
        $testimonial->update($requestData);
        
        return redirect('admin/testimonial')->with('flash_message', 'Testimonial updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Testimonial::destroy($id);

        return redirect('admin/testimonial')->with('flash_message', 'Testimonial deleted!');
    }
}
