<footer id="opal-footer" class="opal-footer">
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
          <aside id="widget_sp_image-18" class="widget clearfix widget_sp_image">
            <img width="187" height="57" class="attachment-full" style="max-width: 100%;" src="{{url('/images/rishabh2.png')}}"></aside>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
          <aside id="popupnewsletter-3" class="clearfix widget_popupnewsletter">
            <div class="popupnewsletter">
              <span class="pull-right">Subscribe to Our News Offers</span>
              <button type="button" class="btn btn-flying-right pull-right" data-toggle="modal" data-target="#popupNewsletterModal">
                <i class="fas fa-envelope"></i>
              </button>

              <!-- Modal -->
              <div class="modal fade" id="popupNewsletterModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                    <div class="modal-body">
                      <div class="popupnewsletter-widget  ">
                        <div>
                          <h3>
                            <span>Newsletter</span>
                          </h3>
                          <p class="description">
                            Put your content here</p>

                          <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-6363 mc4wp-form-basic" method="post" data-id="6363" data-name="Default sign-up form">
                            <div class="mc4wp-form-fields">
                              <div class="form-style-1">
                                <div class="input-group">
                                  <input id="mc4wp_email" class="form-control" name="EMAIL" required="required" type="email" placeholder="Your Email">
                                  <div class="input-group-btn">
                                    <input class="btn-primary btn" type="submit" value="Subcribe">
                                  </div>
                                </div>
                              </div>
                              <div style="display: none;">
                                <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off">
                              </div>
                              <input type="hidden" name="_mc4wp_timestamp" value="1495523050">
                              <input type="hidden" name="_mc4wp_form_id" value="6363">
                              <input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1">
                            </div>
                            <div class="mc4wp-response"></div>
                          </form>
                          <!-- / MailChimp for WordPress Plugin -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-bottom">
    <div class="container">
      <div class="footer-bottom-inner ">
        <div class="row equal-height">
          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
            <aside id="nav_menu-24" class="widget clearfix widget_nav_menu">
              {{--<h3 class="widget-title">Theme Features</h3>--}}
              {{--<div class="menu-menu-1-container">--}}
                {{--<ul id="menu-menu-1" class="menu">--}}
                  {{--<li id="menu-item-11280" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11280">--}}
                    {{--<a href="">Poromo</a></li>--}}
                  {{--<li id="menu-item-11281" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11281">--}}
                    {{--<a href="">Place List</a></li>--}}
                  {{--<li id="menu-item-11282" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11282">--}}
                    {{--<a href="">Map</a></li>--}}
                  {{--<li id="menu-item-11283" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11283">--}}
                    {{--<a href="">List User</a></li>--}}
                  {{--<li id="menu-item-11284" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11284">--}}
                    {{--<a href="">Post Place</a></li>--}}
                {{--</ul>--}}
              {{--</div>--}}
            </aside>
          </div>

          {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 space-top-40">--}}
            {{--<aside id="nav_menu-25" class="widget clearfix widget_nav_menu">--}}
              {{--<div class="menu-menu-2-container">--}}
                {{--<ul id="menu-menu-2" class="menu">--}}
                  {{--<li id="menu-item-11287" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11287">--}}
                    {{--<a href="">Teams</a></li>--}}
                  {{--<li id="menu-item-11288" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11288">--}}
                    {{--<a href="">Forums</a></li>--}}
                  {{--<li id="menu-item-11289" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11289">--}}
                    {{--<a href="">Upcoming events</a></li>--}}
                  {{--<li id="menu-item-11290" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11290">--}}
                    {{--<a href="">Affiliates</a></li>--}}
                  {{--<li id="menu-item-11291" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11291">--}}
                    {{--<a href="">Rentme Local</a></li>--}}
                {{--</ul>--}}
              {{--</div>--}}
            {{--</aside>--}}
          {{--</div>--}}

          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
            <aside id="nav_menu-26" class="widget clearfix widget_nav_menu">
              <h3 class="widget-title">Categories</h3>
              <div class="menu-menu-3-container">
                <ul id="menu-menu-3" class="menu">
                  <li id="menu-item-11292" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11292">
                    <a href="">Charities</a></li>
                  <li id="menu-item-11293" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11293">
                    <a href="">City Essentials</a></li>
                  <li id="menu-item-11294" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11294">
                    <a href="">Event &amp; Party Planning</a></li>
                  <li id="menu-item-11349" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11349">
                    <a href="">Fitness</a></li>
                  <li id="menu-item-11350" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11350">
                    <a href="">Food Shopping</a></li>
                </ul>
              </div>
            </aside>
          </div>

          {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 space-top-40 clearstyle">--}}
            {{--<aside id="nav_menu-27" class="widget clearfix widget_nav_menu">--}}
              {{--<div class="menu-menu-4-container">--}}
                {{--<ul id="menu-menu-4" class="menu">--}}
                  {{--<li id="menu-item-11297" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11297">--}}
                    {{--<a href="">Gifts</a></li>--}}
                  {{--<li id="menu-item-11298" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11298">--}}
                    {{--<a href="">Health &amp; Wellness</a></li>--}}
                  {{--<li id="menu-item-11299" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11299">--}}
                    {{--<a href="">Upcoming events</a></li>--}}
                  {{--<li id="menu-item-11300" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11300">--}}
                    {{--<a href="">Home</a></li>--}}
                  {{--<li id="menu-item-11301" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11301">--}}
                    {{--<a href="">Male Grooming</a></li>--}}
                {{--</ul>--}}
              {{--</div>--}}
            {{--</aside>--}}
          {{--</div>--}}

          {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 space-top-40">--}}
            {{--<aside id="nav_menu-28" class="widget clearfix widget_nav_menu">--}}
              {{--<div class="menu-menu-5-container">--}}
                {{--<ul id="menu-menu-5" class="menu">--}}
                  {{--<li id="menu-item-11302" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11302">--}}
                    {{--<a href="">Nightlife</a></li>--}}
                  {{--<li id="menu-item-11303" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11303">--}}
                    {{--<a href="">Parenting</a></li>--}}
                  {{--<li id="menu-item-11304" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11304">--}}
                    {{--<a href="">Pets</a></li>--}}
                  {{--<li id="menu-item-11305" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11305">--}}
                    {{--<a href="">Restaurants</a></li>--}}
                  {{--<li id="menu-item-11306" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11306">--}}
                    {{--<a href="">Shopping</a></li>--}}
                {{--</ul>--}}
              {{--</div>--}}
            {{--</aside>--}}
          {{--</div>--}}

          {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 space-top-40">--}}
            {{--<aside id="nav_menu-29" class="widget clearfix widget_nav_menu">--}}
              {{--<div class="menu-menu-6-container">--}}
                {{--<ul id="menu-menu-6" class="menu">--}}
                  {{--<li id="menu-item-11307" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11307">--}}
                    {{--<a href="">Sports</a></li>--}}
                  {{--<li id="menu-item-11308" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11308">--}}
                    {{--<a href="">Things to do</a></li>--}}
                  {{--<li id="menu-item-11309" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11309">--}}
                    {{--<a href="">Travel</a></li>--}}
                  {{--<li id="menu-item-11310" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11310">--}}
                    {{--<a href="">Vehicles</a></li>--}}
                  {{--<li id="menu-item-11311" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11311">--}}
                    {{--<a href="">Women’s Beauty</a></li>--}}
                {{--</ul>--}}
              {{--</div>--}}
            {{--</aside>--}}
          {{--</div>--}}
        </div>
      </div>
    </div>
  </div>

</footer>
<!--footer start from here-->
<div class="copyright">
  <div class="container">
    <div class="col-md-12">
      <p>© 2017 All rights reserved | Developed by 
        <a class="link-developed-by" href="http://budgetteknologies.com/">
          {{--<img src="../public/images/budgetlogohorizontalsmall.png" alt="Budget Teknologies - Delivering Value For Money" />--}}
          <img src="{{url('/images/budgetlogohorizontalsmall.png')}}" alt="Budget Teknologies - Delivering Value For Money" />
        </a>
      </p>

    </div>

  </div>
</div>
<style>
    .copyright p {
    text-align: center;
    color: #FFF;
    padding: 10px 0;
    margin-bottom: 0px;
    font-size: 18px;
}
.copyright {
    min-height: 40px;
    background-color: #000000;
}
    </style>