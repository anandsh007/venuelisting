@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{ URL::asset('css/kingcomposer.min.css') }}" type="text/css">
    <style>
        .kc-css-293436 {
            background: #f7f7f7;
            /*margin-top: -100px;*/
        }
        .kc-css-975948 {
            background: #f7f7f7;
            margin-bottom: 100px;
            padding-bottom: 80px;
        }
        .kc-css-349522 .kc_title {
            color: #292e38;
            font-size: 30px;
            text-align: left;
        }
        .kc-css-352443 .kc_title {
            font-size: 18px;
            text-align: left;
            margin-bottom: 0px;
        }
    </style>

@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <section id="opal-breadscrumb" class="opal-breadscrumb" style="">
            <div class="container">
                <h2 class="navheading">Let's sit for a coffee</h2>
                <ol class="breadcrumb">
                    <li><a href="{{route('home.home')}}">Home</a></li><span></span>
                    <li><a>Contact Us</a> </li>
                </ol>
            </div>
        </section>
        <section class="kc-elm kc-css-15736 kc_row">
            <div class="kc-row-container">
                <div class="kc-wrap-columns">
                    <div class="kc-elm kc-css-686762 kc_col-sm-12 kc_column kc_col-sm-12">
                        <div class="kc-col-container">
                            <div class="kc-elm kc-css-940624 kc_google_maps kc_shortcode">
                                <div style="" class="kc-google-maps google-map-block">
                                    <iframe  src="https://maps.google.com/maps?q=Budget%20Teknologies%20
                                (a.k.a%20Budget%20Technologies)%2C%20Lucknow%2C%20Uttar%20Pradesh%2C%20India&t=&z=14&ie=
                                UTF8&iwloc=&output=embed" width="100%" height="470" frameborder="0" style="border: 0px; pointer-events: auto;" allowfullscreen="">
                                </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="kc-elm kc-css-293436 kc_row">
            <div class="kc-row-container  kc-container">
                <div class="kc-wrap-columns">
                    <div class="kc-elm kc-css-191104 kc_col-sm-3 kc_column kc_col-sm-3">
                        <div class="kc-col-container"></div>
                    </div>
                    <div class="kc-elm kc-css-30706 kc_col-sm-6 kc_column kc_col-sm-6">
                        <div class="kc-col-container">
                            <div class="element-block-heading style-v3">
                                <div class="inner">
                                    <h2 class="heading">Get In Touch</h2>

                                    <h4 class="sub-heading">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                                        odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.</h4>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kc-elm kc-css-567191 kc_col-sm-3 kc_column kc_col-sm-3">
                        <div class="kc-col-container"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="kc-elm kc-css-975948 kc_row">
            <div class="kc-row-container  kc-container">
                <div class="kc-wrap-columns">
                    <div class="kc-elm kc-css-962647 kc_col-sm-6 kc_column kc_col-sm-6">
                        <div class="kc-col-container">
                            <div class="kc-elm kc-css-349522 kc-title-wrap ">

                                <h2 class="kc_title">Contact Info</h2>
                            </div>
                            <div class="kc-elm kc-css-279649 kc_row kc_row_inner">
                                <div class="kc-elm kc-css-935289 kc_col-sm-6 kc_column_inner kc_col-sm-6">
                                    <div class="kc_wrapper kc-col-inner-container">
                                        <div class="kc-elm kc-css-28612 kc_text_block">
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                                architecto beatae vitae sunt explicabo.</p>
                                        </div>
                                        <div class="kc-elm kc-css-352443 kc-title-wrap ">

                                            <div class="kc_title">We Are On Social Plateform </div>
                                        </div>
                                        <div class="kc-elm kc-css-758833 kc_box_wrap ">
                                            <ul class="social list-unstyled list-inline bo-sicolor highlight">
                                                <li><a class="facebook" href="#"><i class="fab fa-facebook"></i></a></li>
                                                <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                                <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="kc-elm kc-css-925553 kc_col-sm-6 kc_column_inner kc_col-sm-6">
                                    <div class="kc_wrapper kc-col-inner-container">
                                        <div class="kc-elm kc-css-348260 kc_box_wrap ">
                                            <ul class="contact-us highlight">
                                                <li><i class="fa fa-map-marker"></i>
                                                    <p>
                                                        <text>Venue, Aliganj Lucknow Pin - 226024</text>
                                                    </p>
                                                </li>
                                                <li><i class="fa fa-mobile"></i>
                                                    <p>
                                                        <text>+91 7071777070</text>
                                                    </p>
                                                </li>
                                                <li><i class="fa fa-envelope"></i>
                                                    <a href="mailto:contact@budgetteknologies.com">
                                                        <p>
                                                            <text>contact@budgetteknologies.com</text>
                                                        </p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kc-elm kc-css-567274 kc_col-sm-6 kc_column kc_col-sm-6">
                        <div class="contact kc-col-container">
                            <div role="form" class="wpcf7" id="wpcf7-f10779-p9861-o1" lang="en-US" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="10779">
                                        <input type="hidden" name="_wpcf7_version" value="4.7">
                                        <input type="hidden" name="_wpcf7_locale" value="en_US">
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f10779-p9861-o1">
                                        <input type="hidden" name="_wpnonce" value="1df4182925">
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-12">
                                            <span class="wpcf7-form-control-wrap text-799"><input type="text" name="text-799" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="your_name" aria-required="true" aria-invalid="false" placeholder="Name*"></span>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-12">
                                            <span class="wpcf7-form-control-wrap email-257"><input type="email" name="email-257" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="your_email" aria-required="true" aria-invalid="false" placeholder="Email*"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="wpcf7-form-control-wrap text-144"><input type="text" name="text-144" value="" size="40" class="wpcf7-form-control wpcf7-text form-control" id="contact_subject" aria-invalid="false" placeholder="Subject"></span>
                                    </div>
                                    <div class="form-group area-content">
                                        <span class="wpcf7-form-control-wrap textarea-552"><textarea name="textarea-552" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" id="your_message" aria-invalid="false" placeholder="Message"></textarea></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit btn btn-block btn-primary"><span class="ajax-loader"></span>
                                    </div>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
</div>

    @endsection
    @section('scripts')
        <script type="text/javascript" src="{{ URL::asset('js/kingcomposer.min.js') }}"></script>

@endsection