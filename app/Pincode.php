<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Common\Pincodes
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $OfficeName
 * @property string $Pincode
 * @property int $DistrictId
 * @property int $StateId
 * @property float $Latitude
 * @property float $Longitude
 * @property int $EstLatLongAccuracy
 * @property string $Created_at
 * @property string $Updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereDistrictId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereEstLatLongAccuracy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereOfficeName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes wherePincode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereStateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Common\Pincodes whereUpdatedAt($value)
 */
class Pincode extends Model
{

    protected $guarded = ['id'];
    protected $table = 'Pincodes';
}