@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    <div>
        <h3>Edit Venue Video #{{ $venuevideo->id }}</h3>
        <div>
            <a href="{{ url('/admin/venue-videos') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif
            {!! Form::model($venuevideo, [
            'method' => 'PATCH',
            'url' => ['/admin/venue-videos', $venuevideo->id],
            'class' => 'form-horizontal',
            'files' => true
            ]) !!}
            @include ('admin.venue-videos.form', ['submitButtonText' => 'Update'])
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection