@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    <div>
        <h3>Venue Video {{ $venuevideo->id }}</h3>
        <div>
            <a href="{{ url('/admin/venue-videos') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/admin/venue-videos/' . $venuevideo->id . '/edit') }}" title="Edit VenueVideo"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
            'method'=>'DELETE',
            'url' => ['admin/venuevideos', $venuevideo->id],
            'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-sm',
            'title' => 'Delete VenueVideo',
            'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $venuevideo->id }}</td>
                        </tr>
                        <tr><th> Venue Id </th><td> {{ $venuevideo->venue_id }} </td></tr><tr><th> Video Name </th><td> {{ $venuevideo->video_name }} </td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection