<?php

namespace App\Http\Controllers\Admin\Venue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VenueVideo;
use Illuminate\Http\Request;

class VenueVideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $venuevideos = VenueVideo::where('venue_id', 'LIKE', "%$keyword%")
                ->orWhere('video_name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venuevideos = VenueVideo::paginate($perPage);
        }

        return view('admin.venue-videos.index', compact('venuevideos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.venue-videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'venue_id' => 'required'
		]);
        $requestData = $request->all();
        
        VenueVideo::create($requestData);

        return redirect('admin/venue-videos')->with('flash_message', 'VenueVideo added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $venuevideo = VenueVideo::findOrFail($id);

        return view('admin.venue-videos.show', compact('venuevideo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $venuevideo = VenueVideo::findOrFail($id);

        return view('admin.venue-videos.edit', compact('venuevideo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'venue_id' => 'required'
		]);
        $requestData = $request->all();
        
        $venuevideo = VenueVideo::findOrFail($id);
        $venuevideo->update($requestData);

        return redirect('admin/venue-videos')->with('flash_message', 'VenueVideo updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        VenueVideo::destroy($id);

        return redirect('admin/venue-videos')->with('flash_message', 'VenueVideo deleted!');
    }
}
