@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div>
            <h3>Create New Review</h3>
            <div>
                <a href="{{ url('/admin/reviews') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::open(['url' => '/admin/reviews', 'class' => 'form-horizontal', 'files' => true]) !!}

                @include ('admin.reviews.form')

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
