@extends('layouts.backend')

@section('content')
@section('title','Occasions')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/admin/occasions') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/admin/occasions/' . $occasion->id . '/edit') }}" title="Edit occasion"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/occasions', $occasion->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete Occasions',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Occasions {{ $occasion->id }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $occasion->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $occasion->name}} </td></tr>
                                      <tr><th> Category</th><td> {{ $occasion->occasion_category_id}} </td></tr>
                                    <tr><th> Image </th>
                                <td> 
                                    @if(!empty($occasion->image))
                                    <img src="{{url('uploads/occasion/images/'.$occasion->image)}}" style="height: 80px; width:80px">  
                                    @else  <img src="{{ asset('images/image-not-found.png') }}" style="height: 80px; width:80px"></p>  
                                    @endif 
                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
