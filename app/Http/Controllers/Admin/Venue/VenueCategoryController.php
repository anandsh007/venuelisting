<?php

namespace App\Http\Controllers\Admin\Venue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VenueCategory;
use Illuminate\Http\Request;

class VenueCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $venuecategories = VenueCategory::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venuecategories = VenueCategory::paginate($perPage);
        }

        return view('admin.venue-categories.index', compact('venuecategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.venue-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        VenueCategory::create($requestData);

        return redirect('admin/venue-categories')->with('flash_message', 'Venue Category added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $venuecategories = VenueCategory::findOrFail($id);

        return view('admin.venue-categories.show', compact('venuecategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $venuecategories = VenueCategory::findOrFail($id);

        return view('admin.venue-categories.edit', compact('venuecategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $venuecategories = VenueCategory::findOrFail($id);
        $venuecategories->update($requestData);

        return redirect('admin/venue-categories')->with('flash_message', 'Venue Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        VenueCategory::destroy($id);

        return redirect('admin/venue-categories')->with('flash_message', 'Venue Category deleted!');
    }
}
