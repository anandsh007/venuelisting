<?php

namespace App\Http\Controllers\Admin\Venue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Venue;
use Illuminate\Http\Request;

class VenuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 12;

        if (!empty($keyword)) {
            $venues = Venue::where('name', 'LIKE', "%$keyword%")
                ->orWhere('long_desc', 'LIKE', "%$keyword%")
                ->orWhere('short_desc', 'LIKE', "%$keyword%")
                ->orWhere('max_capacity', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venues = Venue::with('venueAmenities', 'venuePaymentOptions', 'venuePackages','venueSubCategories','occasions', 'venueFeatures', 'venueAddress', 'venueVideo','venueImages','venueTags','reviews')
                ->paginate($perPage);
            foreach ($venues as $venue) 
            {
                if($venue->reviews != null && !($venue->reviews->isEmpty()))
                {
                    $ratings = $venue->reviews->toArray();
                    $ratingSum = array_sum(array_column($ratings, 'rating'));
                    $ratingCount = count(array_column($ratings, 'rating'));
                    $avgRating = (float) ($ratingSum/$ratingCount);
                    $venue->avgRating = number_format($avgRating, 2, '.', ',');
                }   
                else
                {
                    $venue->avgRating = 0;
                }
            }
        }

        return view('admin.venues.index', compact('venues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.venues.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Venue::create($requestData);

        return redirect('admin/venues')->with('flash_message', 'Venue added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $venue = Venue::findOrFail($id);
        
        return view('admin.venues.show', compact('venue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $venue = Venue::findOrFail($id);

        return view('admin.venues.edit', compact('venue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $venue = Venue::findOrFail($id);
        $venue->update($requestData);

        return redirect('admin/venues')->with('flash_message', 'Venue updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Venue::destroy($id);

        return redirect('admin/venues')->with('flash_message', 'Venue deleted!');
    }
}
