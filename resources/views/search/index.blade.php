@extends('layouts.app')
@section('styles')
<link rel='dns-prefetch' href='//maps.googleapis.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="stylesheet" href="{{ URL::asset('css/home.css') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/magnific-popup.css') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-slider.min.css') }}" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" type="text/css">
<style type="text/css">
#opal-breadscrumb.search-page-banner {
    margin-top: -63px;
        margin-bottom: 0px;
}
.contactus-image-margin
{
    height: 250px;
}
</style>
@endsection 
@section('content')
<div class="">
        <section id="opal-breadscrumb" class="opal-breadscrumb contactus-image-margin search-page-banner" style="">
            <div class="container">
                <h2 class="navheading">Search Venue</h2>
            </div>
        </section>
        <section id="main" class="site-main search-page">
            <div id="main-content" class="main-content">
                <div id="primary" class="content-area">
                    <div id="content" class="site-content" role="main">
                        <article id="post-11651" class="post-11651 page type-page status-publish hentry">
                            <div class="entry-content-page">
                                <div class="opallisting-split-map">
                                    <div class="row opallisting-row-eq-height wrapper">
                                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 hidden-xs no-padding map-section" id="map">
                                            <div class="opallisting-map-preview-wrapper">
                                                <div id="gmap" style="width:100%;height:622px;"></div>
                                                <div id="controls"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12 no-padding places-section" id="places">
                                            <form class="opallisting-search-form opallisting-rows" action="{!!route('search.search')!!}" method="get">
                                                {{-- {{ csrf_field() }} --}}
                                                <div class="row opal-row">                        
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <select name='occasionId' class='form-control'>
                                                            <option class="level-0" value="">Category</option>
                                                            @foreach($occasionId as $key => $occasion)
                                                            <option value="{{$key}}">
                                                                {{$occasion}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <input class='form-control' id='pac-input' placeholder='Enter an address or an intersection'/>
                                                        <input type="hidden" name="latitude" id="lat">
                                                        <input type="hidden" name="longitude" id="long">
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <input class="form-control" name="keywords" placeholder="Keywords" />
                                                    </div>
                                                </div>                                               
                                                <div class="row opal-row">
                                                    <div class="col-md-12">
                                                        <button type="button" class="btn btn-primary filter-button" data-toggle="collapse" data-target="#demo">More Filters</button>
                                                        <div id="demo" class="collapse">
                                                            <div class="row opal-row search-page-filter">
                                                                <div class="col-lg-3 col-xs-3 search-filter">
                                                                    <label for="radius_enabled">
                                                                        Radius: <span class="radius">50 km</span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-9 col-xs-9 search-filter">
                                                                    <b>10 &nbsp; &nbsp;  </b>
                                                                    <input id="ex2" data-slider-id='ex1Slider' type="text" name="radius" data-slider-min="10" data-slider-max="50" data-slider-step="1" />&nbsp; &nbsp; <b>&nbsp;50</b>
                                                                </div>
                                                            </div>
                                                            <div class="row opal-row search-page-filter">
                                                                <div class="col-lg-3 col-xs-3 search-filter">
                                                                    <label for="radius_enabled">
                                                                        Amenities:
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-9 col-xs-9 search-filter">
                                                                    <div class="row">
                                                                        @foreach($venueAmenities as $venueAmenity)
                                                                            <div class="col-md-4">
                                                                            <label class="checkbox-style">
                                                                                <input type="checkbox" class="jquerycheckbox2" value="{{$venueAmenity->id}}" name='amenities[]'>
                                                                                {{$venueAmenity->name}}
                                                                            </label>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row opal-row search-page-filter">
                                                                <div class="col-lg-3 col-xs-3 search-filter">
                                                                    <label for="radius_enabled">
                                                                        Features:
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-9 col-xs-9 search-filter">
                                                                    <div class="row">
                                                                    @foreach($venueFeatures as $venueFeature)
                                                                    <div class="col-md-4">
                                                                    <label class="checkbox-style">
                                                                        <input type="checkbox" class="jquerycheckbox2" value="{{$venueFeature->id}}" name='venue_features[]'>
                                                                        {{$venueFeature->name}}
                                                                    </label>
                                                                </div>
                                                                    @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row opal-row">
                                                    <input type="submit" class="btn btn-primary col-md-12" value="Search" />
                                                </div>
                                            </form>
                                           <div class="opallisting-places-wrapper">
                                                <div class="opallisting-archive-top">
                                                    <div class="row opal-row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class="opallisting-count pull-left">
                                                                {{ count($venues) }} Places Found </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class="pull-right">
                                                                <div class="opallisting-btn-group sortable">
                                                                    <a href="#" class="btn">Sort by : <strong>Newest</strong></a>
                                                                    <ul class="opallisting-dropdown-menu">
                                                                        <li>
                                                                            <a href="#" class=" active"  data-opalsortable="1">
                                                                            Newest              </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="" data-opalsortable="featured_desc">
                                                                            Featured First     </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="" data-opalsortable="rating_asc">
                                                                             Rating Ascending
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="" data-opalsortable="rating_desc">
                                                                              Rating Desending</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="opallisting-btn-group layout">
                                                                    <a href="#" data-archive="" class="opallisting-switch-layout btn active" data-action="opallisting_switch_layout" data-layout="grid" data-nonce="06a46e345a" title="Grid">
                                                                        <i class="fa fa-th"></i> Grid </a>
                                                                    <a href="#" data-archive="" class="opallisting-switch-layout btn" data-action="opallisting_switch_layout" data-layout="list" data-nonce="06a46e345a" title="List">
                                                                        <i class="fa fa-th-list"></i> List </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="opallisting-places-results opallisting-rows">
                                                    <div class="row opal-row">
                                                        @if(!$venues->isEmpty())
                                                        @foreach($venues as $venue)
                                                        <div class="col-lg-4 col-md-6 col-sm-6">
                                                            <article class="place-grid">
                                                                <header>
                                                                    <div class="place-box-image">
                                                                        <a href="{{route('venue.show',$venue->id)}}" class="place-box-image-inner">
                                                                            <img width="495" height="500" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/place-50-495x500.jpg" class="attachment-rentme_place_thumbnail size-rentme_place_thumbnail wp-post-image" alt="" /> </a>
                                                                    </div>

                                                                    <div class="place-actions">

                                                                        <a href="{{route('venue.show',$venue->id)}}" class="author-link"><img alt='' src='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png' srcset='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png 2x' class='avatar avatar-96 photo' height='96' width='96' /><span>admin</span></a>
                                                                        <a href="#" data-icon="heart" data-id="11489" data-nonce="d2cb662fa0" class="favorite  opallisting-need-login">
                                                                            <i class="far fa-heart" aria-hidden="true"></i>
                                                                        </a>
                                                                    </div>
                                                                </header>

                                                                <div class="place-content">

                                                                    <h4 class="place-title"><a href="{{route('venue.show',$venue->id)}}" rel="bookmark">{{ $venue->name }}</a></h4>

                                                                    <div class="place-address">
                                                                        <i class="fa fa-map-marker"></i> {{ $venue->venueAddress['area']}}, {{ $venue->venueAddress['city']}}, {{ $venue->venueAddress['state']}}</div>

                                                                    {{-- <div class="place-type">
                                                                        <span class="fa fa-users "></span>
                                                                        <span>Things to do</span>
                                                                    </div> --}}
                                                                    <div class="place-meta">

                                                                        <div class="comment-rating-wrap">
                                                                            <div class="comment-rating" title="Rated 0 Star for listing">
                                                                                <span style="width:0%">
                                                                                    @if($venue->reviews != null && !($venue->reviews->isEmpty()))
                                                                                      @php
                                                                                        $ratings = $venue->reviews->toArray();
                                                                                        $ratingSum = array_sum(array_column($ratings, 'rating'));
                                                                                        $ratingCount = count(array_column($ratings, 'rating'));
                                                                                        $tempAvgRating = (float) ($ratingSum/$ratingCount);
                                                                                        $avgRating = number_format($tempAvgRating, 2, '.', ',');
                                                                                       @endphp   
                                                                                    @else
                                                                                        @php
                                                                                        $avgRating = 0;
                                                                                        @endphp
                                                                                    @endif
                                                                                    @php
                                                                                    $avint = intval($avgRating);
                                                                                    $avdec = abs($avgRating - $avint);
                                                                                    @endphp
                                                                                    @for( $i = 0; $i<5; $i++ )
                                                                                        @if($i < $avint)
                                                                                            <i class="fas fa-star"></i>
                                                                                        @elseif($avdec > 0)
                                                                                            <i class="fas fa-star-half-alt"></i>
                                                                                            @php 
                                                                                            $avdec = 0;
                                                                                            @endphp  
                                                                                        @else
                                                                                            <i class="far fa-star"></i>
                                                                                        @endif
                                                                                    @endfor
                                                                                </span>
                                                                            </div>
                                                                            <div class="listing-count-reviews">({{ count($venue->reviews) }} Reviews)</div>
                                                                        </div>
                                                                        <span class="current-status  opening">Opening</span> 
                                                                    </div>
                                                                </div>
                                                                <!-- .place-content -->

                                                                <meta itemprop="url" content="#" />

                                                            </article>
                                                            <!-- #post-## -->
                                                        </div>
                                                       @endforeach
                                                       @endif 
                                                       
                                                    </div>

                                                   {{--  <div class="w-pagination pagination">
                                                        <div class="pbr-pagination pagination-main">
                                                            <ul class="pagination">
                                                                <li class="disabled"><a aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>
                                                                <li class="active" data-paged="1"><a href="http://venusdemo.com/wordpress/rentme/split-map/">1 <span class="sr-only"></span></a></li>
                                                                <li data-paged="2"><a href="http://venusdemo.com/wordpress/rentme/split-map/page/2/">2</a></li>
                                                                <li data-paged="3"><a href="http://venusdemo.com/wordpress/rentme/split-map/page/3/">3</a></li>
                                                                <li data-paged="2"><a aria-label="Next" href="http://venusdemo.com/wordpress/rentme/split-map/page/2/"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>
                                                                <li data-paged="0"><a aria-label="Last" href="http://venusdemo.com/wordpress/rentme/split-map/page/12/"><span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span></a></li>
                                                            </ul>
                                                        </div>
                                                    </div> --}}
                                                     <div class="pagination-wrapper"> {!! $venues->appends(['search' => Request::get('search')])->render() !!} </div>

                                        </div>
                                    </div>
                                     
                                </div>
                            </div>
                            <!-- .entry-content -->
                        </article>
                        <!-- #post-## -->
                    </div>
                    <!-- #content -->
                </div>
                <!-- #primary -->
            </div>
            <!-- #main-content -->
            <input type="button" class="btn btn-primary center-block" onclick="myFunction()" value="View Map" id="map-button">
        </section>
            </div>
        </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ URL::asset('js/jquery.form.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-slider.min.js') }}"></script>
 <!--=============== google map script ============-->
<script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry,places&key=AIzaSyAjYIJDSpRo90YUDZNtLnSCTmuMHfLMAlo">
</script>
<script src="{{ asset('js/maplace.js') }}"></script>
<script type="text/javascript">
$(function() {
    new Maplace({
        locations: [{
        lat: 20.593684,
        lon: 80.95661949999999,
        zoom: 4
    }],
        controls_on_map: true
    }).Load();
});
</script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
    $(".js-example-basic-single").select2();
});
</script> --}}
<script>
function myFunction() {
    var button = document.getElementById("map-button");
    if (button.value=="View Map"){
        $("#map").removeClass("hidden-xs");
        $("#places").addClass("hidden-xs");
        button.value = "View List";
    }
    else{
        $("#map").addClass("hidden-xs");
        $("#places").removeClass("hidden-xs");
        button.value = "View Map";
    }         
}
</script>
<script type="text/javascript">
    function initialize() {
        var address = (document.getElementById('pac-input'));
        var autocomplete = new google.maps.places.Autocomplete(address);
        autocomplete.setComponentRestrictions(
                {'country': 'in'});
        autocomplete.setTypes(['geocode']);
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
             var address = place.address_components;
             document.getElementById('lat').value = "";
             document.getElementById('long').value = ""; 
             var latitude = place.geometry.location.lat(); // latitude of location
             document.getElementById('lat').value = latitude;
             var longitude = place.geometry.location.lng(); // longitude of location
             document.getElementById('long').value = longitude; 
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
   $('#ex2').slider({
    formatter: function(value) {
        return value;
    }
});
</script>
@endsection