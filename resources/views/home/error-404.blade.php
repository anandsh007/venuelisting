@extends('layouts.app')

@section('styles')

    <link rel="stylesheet" href="{{ URL::asset('css/kingcomposer.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css">
@endsection
@section('content')
<div class="">
<div class="row">
    <section id="opal-breadscrumb" class="opal-breadscrumb" style="">
        <div class="container">
            <h2 class="navheading">Pricing</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('home.aboutUs')}}">Home</a> </li><span></span>
                <li>Pricing</li>
            </ol>
        </div>
    </section>
    <section id="main-container" class="container inner clearfix notfound-page">
    <div class="row">
        <div id="main-content" class="main-content">
            <div id="primary" class="content-area">
                <div id="content" class="site-content" role="main">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                        <div class="title">
                            4
                            <span class="text-primary">0</span> 4
                        </div>
                        <div class="error-description">
                            <h3>Error</h3>
                            <p>Can not find what you need? Take a moment and do a search below or start from our</p>
                            <a class="" href="{{ route('home.home') }}">homepage.</a>
                        </div>
                        <!-- .page-content -->
                        <form method="get" class="searchform" action="http://venusdemo.com/wordpress/rentme/">
                            <div class="opal-search input-group">
                                <input name="s" maxlength="40" class="form-control input-large input-search" type="text" size="20" placeholder="Search...">
                                <span class="input-group-addon input-large btn-search">
            <input type="submit" class="fa" value="&#xf002;" />
                    </span>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- #content -->
            </div>
            <!-- #primary -->

        </div>
        <!-- #main-content -->

    </div>
</section>
</div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/kingcomposer.min.js') }}"></script>
@endsection
   