<ul class="list-inline pull-left">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            @role('User')
            <li><a href="{{ url('/users/dashboard') }}">
                    <i class="fa fa-btn fa-dashboard"></i>Dahboard</a>
            </li>
            @endrole
            @role('Admin')
            <li><a href="{{ url('/admin/dashboard') }}">
                    <i class="fa fa-btn fa-dashboard"></i>Dahboard</a>
            </li>
            @endrole
            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
        </ul>             
    </li>
</ul>