<!-- ************Admin Sidebar********** -->
<div class="sidebar-wrapper">
    <ul class="nav">
        <li class="nav-item {{ request()->is('admin') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin') }}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/users') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/users') }}">
                <i class="material-icons">group</i>
                <p>Users</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/roles') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/roles') }}">
                <i class="material-icons">assignment_ind</i>
                <p>Roles</p>
            </a>
        </li>
        <li class="nav-item {{ Route::currentRouteNamed('admin.userProfile') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.userProfile') }}">
                <i class="material-icons">person</i>
                <p>User Profile</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/permissions') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/permissions') }}">
                <i class="material-icons">thumb_up</i>
                <p>Permissions</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/venues') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/venues') }}">
                <i class="material-icons">account_balance</i>
                <p>Venues</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/venue-categories') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/venue-categories') }}">
                <i class="material-icons">dns</i>
                <p>Venue Categories</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/venue-sub-categories') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/venue-sub-categories') }}">
                <i class="material-icons">table_chart</i>
                <p>Venue Sub-Categories</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/occasions') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/occasions') }}">
                <i class="material-icons">stars</i>
                <p>Occasions</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/occasion-categories') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/occasion-categories') }}">
                <i class="material-icons">location_ons</i>
                <p>Occasion-Categaries</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/payment-options') ? 'active' : '' }} ">
            <a class="nav-link" href="{{ url('admin/payment-options') }}">
                <i class="material-icons">payment</i>
                <p>Payment-Options</p>
            </a>
        </li>
        <li class="nav-item {{ request()->is('admin/venue-amenities') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/venue-amenities') }}">
                <i class="material-icons">language</i>
                <p>Venue-amenities</p>
            </a>
        </li>
         <li class="nav-item {{ request()->is('admin/venue-features') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/venue-features') }}">
                <i class="material-icons">view_carousel</i>
                <p>Venue-Feature</p>
            </a>
        </li>
         <li class="nav-item {{ request()->is('admin/testimonials') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/testimonial') }}">
                <i class="material-icons">language</i>
                <p>Testimonials</p>
            </a>
        </li>
         <li class="nav-item {{ request()->is('admin/claim-listing') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/claim-listing') }}">
                <i class="material-icons">list</i>
                <p>Claim Listing</p>
            </a>
        </li>
         <li class="nav-item {{ request()->is('admin/booking-enquirys') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('admin/booking-enquirys') }}">
                <i class="material-icons">book</i>
                <p>Booking-Enquiry</p>
            </a>
        </li>
    </ul>
</div>
<!-- ************Admin Sidebar********** -->