@extends('layouts.app')
@section('content')
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">Venue</div>
        <div class="panel-body">
            <a href="{{ url('/venue/create') }}" class="btn btn-success btn-sm" title="Add New venue">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            {!! Form::open(['method' => 'GET', 'url' => '/venue', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group no-border">
                <input type="text" value="" class="form-control" name="search" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                </button>
            </div>
            {!! Form::close() !!}
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>ID</th><th>Name</th><th>short_desc</th><th>max_capacity</th><th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($venue as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->Name }}</td><td>{{ $item->short_desc }}</td><td>{{ $item->max_capacity }}</td>
                            <td>
                                <a href="{{ url('/venue/' . $item->id) }}" title="View venue"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                <a href="{{ url('/venue/' . $item->id . '/edit') }}" title="Edit venue"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/venue', $item->id],
                                'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete venue',
                                'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $venue->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
        </div>
    </div>
</div>
@endsection