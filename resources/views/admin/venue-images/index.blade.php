@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    <div>
        <h3>Venue Images</h3>
        <div>
            <a href="{{ url('/admin/venue-images/create') }}" class="btn btn-success btn-sm" title="Add New VenueImage">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            {!! Form::open(['method' => 'GET', 'url' => '/admin/venue-images', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                <span class="input-group-append">
                    <button class="btn btn-secondary" type="submit">
                    <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>#</th><th>Venue Id</th><th>Image Name</th><th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($venueimages as $item)
                        <tr>
                            <td>{{ $loop->iteration or $item->id }}</td>
                            <td>{{ $item->venue_id }}</td><td>{{ $item->image_name }}</td>
                            <td>
                                <a href="{{ url('/admin/venue-images/' . $item->id) }}" title="View VenueImage"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                <a href="{{ url('/admin/venue-images/' . $item->id . '/edit') }}" title="Edit VenueImage"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/venue-images', $item->id],
                                'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-sm',
                                'title' => 'Delete VenueImage',
                                'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $venueimages->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
        </div>
    </div>
</div>
@endsection