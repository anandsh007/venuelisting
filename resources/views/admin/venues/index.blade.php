@extends('layouts.backend')
@section('content')
{{-- <div class="container-fluid">
    <div>
        <h3>Venues</h3>
        <div>
            <a href="{{ url('/admin/venues/create') }}" class="btn btn-success btn-sm" title="Add New Venue">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            {!! Form::open(['method' => 'GET', 'url' => '/admin/venues', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                <span class="input-group-append">
                    <button class="btn btn-secondary" type="submit">
                    <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>#</th><th>Name</th><th>Long Desc</th><th>Short Desc</th><th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($venues as $item)
                        <tr>
                            <td>{{ $loop->iteration or $item->id }}</td>
                            <td>{{ $item->name }}</td><td>{{ $item->long_desc }}</td><td>{{ $item->short_desc }}</td>
                            <td>
                                <a href="{{ url('/admin/venues/' . $item->id) }}" title="View Venue"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                <a href="{{ url('/admin/venues/' . $item->id . '/edit') }}" title="Edit Venue"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/venues', $item->id],
                                'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-sm',
                                'title' => 'Delete Venue',
                                'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $venues->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
        </div>        
    </div>
</div> --}}
@section('title','Venues')
<div class="content">
    <div class="container-fluid">
        @include('shared.message')
        <div class="row">
            @foreach($venues as $venue)
            <div class="col-md-4">
                <div class="card card-chart">
                    <div class="card-header card-header-success card-style">
                        <a href="{{ route('venue.show',$venue->id) }}"><img src="{{ asset('images/img1.jpg') }}" class="img-responsive card-image-style" width="100%"></a>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('venue.show',$venue->id) }}"><h4 class="card-title">{{ $venue->name }}</h4></a>
                        <p class="card-category">
                            <span class="text-success"><i class="material-icons">room</i></span> {{ $venue->venueAddress['area'] }}, {{ $venue->venueAddress['city'] }}, {{ $venue->venueAddress['state'] }}
                        </p>
                        <p class="card-category">
                            <span style="width:80%" class="text-warning">
                                @php
                                    $avint = intval($venue->avgRating);
                                    $avdec1 = $avdec2 = abs($venue->avgRating - $avint);
                                @endphp
                                @for( $i = 0; $i<5; $i++ )
                                    @if($i < $avint)
                                        <i class="fas fa-star"></i>
                                    @elseif($avdec1 > 0)
                                        <i class="fas fa-star-half-alt"></i>
                                        @php 
                                        $avdec1 = 0;
                                        @endphp  
                                    @else
                                        <i class="far fa-star"></i>
                                    @endif
                                @endfor
                            </span>
                            <span>( {{count($venue->reviews)}} Reviews)</span>
                        </p>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <a href="{{ route('venue.edit',$venue->id) }}" title="Edit Venue"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                            {!! Form::open([
                                'method' => 'DELETE',
                                'url' => ['/admin/venues', $venue->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete Venue',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection