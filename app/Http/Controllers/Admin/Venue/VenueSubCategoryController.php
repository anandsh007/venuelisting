<?php

namespace App\Http\Controllers\Admin\Venue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VenueSubCategory;
use App\VenueCategory;
use Illuminate\Http\Request;

class VenueSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $venueSubCategories = VenueSubCategory::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $venueSubCategories = VenueSubCategory::paginate($perPage);
        }

        return view('admin.venue-sub-categories.index', compact('venueSubCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
         $venueCategoryId = venueCategory::pluck('name','id')->toArray();
        return view('admin.venue-sub-categories.create',compact('venueCategoryId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $venueSubCategories = VenueSubCategory::create($requestData);
        $venueSubCategories->venueCategories()->sync($request->venue_category_id);
        return redirect('admin/venue-sub-categories')->with('flash_message', 'VenueSubCategory added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $venueSubCategories = VenueSubCategory::findOrFail($id);

        return view('admin.venue-sub-categories.show', compact('venueSubCategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $venueSubCategories = VenueSubCategory::findOrFail($id);
        $venueCategoryId = venueCategory::pluck('name','id')->toArray();

        return view('admin.venue-sub-categories.edit', compact('venueSubCategories','venueCategoryId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $venueSubCategories = VenueSubCategory::findOrFail($id);
        $venueSubCategories->venueCategories()->sync($request->venue_category_id);
        $venueSubCategories->update($requestData);

        return redirect('admin/venue-sub-categories')->with('flash_message', 'venue Sub-Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        VenueSubCategory::destroy($id);

        return redirect('admin/venue-sub-categories')->with('flash_message', 'Venue Sub-Category deleted!');
    }
}
