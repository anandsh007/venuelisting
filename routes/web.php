<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', ['as' => 'home.home',
          'uses' => 'HomeController@index']);
// Route::get('/home', 'HomeController@index');
// Route::get('/welcome', 'HomeController@index');

// Home Route
Route::get('/aboutus', ['as' => 'home.aboutUs',   'uses' => 'HomeController@aboutUs']);
Route::get('contactus', ['as' => 'home.contactUs',   'uses' => 'HomeController@contactUs']);
Route::get('faq', ['as' => 'home.faq',   'uses' => 'HomeController@faq']);
Route::get('pricing', ['as' => 'home.pricing',   'uses' => 'HomeController@pricing']);
Route::get('404','HomeController@error404')->name('home.error404');
// Home Route Ended

// venue Routes Starts
Route::group(['middleware' => ['auth']], function() {
  Route::get('venues','VenueController@index')->name('venue.index');
  Route::get('venue/list','VenueController@create')->name('venue.create');
  Route::post('venue/store','VenueController@store')->name('venue.store');
  Route::get('venue/{id}','VenueController@show')->name('venue.show');
  Route::get('venue/{id}/edit','VenueController@edit')->name('venue.edit');
  Route::post('venue/{id}','VenueController@update')->name('venue.update');
  Route::post('venue/upload-images', 'FilesController@imageUpload')->name('files.imageUpload');
  Route::get('venue/create/getpincodes','PincodeController@getPincodes')->name('pincode.getPincodes');
  Route::post('venue/{id}/store-rating','ReviewsController@store')->name('review.store');
  Route::get('get-occasion','EnquiryController@getOccasion')->name('enquiry.getOccasion');
  Route::post('venue-enquiry','EnquiryController@submitEnquiry')->name('enquiry.submitEnquiry');
});
//Venue Routes Ended

//Search Routes
Route::get('search-venues','SearchController@search')->name('search.search');
//Search Routes Ended


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

  Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => 'Admin'], function (){

    Route::get('/', 'Admin\AdminController@index');
    Route::get('profile','Admin\AdminController@userProfile')->name('admin.userProfile');
    Route::resource('roles', 'Admin\RolesController');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::resource('users', 'Admin\UsersController');
    Route::resource('blog', 'Admin\\BlogController');
    Route::resource('testimonial', 'Admin\\TestimonialController');
    Route::resource('venues', 'Admin\\Venue\\VenuesController');
    Route::resource('venue-addresses', 'Admin\\Venue\\VenueAddressesController');

    Route::resource('venue-images', 'Admin\Venue\\VenueImagesController');
    Route::resource('tags', 'Admin\\TagsController');
    Route::resource('venue-videos', 'Admin\Venue\\VenueVideosController');
    Route::resource('occasions', 'Admin\Occasion\\OccasionsController');
    Route::resource('occasion-categories', 'Admin\Occasion\\OccasionCategoriesController');
    Route::resource('venue-amenities', 'Admin\Venue\\VenueAmenitiesController');
    Route::resource('reviews', 'Admin\\ReviewsController');
    Route::resource('claim-listing', 'Admin\\ClaimListingController');
    Route::resource('venue-categories', 'Admin\Venue\\VenueCategoryController');
    Route::resource('venue-sub-categories', 'Admin\Venue\\VenueSubCategoryController');
    Route::resource('payment-options', 'Admin\\PaymentOptionsController');
    Route::resource('venue-features', 'Admin\Venue\\VenueFeaturesController');
    Route::resource('booking-enquirys','Admin\\BookingEnquiryController');
    
  });
  //------User Route-------
  Route::group(['prefix' => 'user', 'middleware' => ['auth', 'roles'], 'roles' => 'User'], function ()
  {
    Route::get('/','User\UserController@index')->name('user.index');
   });