@extends('layouts.backend')
@section('content')
@section('title','Occasion Categories')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/admin/occasion-categories') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/admin/occasion-categories/' . $occasioncategory->id . '/edit') }}" title="Edit occasioncategories"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/occasion-categories', $occasioncategory->id],
                'style' => 'display:inline'
                ]) !!}
                {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', array(
                'type' => 'submit',
                'class' => 'btn btn-danger btn-sm',
                'title' => 'Delete occasioncategories',
                'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title text-center">Occasion Category {{ $occasioncategory->id }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $occasioncategory->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Name </th>
                                        <td> {{ $occasioncategory->name }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection