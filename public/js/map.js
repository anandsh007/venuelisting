(function($) {

    $.fn.opallisting_map = function( settings ) {
        // merge settings to defaults options
        settings = $.extend( {}, {
            
        }, settings );

        return this.each(function(){
            // MAP
            var that = OPALLISTING_MAP = {

                // map element ID
                mapEle: null,

                // map object
                map: null,

                // map options
                options: {},

                // overlay
                mapOverlay: null,

                // markers
                markers: [],

                // markers options
                options_marker: [],

                // bounds
                bounds: null,

                // places
                places: [],

                // markerClusterer
                markerClusterer: null,

                // check lastest infobox open
                infoBoxLast: null,

                // drawing manager
                drawingManager: null,

                // google map polygon
                polygon: null,

                // directionsService
                directionsService: null,

                // directionsDisplay
                directionsDisplay: null,

                // currentPosition
                currentPosition: null,

                // autocomplete
                autocompleteService: null,

                // place Service
                placeService: null,

                infowindow2: null,

                ajax: false,
                runed: false,

                // init map
                initMap: function( mapEle, options ) {
                    if ( typeof google == 'undefined' ) return;
                    // var that = this;

                    that.mapEle = $( mapEle );
                    if ( that.mapEle.length === 0 ){
                        return that.addDomEventListener();
                    }

                    // merge default map options
                    options = options || {};
                    that.options = that.getOptions( options );

                    // set and init map object
                    that.map = new google.maps.Map( mapEle, that.options );

                    // places object data
                    that.places = typeof that.options.places !== 'undefined' ? that.options.places : opallistingJS.places;

                    // markers inititalize
                    that.setMarkers();
                    // markerClusterer
                    that.setClusterer();

                    that.drawingManager = new google.maps.drawing.DrawingManager({
                        drawingMode: google.maps.drawing.OverlayType.POLYGON,
                        drawingControl: false,
                        polygonOptions: that.getPolygonOptions()
                    });

                    that.directionsService = new google.maps.DirectionsService();
                    that.directionsDisplay = new google.maps.DirectionsRenderer({
                        map: that.map
                    });

                    // auto completed service
                    that.autocompleteService = new google.maps.places.AutocompleteService();
                    // place Service
                    that.placeService = new google.maps.places.PlacesService( that.map );

                    // google map listen event
                    that.addDomEventListener();

                    // init polygon draw
                    that.initPolygon();

                    return that;
                },

                // setClusterer
                setClusterer: function() {
                    // var that = this;
                    that.markerClusterer = new MarkerClusterer( that.map, that.markers, {
                        ignoreHidden: true,
                        maxZoom: 14,
                        styles: [{
                            textColor: '#000000',
                            url: '',
                            height: 60,
                            width: 50
                        }]
                    } );

                    return that.markerClusterer;
                },

                // get polygonOptions
                getPolygonOptions: function() {
                    // var that = this;
                    return {
                            fillColor: that.mapEle.data( 'fillColor' ) ? that.mapEle.data( 'fillColor' ) : '#c5d244',
                            fillOpacity: 0.8,
                            strokeWeight: 1,
                            clickable: false,
                            editable: true,
                            zIndex: 1
                        };
                },

                // set options for google map
                getOptions: function( options ) {
                    // var that = this;
                    // merge default map options
                    return $.extend( {}, {
                        scrollwheel: false,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        panControl: true,
                        zoomControl: true,
                        mapTypeControl: true,
                        scaleControl: false,
                        draggable: false,
                        streetViewControl: true,
                        overviewMapControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.SMALL,
                            position: google.maps.ControlPosition.RIGHT_BOTTOM
                        },
                        streetViewControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_BOTTOM
                        },
                        center: {
                            lat: that.mapEle.data( 'lat' ) ? parseFloat( that.mapEle.data( 'lat' ) ) : 54.800685,
                            lng: that.mapEle.data( 'lng' ) ? parseFloat( that.mapEle.data( 'lng' ) ) : -4.130859,
                        },
                        zoom: that.mapEle.data( 'zoom' ) ? parseInt( that.mapEle.data( 'zoom' ) ) : 12,
                        maxZoom: 16,
                        fullscreenControl: true
                    }, options );
                },

                // set map markers
                setMarkers: function( places ) {
                    // var that = this;
                    that.bounds = new google.maps.LatLngBounds();
                    places = places || that.places;

                    // forEach places and add it to map
                    for ( var i = 0; i < places.length; i++ ) {
                        var place = places[i];
                        that._addMarker( place, i );
                    }

                    that.map.fitBounds( that.bounds );
                    that.map.panToBounds( that.bounds );

                    if ( that.directionsDisplay ) {
                        that.directionsDisplay.setMap( null );
                    }

                    if ( that.markers.length === 1 ) {
                        $( '#opallisting-toggle-direction' ).parents( 'li:first' ).show();
                    } else {
                        $( '#opallisting-toggle-direction' ).parents( 'li:first' ).hide();
                    }
                },

                // clear markers
                clearMarkers: function() {
                    // var that = this;
                    that.markers = [];
                    // remove all markers
                    this.markerClusterer.clearMarkers();
                },

                // repaint markers in map
                repaint: function( places, clearDraw ) {
                    // var that = this;
                    that.clearMarkers();
                    // set new marker
                    that.setMarkers( places );

                    that.setClusterer();
                    // repaint marker
                    // that.markerClusterer.repaint();
                    if ( clearDraw && that.polygon ) {
                        that.polygon.setMap( null );
                    }

                    $( '.opallisting-map-controls a' ).removeClass( 'active' );
                },

                // add single marker
                _addMarker: function( place, i ) {
                    if ( ! place.lat || ! place.lng ) {
                        return;
                    }
                    // var that = this;
                    var url = place.icon;
                    var size = new google.maps.Size( 42, 57 );
                    if( window.devicePixelRatio > 1.5 ) {
                        if ( place.retinaIcon ) {
                            url = place.retinaIcon;
                            size = new google.maps.Size( 83, 113 );
                        }
                    }

                    var image = {
                        url: url,
                        size: size,
                        scaledSize: new google.maps.Size( 30, 51 ),
                        origin: new google.maps.Point( 0, 0 ),
                        anchor: new google.maps.Point( 21, 56 )
                    };

                    that.options_marker[i] = {
                        id: place.id,
                        map: that.map,
                        position: {
                            lat: parseFloat( place.lat ),
                            lng: parseFloat( place.lng )
                        },
                        icon: place.type_icon,
                        color: place.type_color,
                        title: place.title,
                        animation: google.maps.Animation.DROP
                    }

                    // The photograph is courtesy of the U.S. Geological Survey.
                    that.markers[i] = new CustomMarker( that.options_marker[i] );
                    that.bounds.extend( new google.maps.LatLng( that.markers[i].position.lat(), that.markers[i].position.lng() ) );

                    var boxText = document.createElement( 'div' );

                    boxText.className = 'map-info-preview media';

                    var meta = '<ul class="list-inline place-meta-list">';
                    if( place.metas ){
                        for ( x in place.metas ){
                            var m = place.metas[x];
                            meta += '<li><i class="icon-place-'+x+'"></i>' + m.value +'<span class="label-place">' + m.label + '</span></li>'
                        }
                    }
                    meta    += '</ul>';
                    boxText.innerHTML = wp.template( 'opallisting-place-grid-item' )( place );

                    var myOptions = {
                        content: boxText,
                        disableAutoPan: true,
                        maxWidth: 0,
                        alignBottom: true,
                        pixelOffset: new google.maps.Size( -122, -48 ),
                        zIndex: null,
                        closeBoxMargin: "0 0 -16px -16px",
                        closeBoxURL: opallistingJS.mapiconurl + 'close.png',
                        infoBoxClearance: new google.maps.Size( 1, 1 ),
                        isHidden: false,
                        pane: "floatPane",
                        enableEventPropagation: false
                    };

                    var infoBox = new InfoBox( myOptions );
                    that.attachInfoBoxToMarker( that.markers[i], infoBox );
                },

                // attach info box to marker when user click to marker
                attachInfoBoxToMarker: function( marker, infoBox ) {
                    // var that = this;
                    google.maps.event.addListener( marker, 'click', function(){
                        if( $( '[data-related="map"]' ).filter('[data-id="' + marker.id + '"]').length > 0 ){
                            var $m = $( '[data-related="map"]' ).filter('[data-id="'+marker.id+'"]');
                            $( '[data-related="map"]' ).removeClass( 'map-active' );
                            $m.addClass('map-active');

                            $('html, body').animate({
                                scrollTop: parseInt( $m.offset().top - $m.height() / 2 )
                            }, 1000);

                        }

                        if( that.infoBoxLast != null ){
                            that.infoBoxLast.close();
                        }

                        var scale = Math.pow( 2, that.map.getZoom() );
                        var offsety = ( (100/scale) || 0 );
                        var projection = that.map.getProjection();
                        var markerPosition = marker.position;
                        var markerScreenPosition = projection.fromLatLngToPoint( markerPosition );
                        var pointHalfScreenAbove = new google.maps.Point( markerScreenPosition.x, markerScreenPosition.y - offsety );
                        var aboveMarkerLatLng = projection.fromPointToLatLng( pointHalfScreenAbove );
                        that.map.setCenter( aboveMarkerLatLng );
                        infoBox.open( that.map, marker );
                        that.infoBoxLast = infoBox;
                        $('.infoBox').fitVids();
                    }, false);
                },

                // Get Location and set Map zoom, center
                geoLocation: function() {
                    // var that = this;
                    that.mapEle.addClass( 'processing' );
                    navigator.geolocation.getCurrentPosition(function( position ) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        if ( ! that.currentPosition ) {
                            that.currentPosition = pos;
                        }
                        that.map.setCenter( pos );

                        var radius_ele = $( '#opallisting-search-form input[name="radius"]' );
                        var radius = radius_ele.length === 1 ? parseInt( radius_ele.val() ) : 100;
                        var zoom = that.getZoomFromRadius( radius, {
                            width: that.mapEle.width(),
                            height: that.mapEle.height()
                        } );
                        that.map.setZoom( zoom );
                        that.mapEle.parent().find('.geo-location').addClass( 'active' );
                    }, function(){
                        that.mapEle.removeClass( 'processing' );
                    });
                    return false;
                },

                // calculator zoom from radius
                getZoomFromRadius: function( radius, mapDim ) {
                    // Calculates the target zoom level given a radius in miles
                    var mapWidth = mapDim.width;
                    var mapHeight = mapDim.height;

                    // var radiusMeters = radius * 1609.34; // converting miles to meters
                    var radiusMeters = radius * 1000;
                    var diameter = radiusMeters * 2;

                    var basePixelSize = 156543.03392799936; // pixel size in meters at zoom level 0
                    var pixelSize = Math.max((diameter / mapWidth), (diameter / mapHeight));

                    var zoomFactor = 2; // default for tiled basemap
                    var zoom = Math.log(basePixelSize / pixelSize) / Math.log(zoomFactor);

                    return Math.round(zoom);
                },

                // add google map listen event
                addDomEventListener: function() {
                    // var that = this;
                    // toggle geo location
                    if ( $( document.getElementById( 'opallisting-toggle-geolocation' ) ).length > 0 ) {
                        google.maps.event.addDomListener( document.getElementById( 'opallisting-toggle-geolocation' ), 'click', that.toggleGeolocation );
                    }
                    // lock mao
                    if ( $( document.getElementById( 'opallisting-toggle-lock-map' ) ).length > 0 ) {
                        google.maps.event.addDomListener( document.getElementById( 'opallisting-toggle-lock-map' ), 'click', that.toggleLockMap );
                    }

                    if ( that.drawingManager ) {
                        // overlaycomplete
                        google.maps.event.addListener( that.drawingManager, 'overlaycomplete', that.overlaycomplete );
                    }
                    // draw in map
                    if ( $( document.getElementById( 'opallisting-toggle-draw' ) ).length > 0 ) {
                        google.maps.event.addDomListener( document.getElementById( 'opallisting-toggle-draw' ), 'click', that.toggleDraw );
                    }

                    // directions
                    if ( $( document.getElementById( 'opallisting-toggle-direction' ) ).length > 0 ) {
                        google.maps.event.addDomListener( document.getElementById( 'opallisting-toggle-direction' ), 'click', toggleShowDirections );
                    }
                    if ( $( document.getElementById( 'opallisting-close-directions-panel' ) ).length > 0 ) {
                        google.maps.event.addDomListener( document.getElementById( 'opallisting-close-directions-panel' ), 'click', toggleShowDirections );
                    }

                    function toggleShowDirections() {
                        $( '#opallisting-directions-wrapper' ).toggleClass( 'active' );
                    }

                    // save places selections
                    $( document ).on( 'submit', '.opallisting-save-search-form, .opallisting-save-search-form-overlay', that.save_place_selection );
                    $('#radius_enabled').on( 'change', function(e){
                        e.preventDefault();
                        if ( $( this ).is( ':checked' ) ) {
                            that.geoLocation();
                        }
                        return false;
                    } );

                    // view draw selection
                    var selection = document.getElementById( 'opallisting-view-places-selection' );
                    if ( $( selection ).length === 1 ) {
                        selection.addEventListener( 'click', function(e){
                            e.preventDefault();

                            var self = $( this );
                            var form = $( '.opallisting-save-search-form-overlay' );

                            var data = form.serializeArray();
                            data.push( { name: 'get-param', value: 1 } );
                            that.do_place_selection_action( data, form );
                            return false;
                        }, false );
                    }

                    var sliders = $('.opallisting-slider-range');
                    for ( var i = 0; i < sliders.length; i++ ) {
                        var slider = $( sliders[i] );
                        slider.slider({
                            range: 'min',
                            min: slider.data('min'),
                            max: slider.data('max'),
                            value: slider.data('value'),
                            stop: function( e, ui ) {
                                slider.find('input[type="hidden"]').val( ui.value );
                                if ( ! $('#radius_enabled').is(':checked') ) {
                                    that.error( opallistingJS.i18n.map.SHARE_LOCATION );
                                } else {
                                    navigator.geolocation.getCurrentPosition(function( position ) {
                                        var pos = {
                                            lat: position.coords.latitude,
                                            lng: position.coords.longitude
                                        };

                                        if ( ! that.currentPosition ) {
                                            that.currentPosition = pos;
                                        }

                                        if ( slider.find( '.lat' ).length === 0 ) {
                                            slider.append( '<input type="hidden" value="' + position.coords.latitude + '" name="lat" class="lat" />' );
                                        } else {
                                            slider.find( '.lat' ).val( position.coords.latitude );
                                        }

                                        if ( slider.find( '.lng' ).length === 0 ) {
                                            slider.append( '<input type="hidden" value="' + position.coords.longitude + '" name="lng" class="lng" />' );
                                        } else {
                                            slider.find( '.lng' ).val( position.coords.longitude );
                                        }

                                        var form = slider.parents( 'form' );
                                        if ( form.length === 1 && history.pushState ) {
                                            form.find( '[name="paged"]' ).val(1);
                                            that.make_ajax_filter_places();
                                        }
                                    }, function(){
                                        that.error( opallistingJS.i18n.map.SHARE_LOCATION );
                                    });
                                }
                            },
                            slide: function( e, ui ) {
                                var text = ui.value + ' ' + slider.data('unit');
                                slider.parents('form').find('.radius').html( text );
                            }
                        });
                    }

                    if ( that.map ) {
                        google.maps.event.addListener( that.map, 'idle', function() {
                            that.mapEle.removeClass( 'processing' );
                        });

                        google.maps.event.addListener( that.map, 'bounds_changed', function() {
                            that.mapEle.addClass( 'processing' );
                        });
                    }

                    // addEventListener
                    var startPoint = document.getElementById( 'start-point' );
                    if ( $( startPoint ).length > 0 ) {
                        startPoint.addEventListener( 'keyup', function( e ){
                            if ( e.keyCode === 13 ) {
                                e.preventDefault();
                                return;
                            }
                            var val = $( startPoint ).val();
                            if ( val ) {
                                that.autocompleteService.getQueryPredictions( { input: val }, that.displaySuggestions );
                            } else {
                                $( '#opallisting-place-api-results' ).empty();
                            }
                        } );
                    }

                    var directionPoints = document.querySelectorAll( '.panel-actions a' );
                    if ( $( directionPoints ).length > 0 ) {
                        for ( var i = 0; i < directionPoints.length; i++ ) {
                            var type = directionPoints[i];
                            type.addEventListener( 'click', function(e){
                                $( directionPoints ).removeClass( 'active' );
                                $(this).addClass( 'active' );

                                if ( $( '#start-point' ).val() ) {
                                    that.autocompleteService.getQueryPredictions( { input: $( '#start-point' ).val() }, that.displaySuggestions );
                                }
                            }, false );
                        }
                    }

                    // trigger change
                    OpalListingHook.addAction( 'opallisting_trigger_filter_places', function( res ){
                        that.repaint_after_ajax( res );
                    }, 10, 1 );
                },

                repaint_after_ajax: function( res ) {
                    if ( typeof res.places !== 'undefined' ) {
                        if ( that.mapEle.length > 0 ) {
                            that.repaint( res.places );
                        }
                        $( '#opallisting-map-preview').removeClass( 'processing' );
                    }
                },

                displaySuggestions: function( predictions, status ) {
                    // var that = OPALLISTING_MAP;
                    var wrapList = $( '#opallisting-place-api-results' );
                    wrapList.empty();
                    if ( status != google.maps.places.PlacesServiceStatus.OK ) {
                        wrapList.append( '<li><a href="#">' + opallistingJS.i18n.map.ZERO_RESULTS + '</a></li>' );
                        return;
                    }

                    wrapList.append( '<li><a href="#" class="get-direction" data-placeid="0"><i class="fa fa-map-marker" aria-hidden="true"></i>' + opallistingJS.i18n.map.CURRENT_LOCATION + '</a></li>' );
                    predictions.forEach( function( prediction ) {
                        wrapList.append( '<li><a href="#" class="get-direction" data-placeid="' + prediction.place_id + '"><i class="fa fa-map-marker" aria-hidden="true"></i>' + prediction.description + '</a></li>' );
                    } );

                    var directions = document.getElementsByClassName( 'get-direction' );
                    var mode = $('#opallisting-directions-wrapper .panel-actions a.active');
                    if ( mode.length == 0 ) return;

                    mode = mode.data('mode');
                    for ( var i = 0; i < directions.length; i++ ) {
                        var direct = directions[i];
                        direct.addEventListener( 'click', function( e ){
                            e.preventDefault();
                            var self = $( this );
                            var place_id = self.data( 'placeid' );
                            var dist = that.markerClusterer.getMarkers()[0];
                            $('#start-point').val( self.text() );
                            if ( place_id == 0 ) {
                                that.mapEle.removeClass( 'processing' );
                                if ( ! that.currentPosition && ! navigator.geolocation ) {
                                    that.error( opallistingJS.i18n.map.SHARE_LOCATION );
                                    // clear direction
                                    that.directionsDisplay.setDirections( null );
                                    that.directionsDisplay = null;
                                } else {
                                    if ( ! that.currentPosition ){
                                        navigator.geolocation.getCurrentPosition(function( position ) {
                                            var pos = {
                                                lat: position.coords.latitude,
                                                lng: position.coords.longitude
                                            };

                                            that.currentPosition = pos;
                                            that.calculateAndDisplayRoute( that.currentPosition, dist, mode );
                                        }, function(){
                                            that.error( opallistingJS.i18n.map.SHARE_LOCATION );
                                        });
                                    } else {
                                        that.calculateAndDisplayRoute( that.currentPosition, dist, mode );
                                    }
                                }
                            } else {
                                // place request
                                that.placeService.getDetails( {
                                    placeId: place_id
                                }, function( place, status ){
                                    that.mapEle.removeClass( 'processing' );
                                    wrapList.empty();
                                    if ( status == google.maps.places.PlacesServiceStatus.OK ) {
                                        var start = {
                                            lat: place.geometry.location.lat(),
                                            lng: place.geometry.location.lng()
                                        }
                                        that.calculateAndDisplayRoute( start, dist, mode );
                                    } else {
                                        that.directionsDisplay.setDirections( null );
                                        that.directionsDisplay = null;
                                        wrapList.append( '<li><a href="#">' + opallistingJS.i18n.map.NO_DIRECTION + '</a></li>' );
                                    }
                                } );
                            }
                        }, false );
                    }
                },

                calculateAndDisplayRoute: function ( current, dest, mode ) {
                    // var that = this;

                    var current = new google.maps.LatLng( current.lat, current.lng );
                    var dest = new google.maps.LatLng( dest.position.lat(), dest.position.lng() );
                    that.directionsService.route({
                        origin: current,
                        destination: dest,
                        avoidTolls: true,
                        avoidHighways: false,
                        travelMode: google.maps.TravelMode[mode]
                    }, function( response, status ) {
                        if ( status == google.maps.DirectionsStatus.OK ) {
                            that.directionsDisplay.setDirections(response);
                            if ( ! that.infowindow2 ) {
                                that.infowindow2 = new google.maps.InfoWindow();
                            }
                            that.infowindow2.setContent( response.routes[0].legs[0].distance.text + "<br>" + response.routes[0].legs[0].duration.text + ' ' );

                            var total = response.routes[0].legs[0].steps.length;
                            that.infowindow2.setPosition( response.routes[0].legs[0].steps[total / 2].end_location );
                            that.infowindow2.open( that.map );
                        } else {
                            that.mapEle.removeClass( 'processing' );
                            // alert an error message when the route could nog be calculated.
                            if (status == 'ZERO_RESULTS') {
                                that.error( opallistingJS.i18n.map.ZERO_RESULTS );
                            } else if (status == 'UNKNOWN_ERROR') {
                                that.error( opallistingJS.i18n.map.UNKNOWN_ERROR );
                            } else if (status == 'REQUEST_DENIED') {
                                that.error( opallistingJS.i18n.map.REQUEST_DENIED );
                            } else if (status == 'OVER_QUERY_LIMIT') {
                                that.error( opallistingJS.i18n.map.OVER_QUERY_LIMIT );
                            } else if (status == 'NOT_FOUND') {
                                that.error( opallistingJS.i18n.map.NOT_FOUND );
                            } else if (status == 'INVALID_REQUEST') {
                                that.error( opallistingJS.i18n.map.INVALID_REQUEST );
                            } else {
                                that.error( opallistingJS.i18n.map.ERROR + status);
                            }
                        }
                    });
                },

                // toggle geolocation
                toggleGeolocation: function( e ) {
                    e.preventDefault();
                    // var that = OPALLISTING_MAP;
                    var ele = $( '#opallisting-toggle-geolocation' );
                    ele.toggleClass( 'active' );
                    if ( ele.hasClass('active') && navigator.geolocation ) {
                        that.geoLocation();
                    } else {
                        that.map.fitBounds( that.bounds );
                    }
                    return false;
                },

                // toggle lock - unlock map
                toggleLockMap: function( e ) {
                    e.preventDefault();
                    // var that = OPALLISTING_MAP;
                    var ele = $( '#opallisting-toggle-lock-map' );
                    ele.toggleClass( 'active' );
                    if ( that.polygon ) {
                        that.polygon.setMap( null );
                    }
                    if ( that.directionsDisplay ) {
                        that.directionsDisplay.setMap( null );
                    }
                    if ( that.drawingManager ) {
                        that.drawingManager.setDrawingMode( null );
                        that.drawingManager.setMap( null );
                    }

                    if ( that.infowindow2 ) {
                        that.infowindow2.setMap( null );
                        that.infowindow2.setContent( null );
                        that.infowindow2.setPosition( null );
                    }
                    if ( ele.hasClass('active')) {
                        that.map.setOptions({ scrollwheel: true, draggable: true });
                        $( this ).find( 'i' ).replaceWith( '<i class="fa fa-unlock-alt"></i>' );
                        $( '#opallisting-toggle-draw' ).removeClass( 'active' );

                        // reset mapOverlay and polygon
                        that._reset();
                    } else {
                        $( this ).find( 'i' ).replaceWith( '<i class="fa fa-lock"></i>' );
                        that.map.setOptions({ scrollwheel: false, draggable: false  });
                    }

                    return false;
                },

                // toggle draw mode
                toggleDraw: function( e ) {
                    e.preventDefault();
                    // var that = OPALLISTING_MAP;
                    var ele = $( '#opallisting-toggle-draw' );
                    ele.toggleClass( 'active' );
                    that._reset();
                    if ( ele.hasClass('active') ) {
                        if ( that.polygon ) {
                            that.polygon.setMap( null );
                        }
                        that.drawingManager.setDrawingMode( 'polygon' );
                        that.drawingManager.setMap( that.map );
                        that.markerClusterer.resetViewport_( true );
                    } else {
                        if ( that.mapOverlay ) {
                            that.mapOverlay.setMap( null );
                        }
                        // that.markerClusterer.repaint();
                        that.repaint( that.places, true );
                    }
                    that.polygon = null;
                    return false;
                },

                _reset: function() {
                    // var that = this;
                    if ( that.mapOverlay ) {
                        that.mapOverlay.setMap( null );
                    }
                    if ( that.polygon ) {
                        that.polygon.setOptions( { 'paths' : [] } );
                        that.polygon.setMap( null );
                    }
                    if ( that.directionsDisplay ) {
                        that.directionsDisplay.setMap( null );
                    }
                },

                // overlay completed
                overlaycomplete: function( e ) {
                    // Disable drawingManager
                    // var that = OPALLISTING_MAP;
                    that.mapOverlay = e.overlay;
                    // overlayPaths = window.overlayPaths = that.mapOverlay.getPaths();
                    that.drawingManager.setDrawingMode(null);

                    $.magnificPopup.open({
                        items: {
                            src: '#opallisting-save-places-selection'
                        },
                        type: 'inline',
                        callbacks: {
                            close: function() {
                                $( '#opallisting-save-places-selection' ).find('.opallisting-messages').remove();

                                if ( that.mapOverlay ) {
                                    that.mapOverlay.setMap( null );
                                }
                                if ( that.directionsDisplay ) {
                                    that.directionsDisplay.setMap( null );
                                }
                                that.drawingManager.setDrawingMode( 'polygon' );
                                that.drawingManager.setMap( that.map );
                                that.markerClusterer.resetViewport_( true );
                            }
                        },
                        mainClass: 'opallisting'
                    });
                },

                do_place_selection_action: function( data, form ) {
                    // var that = this;
                    var overlayPaths = that.mapOverlay ? that.mapOverlay.getPaths() : [];
                    var button = form.find('.submit');
                    var bounds = null;
                    if ( overlayPaths.length > 0 ) {
                        var pathsArray = overlayPaths.getArray();
                        pathsArray = pathsArray[0].b;
                        var bounds = new google.maps.LatLngBounds();
                        for ( var i = 0; i < pathsArray.length; i++ ) {
                            data.push( { name: 'lats[]', value: pathsArray[i].lat() } );
                            data.push( { name: 'lngs[]', value: pathsArray[i].lng() } );

                            // custom bounds
                            bounds.extend( new google.maps.LatLng( pathsArray[i].lat(), pathsArray[i].lng() ) );
                        }
                    }
                    $.each( $('#opallisting-places-ajax').data(), function( index, value) {
                        data.push( { name: index, value: value } );
                    } );

                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: opallistingJS.ajaxurl,
                        data: data,
                        beforeSend: function() {
                            form.addClass( 'processing' );
                            form.find( '.has-error' ).removeClass( 'has-error' );
                            form.find( '.opallisting-messages' ).remove();
                            if ( typeof $.fn.button !== 'undefined' ) {
                                button.button('loading');
                            }
                        }
                    }).always(function(){
                        form.removeClass( 'processing' );
                        if ( typeof $.fn.button !== 'undefined' ) {
                            button.button('reset');
                        }
                    }).done(function( res ){
                        if ( typeof res.status === 'undefined' ) {
                            return;
                        }

                        if ( typeof res.params !== 'undefined' ) {
                            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?'+ res.params;
                            window.history.pushState( { path:newurl }, '', newurl );
                        }

                        if ( res.status === false && typeof res.fields !== 'undefined' && res.fields.length > 0 ) {
                            for ( var i = 0; i < res.fields.length; i++ ) {
                                var input = form.find( '[name="'+res.fields[i]+'"]' ).parents( '.form-row:first' ).addClass( 'has-error' );
                            }
                        }

                        if ( typeof res.message !== 'undefined' ) {
                            form.append( res.message );
                        }

                        if ( res.status === true ) {
                            form.find( '.opallisting-messages' ).delay(2000).queue( function(){
                                $(this).remove();
                                $.magnificPopup.close();
                            } );
                        }

                        if ( res.status === true ) {
                            form.find( 'input[type="text"], input[type="email"], input[type="tel"], textarea' ).val('');
                        }

                        if ( typeof res.places !== 'undefined' ) {
                            if ( bounds ) {
                                // set center map focus polygon initialized
                                that.map.fitBounds( bounds );
                                that.map.panToBounds( bounds );
                            } else if ( $('#opallisting-map-preview').length == 1 ) {
                                that.repaint( res.places );
                            }
                            $( '#opallisting-map-preview').removeClass( 'processing' );
                        }

                        if ( typeof res.html !== 'undefined' ) {
                            $( '#opallisting-places-ajax' ).replaceWith( res.html );
                            $( '#opallisting-places-ajax' ).removeClass( 'processing' );
                        }

                        if ( res.status === true ) {
                            $.magnificPopup.close();
                        }

                        if ( that.drawingManager ) {
                            that.drawingManager.setMap( null );
                        }
                        if ( that.mapOverlay ) {
                            that.mapOverlay.setMap( that.map );
                        }
                    });
                },

                // save places selection
                save_place_selection: function( e ){
                    e.preventDefault();
                    // var that = OPALLISTING_MAP;
                    var form = $( this );
                    var button = form.find('.submit');
                    var params = window.location.search.substring(1);

                    var data = form.serializeArray();
                    data.push( { name: 'params', 'value': params } );
                    that.do_place_selection_action( data, form );
                    return false;
                },

                // init polygon
                initPolygon: function( data ) {
                    if ( typeof data === 'undefined' ) {
                        data = opallistingJS.polygon;
                    }
                    if ( data.length == 0 ) return;

                    // var that = this;
                    var bounds = new google.maps.LatLngBounds();
                    var coords = [];
                    for ( var i = 0; i < data.length; i++ ) {
                        var latlng = new google.maps.LatLng( data[i].lat, data[i].lng );
                        coords.push( latlng );
                        // set new custom bounds
                        bounds.extend( latlng );
                    }

                    // set center map focus polygon initialized
                    that.map.fitBounds( bounds );
                    that.map.panToBounds( bounds );

                    var options = $.extend( {}, {
                        paths: coords
                    }, that.getPolygonOptions() );
                    // Styling & Controls
                    that.polygon = new google.maps.Polygon( options );

                    that.polygon.setMap( that.map );

                    var markerIDs = places = [];
                    for ( var i = 0; i < that.markers.length; i++ ) {
                        if ( ! google.maps.geometry.poly.containsLocation( that.markers[i].getPosition(), that.polygon ) ) {
                            that.markerClusterer.removeMarker( that.markers[i], true );
                        } else {
                            markerIDs.push( that.markers[i].id );
                        }
                    }

                },

                // open window error
                error: function( message ) {
                    // var that = this;
                    var html = wp.template( 'map-error-message' )({ 'message': message });
                    that.mapEle.find( '.overlay' ).remove();
                    that.mapEle.append( html );

                    // add close event handler
                    var close = document.getElementById( 'opallisting-close-map-message' );
                    if ( $( close ).length > 0 ) {
                        close.addEventListener( 'click', function(e){
                            e.preventDefault();
                            that.closeError();
                        }, false );
                    }
                },

                // close window error
                closeError: function() {
                    // var that = this;
                    that.mapEle.find( '.overlay' ).remove();
                },

                make_ajax_filter_places: function( data, callback ) {
                    // stop current ajax
                    if ( that.ajax ) {
                        if ( that.ajax.state() == 'pending' ) {
                            that.ajax.abort();
                        }
                    }
                    that.runed = true;

                    var searchForm = $( '#opallisting-search-form' );
                    var formData = searchForm.serializeArray();
                    if ( typeof data !== 'undefined' ) {
                        data = $.merge( formData, data );
                        data = $.merge( $( '.opallisting-search-ajax-filter input' ).serializeArray(), data );
                    } else {
                        data = formData;
                    }

                    var html = wp.template('opallisting-loading')({});
                    var div = $( '#opallisting-places-ajax' );
                    var layout = div.data('layout');
                    var columns = div.data('columns');
                    var exclude_map = div.data( 'exclude_map' );
                    var limit = div.data('limit');
                    var breadcrumb = div.data( 'breadcrumb' );

                    data = $.merge( data, [
                            {
                                name: 'action',
                                value: 'opallisting_get_places'
                            },
                            {
                                name: 'layout',
                                value: layout
                            },
                            {
                                name: 'columns',
                                value: columns
                            },
                            {
                                name: 'limit',
                                value: limit
                            },
                            {
                                name: 'exclude_map',
                                value: exclude_map
                            }
                        ]);

                    if ( breadcrumb !== undefined || breadcrumb === 'false' ) {
                        data.push({
                            name: 'breadcrumb',
                            value: 0
                        });
                    }

                    for ( var i = 0; i < data.length; i++ ) {
                        if ( data[i].value == -1 ) {
                            data.splice( i, 1 );
                        }
                    }

                    if ( that.runed && history.pushState ) {
                        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + $.param( data );
                        window.history.pushState( { path: newurl }, '', newurl );
                    }

                    // sync places and maps
                    that.ajax = $.ajax({
                        type: 'GET',
                        url: opallistingJS.ajaxurl,
                        data: data,
                        beforeSend: function() {
                            $( '#opallisting-places-ajax').addClass( 'processing' );
                            $( '#opallisting-map-preview').addClass( 'processing' );
                        }
                    }).always(function(){
                        $( '#opallisting-map-preview' ).removeClass( 'processing' );
                        $( '#opallisting-places-ajax' ).removeClass( 'processing' );
                    }).done(function( res ){
                        if ( typeof res.places !== 'undefined' ) {
                            if ( that.mapEle.length > 0 ) {
                                that.repaint( res.places );
                            }
                            $( '#opallisting-map-preview').removeClass( 'processing' );
                        }

                        if ( typeof res.html !== 'undefined' ) {
                            $( '#opallisting-places-ajax' ).replaceWith( res.html );

                            var winTop = $(window).scrollTop();
                            var EleTop = $( '#opallisting-places-ajax' ).length > 0 ? $( '#opallisting-places-ajax' ).offset().top : 0;
                            if ( $( '#wpadminbar' ).length > 0 ) {
                                EleTop = EleTop - $( '#wpadminbar' ).height();
                            }
                            if ( EleTop < winTop ) {
                                $( 'body' ).animate({
                                    scrollTop: EleTop
                                }, 800 );
                            }
                            // place map
                            OpalListing_Places_Map_Layout.initMap();
                            $( '#opallisting-places-ajax' ).removeClass( 'processing' );
                        }

                        if ( typeof callback == 'function' ) {
                            callback.apply( null, [ res ] );
                        }
                    });
                }

            };
            that.initMap( this, settings );
        });

    };

    $.fn.opallisting_map_street_view = function( options ) {
        return this.each(function() {
            options = $.extend( {}, {
                position: {
                    lat: $( this ).data( 'lat' ) ? parseFloat( $( this ).data( 'lat' ) ) : 54.800685,
                    lng: $( this ).data( 'lng' ) ? parseFloat( $( this ).data( 'lng' ) ) : -4.130859,
                },
                pov: {
                    heading: $( this ).data( 'heading' ) ? parseInt( $( this ).data( 'heading' ) ) : 165,
                    pitch: $( this ).data( 'pitch' ) ? parseInt( $( this ).data( 'pitch' ) ) : 0,
                },
                zoom: $( this ).data( 'zoom' ) ? parseInt( $( this ).data( 'zoom' ) ) : 10,
            }, options );

            new google.maps.StreetViewPanorama( this, options );
        });
    };

    // Map Street View
    var OPALLISTING_MAP_STREET_VIEW = window.OPALLISTING_MAP_STREET_VIEW = {

        map: null,

        mapID: null,

        mapEle: null,

        initMap: function( eleID, options ) {
            if ( typeof google === 'undefined' || typeof eleID === 'undefined' ) return;

            var that = this;

            // set MapID
            that.mapID = eleID;

            that.mapEle = $( '#' + eleID );

            that.map = new google.maps.StreetViewPanorama( document.getElementById( that.mapID ), this.getOptions( options ) );
        },

        // map street view options
        getOptions: function( options ) {
            options = typeof options == 'undefined' ? {} : options;
            var that = this;

            return $.extend( {}, {
                position: {
                    lat: that.mapEle.data( 'lat' ) ? parseFloat( that.mapEle.data( 'lat' ) ) : 54.800685,
                    lng: that.mapEle.data( 'lng' ) ? parseFloat( that.mapEle.data( 'lng' ) ) : -4.130859,
                },
                pov: {
                    heading: that.mapEle.data( 'heading' ) ? parseInt( that.mapEle.data( 'heading' ) ) : 165,
                    pitch: that.mapEle.data( 'pitch' ) ? parseInt( that.mapEle.data( 'pitch' ) ) : 0,
                },
                zoom: that.mapEle.data( 'zoom' ) ? parseInt( that.mapEle.data( 'zoom' ) ) : 10,
            }, options );
        }

    };

    // TEST
    $( 'form#opallisting-search-form.ajax-change' ).submit( function ( e ) {
        e.preventDefault();
        opallisting_make_ajax_filter_places( {}, function( res ){
            OpalListingHook.doAction( 'opallisting_trigger_filter_places', res );
        } );
        return false;
    } );

    $( document ).on( 'click', '.opallisting-search-ajax-filter .checkbox-button', function(e){
        e.preventDefault();
        $(this).toggleClass( 'active' );
        if ( $(this).hasClass('active') ) {
            $(this).find( 'input' ).attr('checked', true);
        } else {
            $(this).find('input').attr('checked', false);
        }
        var data = $(this).parents('.opallisting-search-ajax-filter:first').find('input').serializeArray();

        opallisting_make_ajax_filter_places( data, function( res, data ){
            OpalListingHook.doAction( 'opallisting_trigger_filter_places', res );
        } );
        return false;
    } );

    $( document ).on( 'change', '.opallisting-search-ajax-filter input', function(e){
        e.preventDefault();

        var wraper = $(this).parents('.opallisting-search-ajax-filter:first');
        var data = wraper.find( 'input' ).serializeArray();

        data.push({
            name: 'paged',
            value: 1
        });

        opallisting_make_ajax_filter_places( data, function( res, data ){
            OpalListingHook.doAction( 'opallisting_trigger_filter_places', res );
        } );
        return false;
    } );

    $( document ).on( 'click', '.opallisting-places-wrapper .pagination li', function( e ) {
        e.preventDefault();
        opallisting_make_ajax_filter_places( [{ name: 'paged', value: $(this).data('paged') }], function( res ){
            OpalListingHook.doAction( 'opallisting_trigger_filter_places', res );
                OpalListing_Places_Map_Layout.initMap();
        } );
        return false;
    } );
    // END TEST

    var ajax = runed = false;
    function opallisting_make_ajax_filter_places( data, callback ) {
        // stop current ajax
        if ( ajax ) {
            if ( ajax.state() == 'pending' ) {
                ajax.abort();
            }
        }
        runed = true;

        var searchForm = $( '#opallisting-search-form' );
        var formData = searchForm.serializeArray();
        if ( typeof data !== 'undefined' ) {
            data = $.merge( formData, data );
            data = $.merge( $( '.opallisting-search-ajax-filter input' ).serializeArray(), data );
        } else {
            data = formData;
        }

        var html = wp.template('opallisting-loading')({});
        var div = $( '#opallisting-places-ajax' );
        var layout = div.data('layout');
        var columns = div.data('columns');
        var exclude_map = div.data( 'exclude_map' );
        var limit = div.data('limit');
        var breadcrumb = div.data( 'breadcrumb' );

        data = $.merge( data, [
                {
                    name: 'action',
                    value: 'opallisting_get_places'
                },
                {
                    name: 'layout',
                    value: layout
                },
                {
                    name: 'columns',
                    value: columns
                },
                {
                    name: 'limit',
                    value: limit
                },
                {
                    name: 'exclude_map',
                    value: exclude_map
                }
            ]);

        if ( breadcrumb !== undefined || breadcrumb === 'false' ) {
            data.push({
                name: 'breadcrumb',
                value: 0
            });
        }

        for ( var i = 0; i < data.length; i++ ) {
            if ( data[i].value == -1 ) {
                data.splice( i, 1 );
            }
        }

        if ( runed && history.pushState ) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + $.param( data );
            window.history.pushState( { path: newurl }, '', newurl );
        }

        // sync places and maps
        ajax = $.ajax({
            type: 'GET',
            url: opallistingJS.ajaxurl,
            data: data,
            beforeSend: function() {
                $( '#opallisting-places-ajax').addClass( 'processing' );
                $( '#opallisting-map-preview').addClass( 'processing' );
            }
        }).always(function(){
            $( '#opallisting-map-preview' ).removeClass( 'processing' );
            $( '#opallisting-places-ajax' ).removeClass( 'processing' );
        }).done(function( res ){
            // if ( typeof res.places !== 'undefined' ) {
            //     if ( that.mapEle.length > 0 ) {
            //         that.repaint( res.places );
            //     }
            //     $( '#opallisting-map-preview').removeClass( 'processing' );
            // }

            if ( typeof res.html !== 'undefined' ) {
                $( '#opallisting-places-ajax' ).replaceWith( res.html );

                var winTop = $(window).scrollTop();
                var EleTop = $( '#opallisting-places-ajax' ).length > 0 ? $( '#opallisting-places-ajax' ).offset().top : 0;
                if ( $( '#wpadminbar' ).length > 0 ) {
                    EleTop = EleTop - $( '#wpadminbar' ).height();
                }
                if ( EleTop < winTop ) {
                    $( 'body' ).animate({
                        scrollTop: EleTop
                    }, 800 );
                }
                // place map
                OpalListing_Places_Map_Layout.initMap();
                $( '#opallisting-places-ajax' ).removeClass( 'processing' );
            }

            if ( typeof callback == 'function' ) {
                callback.apply( null, [ res ] );
            }
        });
    }

    $( document ).ready(function() {
        // initialize map
        if ( $( '#opallisting-map-preview:not(.not-init)' ).length > 0 ) {
            $('#opallisting-map-preview').opallisting_map();
        }
        // street view
        if ( $( '#opallisting-map-street-view:not(.not-init)' ).length > 0 ) {
            $( '#opallisting-map-street-view' ).opallisting_map_street_view();
        }

    });

    var headerActions = document.querySelectorAll( '.header-actions a' );
    var map_inited = street_view_inited = false;
    for ( var i = 0; i < headerActions.length; i++ ) {
        var action = headerActions[i];
        action.addEventListener( 'click', function(e){
            e.preventDefault();
            var self = $( this );
            var type = self.data( 'type' );
            var tab = $( '#section-' + type );

            var parent = self.parents( '.opallisting-single-header:first' );
            parent.find( '.section' ).addClass( 'hide' );
            tab.removeClass( 'hide' ).addClass( 'fade in' );

            $( '.header-actions a' ).removeClass( 'active' );
            self.addClass( 'active' );

            if ( type == 'map' && ! map_inited ) {
                map_inited = true;
                $( '#opallisting-map-preview' ).opallisting_map();
            } else if ( type == 'street-view' && ! street_view_inited ) {
                street_view_inited = true;
                $( '#opallisting-map-street-view' ).opallisting_map_street_view();
            }
        }, false );
    }

})(jQuery);
