@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    <div>
        <h3>Edit Venue Image #{{ $venueimage->id }}</h3>
        <div>
            <a href="{{ url('/admin/venue-images') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif
            {!! Form::model($venueimage, [
            'method' => 'PATCH',
            'url' => ['/admin/venue-images', $venueimage->id],
            'class' => 'form-horizontal',
            'files' => true
            ]) !!}
            @include ('admin.venue-images.form', ['submitButtonText' => 'Update'])
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection