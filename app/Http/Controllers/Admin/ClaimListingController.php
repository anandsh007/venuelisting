<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ClaimListing;
use Illuminate\Http\Request;

class ClaimListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $claimlisting = ClaimListing::where('email', 'LIKE', "%$keyword%")
                ->orWhere('message', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $claimlisting = ClaimListing::paginate($perPage);
        }

        return view('admin.claim-listing.index', compact('claimlisting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.claim-listing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        ClaimListing::create($requestData);

        return redirect('admin/claim-listing')->with('flash_message', 'ClaimListing added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $claimlisting = ClaimListing::findOrFail($id);

        return view('admin.claim-listing.show', compact('claimlisting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $claimlisting = ClaimListing::findOrFail($id);

        return view('admin.claim-listing.edit', compact('claimlisting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $claimlisting = ClaimListing::findOrFail($id);
        $claimlisting->update($requestData);

        return redirect('admin/claim-listing')->with('flash_message', 'ClaimListing updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ClaimListing::destroy($id);

        return redirect('admin/claim-listing')->with('flash_message', 'ClaimListing deleted!');
    }
}
