@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    <div>
        <h3>Venue Image {{ $venueimage->id }}</h3>
        <div>
            <a href="{{ url('/admin/venue-images') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/admin/venue-images/' . $venueimage->id . '/edit') }}" title="Edit VenueImage"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
            'method'=>'DELETE',
            'url' => ['admin/venueimages', $venueimage->id],
            'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-sm',
            'title' => 'Delete VenueImage',
            'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $venueimage->id }}</td>
                        </tr>
                        <tr><th> Venue Id </th><td> {{ $venueimage->venue_id }} </td></tr><tr><th> Image Name </th><td> {{ $venueimage->image_name }} </td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection