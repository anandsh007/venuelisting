<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueVideo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue_videos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['venue_id', 'video_name'];

    public function venues()
    {
        return $this->belongsTo('App\Venue','venue_id');
    }
}
