<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenuePackage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue_packages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['package_name','cost_per_person','venue_id'];

    public function venues()
    {
        return $this->belongsTo('App\Venue','venue_id');
    }
}
