@extends('layouts.app') 
@section('styles')
<!-- Styles  start-->
<!--   trending Categories slider oul style meta-->
<meta charset="utf-8">
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Touch enabled jQuery plugin that lets you create beautiful responsive carousel slider.">
    <meta name="author" content="David Deutsch">
    <!--   trending Categories slider oul style mate-->
    <link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/owl.theme.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/kingcomposer.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/home.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" type="text/css">
     <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/slick-theme.css')}}"/>
    <!--   trending Categories slider oul style css-->
    <style type="text/css">
       body.kc-css-system .kc-css-203701 {
           background: transparent url('images/businessmen.jpg') 0% 0%/cover no-repeat fixed;
       }
    </style>

@endsection 
@section('content')
<body class="home page-template-default page page-id-4420 kingcomposer kc-css-system masthead-fixed" cz-shortcut-listen="true">
    <!-- #masthead -->
    <!-- background video Start Code-->
    <section id="main-container" class="container-fluid inner">
        <div class="row">

            <div id="main-content" class="main-content ">
                <div id="primary" class="content-area">
                    <div id="content" class="site-content" role="main">

                        <div id="post-4420" class="post-4420 page type-page status-publish hentry">
                            <div class="entry-content-page">
                                <div class="kc_clfw"></div>
                                <section class="kc-elm kc-css-690791 kc_row">

                                    <div class="kc-wrap-columns">
                                        <div class="kc-elm kc-css-415536 kc_col-sm-12 kc_column kc_col-sm-12">

                                            <div id="MainVideo" class="MobileViewVideo" style="width: 100%; height:500px; position: relative;" data-vide-bg="../public/video/ocean" data-vide-options="position: 0% 50%">
                                                <div style="position: absolute; z-index: -1; top: 0px; left: 0px;
                                                      bottom: 0px; right: 0px; overflow: hidden; background-size:
                                                      cover; background-color: transparent; background-repeat:
                                                      no-repeat; background-position: 0% 50%; background-image:
                                                      none;">
                                                     <video autoplay="" loop="" muted="" style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 0%; transform: translate(0%, -50%);visibility: visible; opacity: 1; width: 100%; height: auto;">
                                                        <source src="{{url('../public/video/wedding.mp4')}}" type="video/mp4">
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--   background video ended Code-->
    <section class="kc-elm kc-css-946 kc_row">
        <div class="kc-row-container  kc-container">
            <div class="kc-wrap-columns">
                <div class="kc-elm kc-css-96451 kc_col-sm-12 kc_column kc_col-sm-12">
                    <div class="kc-col-container">
                        <div class="kc-elm kc-css-157702 kc_text_block highlight">
                            <ul class="opallisting-listing-types-list horizontal">
                                @foreach($venueSubCategories->slice(0,6) as $venueSubCategory)
                                <li>
                                    <a href="{{ route('search.search') }}?occasionId={{ $venueSubCategory->id }}"    data-type="11053" class="" title="{{$venueSubCategory->name}}">
                                    <i class="{{$venueSubCategory->font_awesome_icon }}"></i></a>
                                </li>
                                @endforeach 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="kc-elm kc-css-670160 kc_row">
        <div class="kc-row-container  kc-container">
            <div class="kc-wrap-columns">
                <div class="kc-elm kc-css-615048 kc_col-sm-12 kc_column kc_col-sm-12">
                    <div class="kc-col-container">
                        <div id=" " class="kc-elm kc-css-735420 kc_row kc_row_inner">
                            <div class="kc-elm kc-css-798707 kc_col-sm-12 kc_column_inner kc_col-sm-12">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="kc_shortcode kc_single_image "><img src="{{url('/images/logo-1.png')}}" class="" alt=""></div>
                                    <div class="kc-elm kc-css-187973 kc_text_block">
                                        <p>Explore This City</p>
                                    </div>
                                    <div class="kc-elm kc-css-501636 kc_row kc_row_inner">
                                        <div class="kc-elm kc-css-464064 kc_col-sm-2 kc_column_inner kc_col-sm-2">
                                            <div class="kc_wrapper kc-col-inner-container"></div>
                                        </div>
                                        <div class="kc-elm kc-css-382711 kc_col-sm-8 kc_column_inner kc_col-sm-8">
                                            <div class="kc_wrapper kc-col-inner-container">
                                                <div class="kc-elm kc-css-108448 kc_text_block">
                                                    <p>Let’s uncover the best places to eat, drink, and shop nearest to you.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kc-elm kc-css-84687 kc_col-sm-2 kc_column_inner kc_col-sm-2">
                                            <div class="kc_wrapper kc-col-inner-container"></div>
                                        </div>
                                    </div>
                                    <div class="widget widget_opallisting_search_places_widget kc-elm kc-css-539876">
                                        <form role="form" action="{!!route('search.search')!!}" method="get">
                                            {{-- {{ csrf_field() }} --}}
                                            <div class="row opal-row">
                                                <div class="col-lg-10 col-md-10 col-sm-10">
                                                    <div class="row opal-row">                        
                                                        <div class="col-md-4 col-lg-4">
                                                            <select class="js-example-basic-single" name="occasionId"><option></option>
                                                            @foreach($occasionId as $key => $occasion)
                                                            <option value="{{$key}}">
                                                                {{$occasion}}
                                                            </option>
                                                            @endforeach
                                                            </select>
                                                          </div>
                                                          <div class="col-md-4 col-lg-4 form-group {{ $errors->has('MapsAutoComplete') ? 'has-error' : ''}}">
                                                            @if($errors->any())
                                                                <h4>{{$errors->first('', '<p class="help-block">:message</p>')}}
                                                                </h4> 
                                                            @endif
                                                            <input id="search_address" class="controls" placeholder="Search Your City" type="text">
                                                            <input type="hidden" name="latitude" id="lat">
                                                            <input type="hidden" name="longitude" id="long">  
                                                        {!! $errors->first('MapsAutoComplete','<p class="help-block">:message</p>') !!}
                                                        </div>
                                                          <div class="col-md-4 col-lg-4 form-group">
                                                            <input id="" name="keywords" placeholder="Keywords" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-2">

                                                    <button type="submit" class="btn btn-default home-search-button">
                                                     Search </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    Compaleted Trending Categories  -->
     <section>
       <div class="trending-categories-heading">
            <h2 class="heading-block-Trending">Featured Categories</h2>
        </div>
            <div class="owl-carousel owl-theme mobile-view-box-center">
                {{-- @foreach($venueSubSubCategories->slice(0,7) as $venueSubSubCategory)
                <div style="width: 198.6px;">
                    <div class="item item-title-block-hover">
                        <a href="{!! route('search.search', ['District' => 'Lucknow',
                    'Category' => str_replace(' ', '-', $venueSubSubCategory->Name)])!!}">
                        <img class="lazyOwl" src="../public/venuecategories/{!!$venueSubSubCategory->ImageName!!}"alt="" >
                    </a>
                        <h4 class="featured-categories-hedding-name">
                     <a>{{$venueSubSubCategory->Name}}</a>
              </h4>
                    </div>
                </div>
                @endforeach --}}
            </div>

    </section>
    {{-- <section class="kc-elm kc-css-736902 kc_row">
    <div class="kc-row-container  kc-container">
        <div class="kc-wrap-columns">
            <div class="kc-elm kc-css-162721 kc_col-sm-12 kc_column kc_col-sm-12">
                <div class="kc-col-container">
                    <div class="element-block-heading style-v3">
                        <div class="inner">
                            <h2 class="heading">Trending Categories</h2>

                        </div>
                    </div>
                    
                    <div class="widget widget_opallisting_place_categories_carousel_widget kc-elm kc-css-841800">
                        <div class="owl-carousel-play">
                            <div class="opallisting-carousel" data-items="5" data-lazyload="1" data-navigation="1" data-pagination="1">
                                @foreach($venueSubSubCategories->slice(0,7) as $venueSubSubCategory)
                                <div class="item">
                                   
                                    <a href="{!! route('search.search', ['District' => 'Lucknow',
                                         'Category' => str_replace(' ', '-', $venueSubSubCategory->Name)])!!}">
                                        <img class="lazyOwl" src="../public/venuecategories/{!!$venueSubSubCategory->ImageName!!}" alt=" " />
                                    </a>
                                    <h4 class="owl-carousel-item-title">
                                    <a href="{!! route('search.search', ['District' => 'Lucknow',
                                    'Category' => str_replace(' ', '-', $venueSubSubCategory->Name)])!!}">{{$venueSubSubCategory->Name}}</a>
                                    </h4>

                                </div>
                               @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}

    <!--         ==========================================================================================-->
    {{--
    <section class="kc-elm kc-css-183558 kc_row">--}} {{--
        <div class="container">--}} {{--
            <div class="row">--}} {{--
                <div class="element-block-heading-Trending">--}} {{--
                    <h2 class="heading-block-Trending">Most Popular Venues</h2>--}} {{--
                    <h4 class="sub-heading-block-Trending">Explore some of the best tips from around--}}
          {{--the world from our partners and friends.</h4>--}} {{--
                </div>--}} {{--
                <div class="">--}} {{--
                    <div class="most-popular-venue-width-box">--}} {{--@foreach($featuredVenues->slice(0,8) as $featuredVenue)--}} {{--
                        <div class="col-md-12">--}} {{--
                            <div class="block-view">--}} {{--
                                <header>--}} {{--
                                    <div class="col-md-4 ">--}} {{--
                                        <div class="place-box-image-block">--}} {{--
                                            <a href="" class="place-box-image-inner">--}}
                        {{--@include('shared.venueblock', ['venue' => $featuredVenue])--}}
                    {{--</a>--}} {{--
                                        </div>--}} {{--
                                    </div>--}} {{--
                                </header>--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--@endforeach--}} {{--
                    </div>--}} {{--
                </div>--}} {{--
            </div>--}} {{--
        </div>--}} {{--
    </section>--}}
    <div>
        <section class="kc-elm kc-css-183558 kc_row">
            <div class="kc-row-container  kc-container ">
                <div class="kc-wrap-columns">
                    <div class="kc_col-sm-12 kc_column kc_col-sm-12">
                        <div class="kc-col-container">
                            <div class="element-block-heading-Trending style-v3">
                                <div class="inner">
                                    <h2 class="heading-block-Trending">Most Popular Venues</h2>
                                    <h4 class="sub-heading-block-Trending">Explore some of the best tips from around. the world from our partners and friends.</h4>
                                </div>
                            </div>

                            {{-- @foreach($featuredVenues->slice(0,6) as $featuredVenue)
                            <div class=" col-md-3">
                                <div class="block-view">
                                    <header>
                                        <div class="place-box-image-block">
                                            <a href="" class="place-box-image-inner">
                                                <div class="">
                                                    @include('shared.venueblock', ['venue' => $featuredVenue])
                                                </div>
                                            </a>
                                        </div>
                                    </header>
                                </div>
                            </div>
                            @endforeach --}}
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
    <section class="kc-elm kc-css-183558 kc_row">
        <div class="kc-row-container  kc-container">
            <div class="kc-wrap-columns">
                <div class="kc-elm kc-css-159534 kc_col-sm-12 kc_column kc_col-sm-12">
                    <div class="kc-col-container">
                        <div class="element-block-heading-Trending style-v3">
                            <div class="inner">
                                <h2 class="heading-block-Trending">Featured Venues</h2>
                                <h4 class="sub-heading-block-Trending">See and visit interesting places. Share experiences
                          with your friends. Simply.</h4>
                            </div>
                        </div>

                        <div class="widget widget_opallisting_places_widget kc-elm kc-css-423461">
                            @foreach($featuredVenues as $featuredVenue)
                            <div class="item col-md-4">
                                <article itemscope="itemscope" itemtype="http://schema.org/Place" class="place-grid place-grid-v1">
                                    <header>
                                        <div class="place-box-image">
                                            <a href="{{ route('venue.show',['id' => $featuredVenue->id]) }}" class="place-box-image-inner">
                                                <img width="495" height="500" src="http://venusdemo.com/wordpress/rentme/wp-content/uploads/2017/04/place-50-495x500.jpg" class="attachment-rentme_place_thumbnail size-rentme_place_thumbnail wp-post-image" alt="" /> </a>
                                        </div>
                                        <div class="place-actions">
                                            <a href="#" class="author-link"><img alt='' src='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png' srcset='http://venusdemo.com/wordpress/rentme/wp-content/uploads/2014/06/avatar.png 2x' class='avatar avatar-96 photo' height='96' width='96' /><span>admin</span></a>
                                            <a href="#" data-icon="heart" data-id="11489" data-nonce="ee5a0830da" class="favorite  opallisting-need-login">
                                                <i class="far fa-heart"></i>
                                            </a>
                                        </div>
                                    </header>
                                    <div class="place-content">
                                        <h4 class="place-title"><a href="{{ route('venue.show',['id' => $featuredVenue->id]) }}" rel="bookmark"></a>{{$featuredVenue->name}}</h4>
                                        <div class="place-address">
                                            <i class="fa fa-map-marker"></i> {{$featuredVenue->venueAddress['area']}}, {{$featuredVenue->venueAddress['city']}}, {{$featuredVenue->venueAddress['state']}}</div>

                                        <div class="place-type">
                                            <span class="fa fa-users "></span>
                                            <span>Things to do</span>
                                        </div>
                                        <div class="place-meta">

                                            <div class="comment-rating-wrap">
                                                <div class="comment-rating" title="Rated 0 Star for listing">
                                                    <span style="width:80%">
                                                        @php
                                                            $avint = intval($featuredVenue->avgRating);
                                                            $avdec1 = $avdec2 = abs($featuredVenue->avgRating - $avint);
                                                        @endphp
                                                        @for( $i = 0; $i<5; $i++ )
                                                            @if($i < $avint)
                                                                <i class="fas fa-star"></i>
                                                            @elseif($avdec1 > 0)
                                                                <i class="fas fa-star-half-alt"></i>
                                                                @php 
                                                                $avdec1 = 0;
                                                                @endphp  
                                                            @else
                                                                <i class="far fa-star"></i>
                                                            @endif
                                                        @endfor
                                                    </span>
                                                </div>
                                                <div class="listing-count-reviews">({{ count($featuredVenue->reviews) }} Reviews)</div>
                                            </div>
                                            <span class="current-status  opening">
                                            Opening</span> </div>
                                    </div>
                                    <!-- .place-content -->

                                    <meta itemprop="url" content="http://venusdemo.com/wordpress/rentme/places/royal-ontario-museum/" />

                                </article>
                                <!-- #post-## -->
                            </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

<section class="kc-elm kc-css-736902 kc_row">
    <div class="kc-row-container  kc-container">
        <div class="kc-wrap-columns">
            <div class="kc-elm kc-css-162721 kc_col-sm-12 kc_column kc_col-sm-12">
                <div class="kc-col-container">
                    <div class="element-block-heading style-v3">
                        <div class="inner">
                            <h2 class="heading">Trending Categories</h2>

                        </div>
                    </div>
                    <div class="trending-categories">
                      @foreach($occasions as $occasion)
                        <div class="item">
                            <a href="#">
                                <img class="img-responsive trending-image" width="100%" src="{{ asset('uploads/occasion/images/'.$occasion->image)}}" alt="{{ $occasion->name }}"/>
                            </a>
                            <h4 class="text-center owl-carousel-item-title">
                                <a href="#">{{ $occasion->name }}</a>
                            </h4>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
    {{--======================Popular Cities======================--}}

    <section class="latestActivity">
        <div class="col-md-12 col-xs-12 noPadding ActivityWrapper innerContainer">
            <div class="col-lg-8 col-lg-offset-2 col-md-12 col-xs-12 form-group">
                <div class="city">
                    <div class="row text-center">
                        <div class="element-block-heading-Trending">
                            <h2 class="heading-block-Trending">Popular Cities</h2>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 noPadding FlexWrapper" data-sr-id="4">
                    <div class="col-md-6 col-sm-4 col-xs-12 activityHolder  ActivityImagePad xsNoPadding">

                        <div class="col-xs-12 noPadding BoxShadow FullHeight effect4 bottomtop ">
                            <a href="{!! route('search.search', ['District' => 'Delhi',])!!}">
                                <img class="FullWidth FullAbsoluteImage" src="../public/images/delhi.jpeg" title="Venues in Delhi">
                                <div class="ActivityLocation " style="font-size: 34px !important;font-weight: bold !important;">Delhi</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12 activityHolder  ActivityImagePad xsNoPadding">
                        <div class="col-xs-12 noPadding BoxShadow FullHeight effect4 bottomtop">
                            <a href="{!! route('search.search', ['District' => 'Jaipur'])!!}">
                                <img class="FullWidth FullAbsoluteImage" src="{{url('/images/photo/Amer-fort.jpg')}}" title="Venues in Jaipur">
                                <div class="ActivityLocation MBHeader2" style="font-size: 34px !important;font-weight: bold !important;">Jaipur</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12 noPadding activityHolder ActivityImagePad xsNoPadding ">
                        <div class="col-xs-12 noPadding BoxShadow FullHeight effect4 bottomtop" style="height:223px;">
                            <a href="{!! route('search.search', ['District' => 'Lucknow'])!!}">
                                <img class="FullWidth FullAbsoluteImage" src="{{url('/images/photo/lucknow1.jpg')}}" title="Venues in Lucknow">
                                <div class="ActivityLocation MBHeader2" style="font-size: 34px !important;font-weight: bold !important;">Lucknow</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12 noPadding ActivityWrapper innerContainer" style="padding-bottom: 5% !important;">
            <div class="col-lg-8 col-lg-offset-2 col-md-12 col-xs-12 form-group">
                <div class="col-md-6 col-md-offset-5 col-xs-12">
                </div>
                <div class="col-xs-12 noPadding FlexWrapper" data-sr-id="5" style="visibility: visible;  -webkit-transform: translateY(0) scale(1); opacity: 1;transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 0.5s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 0.5s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; transition: transform 0.5s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 0.5s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; ">
                    <div class="col-md-3 col-sm-4 col-xs-12 activityHolder  ActivityImagePad xsNoPadding
                 city-container">
                        <div class="col-xs-12 noPadding BoxShadow FullHeight effect4 bottomtop " style="height:223px;">

                            <a href="{!! route('search.search', ['District' => 'Varanasi'])!!}">
                                <img class="FullWidth FullAbsoluteImage" src="{{url('/images/photo/varanasi1.jpg')}}" title="meeting rooms in Varanasi">
                                <div class="ActivityLocation MBHeader2 " style="font-size: 34px !important;font-weight: bold !important;">Varanasi</div>
                            </a>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12 activityHolder  ActivityImagePad xsNoPadding">
                        <div class="col-xs-12 noPadding BoxShadow FullHeight effect4 bottomtop ">
                            <a href="{!! route('search.search', ['District' => 'Kanpur'])!!}">
                                <img class="FullWidth FullAbsoluteImage  " src="{{url('/images/photo/kanpur-city.jpg')}}" title="meeting rooms in Kanpur">

                                <div class="ActivityLocation MBHeader2" style="font-size: 34px !important;font-weight: bold !important;">Kanpur</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-4 col-xs-12 hello">
                        <a href="{!! route('search.search', ['District' => 'Mumbai'])!!}">
                            <div class="col-xs-12 noPadding BoxShadow FullHeight effect4 bottomtop ">
                                <img class=" FullWidth FullAbsoluteImage" src="{{url('/images/photo/Victoria_Terminus,_Mumbai.jpg')}}" title="meeting rooms in Mumbai">
                                <div class="ActivityLocation MBHeader2" style="font-size: 34px !important;font-weight: bold !important;">Mumbai</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--======================Popular Cities ended======================--}}

    <section class="kc-elm kc-css-476976 kc_row">
        <div class="kc-row-container  kc-container">
            <div class="kc-wrap-columns">
                <div class="kc-elm kc-css-360221 kc_col-sm-3 kc_column kc_col-sm-3">
                    <div class="kc-col-container">
                        <div class="kc-elm kc-css-752227 kc_counter_box">
                            <i class="fa fa-home element-icon"></i> <span class="counterup">983</span>
                            <h4>Listing PLaces</h4></div>
                    </div>
                </div>
                <div class="kc-elm kc-css-856406 kc_col-sm-3 kc_column kc_col-sm-3">
                    <div class="kc-col-container">
                        <div class="kc-elm kc-css-21161 kc_counter_box">
                            <i class="fas fa-mobile-alt"></i> <span class="counterup">701</span>
                            <h4>Desks</h4></div>
                    </div>
                </div>

                <div class="kc-elm kc-css-856406 kc_col-sm-3 kc_column kc_col-sm-3">
                    <div class="kc-col-container">
                        <div class="kc-elm kc-css-21161 kc_counter_box">
                            <i class="fas fa-chart-bar"></i></i> <span class="counterup">286</span>
                            <h4>Desks</h4></div>
                    </div>
                </div>
                {{--
                <div class="kc-elm kc-css-873734 kc_col-sm-3 kc_column kc_col-sm-3">--}} {{--
                    <div class="kc-col-container">--}} {{--
                        <div class="kc-elm kc-css-1178457 kc_counter_box">--}} {{--
                            <i class="fa fa-bar-chart element-icon"></i> <span class="counterup">286</span>--}} {{--
                            <h4>Companies</h4></div>--}} {{--
                    </div>--}} {{--
                </div>--}}

                <div class="kc-elm kc-css-19773 kc_col-sm-3 kc_column kc_col-sm-3">
                    <div class="kc-col-container">
                        <div class="kc-elm kc-css-531757 kc_counter_box">
                            <i class="fas fa-users"></i> <span class="counterup">1356</span>
                            <h4>Members</h4></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ended Trending Categories -->
    <section class="kc-elm kc-css-440330 kc_row">
        <div class="kc-row-container  kc-container">
            <div class="kc-wrap-columns">
                <div class="kc-elm kc-css-667430 kc_col-sm-12 kc_column kc_col-sm-12">
                    <div class="kc-col-container">
                        <div class="element-block-heading-Trending">
                            <div class="inner">
                                <h2 class="heading-block-Trending heding-color-block">Quick and Easy Search</h2>

                                <h4 class="sub-heading-block-Trending">See and visit interesting places. Share experiences with your friends. Simply.</h4>

                            </div>
                        </div>
                        <div class="kc-elm kc-css-471623 kc_row kc_row_inner">
                            <div class="kc-elm kc-css-567221 kc_col-sm-4 kc_column_inner kc_col-sm-4">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="feature-box feature-box-v3 ">
                                        <div class="fbox-icon">
                                            <i class="fa fa-bars" style=""></i> </div>
                                        <div class="fbox-content">
                                            <div class="fbox-body">
                                                <h4>Choose a Category</h4>

                                            </div>
                                            <p class="description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kc-elm kc-css-171656 kc_col-sm-4 kc_column_inner kc_col-sm-4">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="feature-box feature-box-v3 style-box">
                                        <div class="fbox-icon">
                                            <i class="fas fa-search-location"></i> </div>
                                        <div class="fbox-content">
                                            <div class="fbox-body">
                                                <h4>Find Locations</h4>

                                            </div>
                                            <p class="description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kc-elm kc-css-351067 kc_col-sm-4 kc_column_inner kc_col-sm-4">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="feature-box feature-box-v3 style-boxed">
                                        <div class="fbox-icon">
                                            <i class="fab fa-empire"></i></i> </div>
                                        <div class="fbox-content">
                                            <div class="fbox-body">
                                                <h4>Go Have Fun</h4>
                                            </div>
                                            <p class="description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="kc-elm kc-css-372420 kc_row">
        <div class="container">
            <div class="kc-row-container  kc-container">
                <div class="kc-wrap-columns">
                    <div class="kc-elm kc-css-896340 kc_col-sm-12 kc_column kc_col-sm-12">
                        <div class="kc-col-container">
                            <div class="element-block-heading-Trending">
                                <div class="inner">
                                    <h2 class="heading-block-Trending">Latest news</h2>
                                    <h4 class="sub-heading-block-Trending">See and visit interesting places. Share experiences with your friends. Simply.</h4>
                                </div>
                            </div>
                            <div class="kc-elm kc-css-22160 kc_row kc_row_inner">
                                <div class="kc-elm kc-css-614539 kc_col-sm-12 kc_column_inner kc_col-sm-12">
                                    <div class="kc_wrapper kc-col-inner-container">
                                        <div class="blog-post">

                                            <div class="blog-layout kc-post-layout-1  ">

                                                <div class="blog-item col-lg-6 col-md-6 col-sm-6 col-xs-12  first">

                                                    <article id="post-4384" class="post-4384 post type-post status-publish format-image has-post-thumbnail hentry category-world tag-html tag-media tag-php tag-wordpress post_format-post-format-image">
                                                        <div class="pull-left post-image">
                                                            <img width="290" height="400" src="../public/images/blog-1-290x400.jpg" pngclass="attachment-thumbnail size-thumbnail wp-post-image" alt=""> </div>

                                                        <div class="post-info pull-right">
                                                            <div class="entry-content">
                                                                <header class="entry-header">
                                                                    <h3 class="entry-title"><a href="" rel="bookmark">Millennials &amp; Women Are Redefining Leadership</a></h3>
                                                                    <div class="entry-meta">
                                                                        <div class="entry-category pull-left">
                                                                            <a href="" rel="category tag">World</a> </div>
                                                                        <span class="meta-sep"></span>
                                                                        <span class="entry-date">
                                                                        <span>06 Aug 2015</span>
                                                                        </span>

                                                                    </div>
                                                                    <!-- .entry-meta -->
                                                                    <div class="entry-content">
                                                                        When it comes to the workplace, turns out female and millennial employees want the same thing... </div>
                                                                    <!-- .entry-content -->
                                                                    <div class="author">
                                                                        <span><img alt="" src="../public/images/avatar.png "srcset="../public/images/avatar.png 2x" class="avatar avatar-36 photo" height="36" width="36"></span> By admin </div>
                                                                </header>
                                                                <!-- .entry-header -->
                                                            </div>
                                                            <!-- .entry-content -->
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </article>
                                                    <!-- #post-## -->

                                                </div>

                                                <div class="blog-item col-lg-6 col-md-6 col-sm-6 col-xs-12 ">

                                                    <article id="post-3272" class="post-3272 post type-post status-publish format-image has-post-thumbnail hentry category-health tag-html tag-media tag-php tag-web-design post_format-post-format-image">
                                                        <div class="pull-left post-image">
                                                            <img width="290" height="400" src="../public/images/blog-2-290x400.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt=""> </div>
                                                        <div class="post-info pull-right">
                                                            <div class="entry-content">
                                                                <header class="entry-header">
                                                                    <h3 class="entry-title"><a href="" rel="bookmark">French theme park will use 50 drones for  a nighttime spectacle</a></h3>
                                                                    <div class="entry-meta">
                                                                        <div class="entry-category pull-left">
                                                                            <a href="" rel="category tag">Health</a> </div>
                                                                        <span class="meta-sep"></span>
                                                                        <span class="entry-date">
                                                                        <span>03 Sep 2014</span>
                                                                        </span>

                                                                    </div>
                                                                    <!-- .entry-meta -->
                                                                    <div class="entry-content">
                                                                        In this competing world, Free service seems to good to be true, but yes there are few companies who do... </div>
                                                                    <!-- .entry-content -->
                                                                    <div class="author">
                                                                        <span><img alt="" src="../public/images/avatar.png" srcset="../public/images/avatar.png 2x" class="avatar avatar-36 photo" height="36" width="36"></span> By admin </div>
                                                                </header>
                                                                <!-- .entry-header -->
                                                            </div>
                                                            <!-- .entry-content -->
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </article>
                                                    <!-- #post-## -->

                                                </div>
                                                <div class="blog-item col-lg-6 col-md-6 col-sm-6 col-xs-12  first">

                                                    <article id="post-3265" class="post-3265 post type-post status-publish format-image has-post-thumbnail hentry category-health post_format-post-format-image">
                                                        <div class="pull-left post-image">
                                                            <img width="290" height="400" src="../public/images/blog-3-290x400.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt=""> </div>
                                                        <div class="post-info pull-right">
                                                            <div class="entry-content">
                                                                <header class="entry-header">
                                                                    <h3 class="entry-title"><a href="" rel="bookmark">Zumper National Rent Report: April 2016</a></h3>
                                                                    <div class="entry-meta">
                                                                        <div class="entry-category pull-left">
                                                                            <a href="" rel="category tag">Health</a> </div>
                                                                        <span class="meta-sep"></span>
                                                                        <span class="entry-date">
                                                                            <span>03 Sep 2014</span>
                                                                        </span>

                                                                    </div>
                                                                    <!-- .entry-meta -->
                                                                    <div class="entry-content">
                                                                        Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris itae erat conuat auctor eu in ... </div>
                                                                    <!-- .entry-content -->
                                                                    <div class="author">
                                                                        <span><img alt="" src="../public/images/avatar.png" srcset="../public/images/avatar.png 2x" class="avatar avatar-36 photo" height="36" width="36"></span> By admin </div>
                                                                </header>
                                                                <!-- .entry-header -->
                                                            </div>
                                                            <!-- .entry-content -->
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </article>
                                                    <!-- #post-## -->

                                                </div>
                                                <div class="blog-item col-lg-6 col-md-6 col-sm-6 col-xs-12 ">

                                                    <article id="post-2629" class="post-2629 post type-post status-publish format-image has-post-thumbnail hentry category-sports post_format-post-format-image">
                                                        <div class="pull-left post-image">
                                                            <img width="290" height="400" src="../public/images/blog-4-290x400.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt=""> </div>
                                                        <div class="post-info pull-right">
                                                            <div class="entry-content">
                                                                <header class="entry-header">
                                                                    <h3 class="entry-title"><a href="" rel="bookmark">Macquarie takes a look at 5 emerging business</a></h3>
                                                                    <div class="entry-meta">
                                                                        <div class="entry-category pull-left">
                                                                            <a href="" rel="category tag">Sports</a> </div>
                                                                        <span class="meta-sep"></span>
                                                                        <span class="entry-date">
                                                                           <span>13 Aug 2014</span>
                                                                        </span>

                                                                    </div>
                                                                    <!-- .entry-meta -->
                                                                    <div class="entry-content">
                                                                        The world of business moves fast. And making sure you are 'up-to-date' or even 'ahead of the game' can be a job in itself. ... </div>
                                                                    <!-- .entry-content -->
                                                                    <div class="author">
                                                                        <span><img alt="" src="../public/images/avatar.png" srcset="../public/images/avatar.png 2x" class="avatar avatar-36 photo" height="36" width="36"></span> By admin </div>
                                                                </header>
                                                                <!-- .entry-header -->
                                                            </div>
                                                            <!-- .entry-content -->
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </article>
                                                    <!-- #post-## -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="kc-elm kc-css-203701 kc_row">
        <div class="kc-row-container  kc-container  container">
            <div class="kc-wrap-columns">
                <div class="kc-elm kc-css-456425 kc_col-sm-12 kc_column kc_col-sm-12">
                    <div class="kc-col-container">
                        <div id=" " class="kc-elm kc-css-529151 kc_row kc_row_inner">
                            <div class="kc-elm kc-css-291404 kc_col-sm-12 kc_column_inner kc_col-sm-12">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="kc-elm kc-css-542824" style="height: 90px; clear: both; width:100%;"></div>
                                </div>
                            </div>
                        </div>
                        <div id=" " class="kc-elm kc-css-934130 kc_row kc_row_inner">
                            <div class="kc-elm kc-css-602499 kc_col-sm-12 kc_column_inner kc_col-sm-12">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="kc-elm kc-css-921759 kc-title-wrap para-title">
                                        How To Book
                                        <h2 class="kc_title">India's Favourite Way To Find &amp; Compare Venuesamp</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id=" " class="kc-elm kc-css-675844 kc_row kc_row_inner">
                            <div class="kc-elm kc-css-223581 kc_col-sm-3 kc_column_inner kc_col-sm-3">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="kc-elm kc-css-984974 kc-icon-wrapper">
                                        <i class="et-chat"></i>
                                    </div>
                                    <div class="kc-elm kc-css-939060 kc_text_block">
                                        <h4 style="text-align: left;color: rgba(136, 212, 152, 1);">Discover & Shortlist</h4>
                                    </div>
                                    <div class="kc-elm kc-css-989659 kc_text_block para">
                                        <p>Browse venues and create your personalized list of shortlisted venues to send an enquiry</p>
                                    </div>
                                </div>
                            </div>
                            <div class="kc-elm kc-css-548807 kc_col-sm-3 kc_column_inner kc_col-sm-3">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="kc-elm kc-css-503273 kc-icon-wrapper">
                                        <i class="sl-user-follow"></i>
                                    </div>
                                    <div class="kc-elm kc-css-314572 kc_text_block">
                                        <h4 style="text-align: left;color: #FFCA00;">Get Quotes</h4>
                                    </div>
                                    <div class="kc-elm kc-css-128854 kc_text_block para">
                                        <p>Get negotiated rates for your shortlisted venues</p>
                                    </div>
                                </div>
                            </div>
                            <div class="kc-elm kc-css-690100 kc_col-sm-3 kc_column_inner kc_col-sm-3">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="kc-elm kc-css-722658 kc-icon-wrapper">
                                        <i class="et-ribbon"></i>
                                    </div>
                                    <div class="kc-elm kc-css-552594 kc_text_block">
                                        <h4 style="text-align: left;color: #47B8E0; ">Book a Venue</h4>
                                    </div>
                                    <div class="kc-elm kc-css-616578 kc_text_block para">
                                        <p>Select and Book the perfect venue in no time at all.Time is money, save both.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="kc-elm kc-css-133228 kc_col-sm-3 kc_column_inner kc_col-sm-3">
                                <div class="kc_wrapper kc-col-inner-container">
                                    <div class="kc-elm kc-css-722658 kc-icon-wrapper">
                                        <i class="sl-plane plane-icon"></i>
                                    </div>
                                    <div class="kc-elm kc-css-937415 kc_text_block">
                                        <h4 style="text-align: left; color: rgba(254, 95, 85, 1);">Give & Recieve Feedback</h4>
                                    </div>
                                    <div class="kc-elm kc-css-630467 kc_text_block para">
                                        <p> You'll get rewarded with credit for your next booking and become an active member of the VenueListing Community.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kc-elm kc-css-153231" style="height: 70px; clear: both; width:100%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-------------------renew------------>
    <section class="kc-elm background-image-block ">

        <div class=" kc_col-sm-12 kc_column kc_col-sm-12 mobileViewSlider testimonial-style">

            <div class="testimonial-collection testimonial-v2">

                <!-- Skin 1 -->
                <!-- Wrapper for slides -->

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    
                    <ol class="carousel-indicators">
                        @for($i=0; $i < $count; $i++)
                         @if( $i == 0)
                          <li data-target="#myCarousel" data-slide-to="{{ $i }}" class="active"></li>
                         @else
                          <li data-target="#myCarousel" data-slide-to="{{ $i }}"></li>
                         @endif
                        @endfor
                    </ol>                  
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        @foreach($testimonial as $value)
                        @if($loop->iteration - 1 == 0)
                        <div class="item active">
                            <div class="carousel-caption">
                            </div>
                            <div class="item">
                              <div class="testimonials-v2">
                                <div class="text-image-center-block">
                                    <div class="testimonials-avatar radius-x">
                                        <a href=""><img width="80" height="80" src="{{url('uploads/testimonial/images/'.$value->image)}}" class="img-circle" alt="" /></a>
                                    </div>
                                    <div class="testimonials-meta">

                                    <div class="testimonials-description">{{ $value->review }}</div>
                                    <h5 class="testimonials-name">{{ $value->reviewer_name }}</h5>
                                    <div class="job">{{ $value->reviewer_designation }}</div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        @else
                        <div class="carousel-caption">
                        </div>
                        <div class="item">
                            <div class="testimonials-v2">
                            <div class="text-image-center-block">
                                <div class="testimonials-avatar radius-x">
                                        <a href=""><img width="80" height="80" src="{{url('uploads/testimonial/images/'.$value->image)}}" class="img-circle" alt="" /></a>
                                    </div>
                                <div class="testimonials-meta">
                                    
                                    <div class="testimonials-description"> {{ $value->review }}</div>
                                    <h5 class="testimonials-name">{{ $value->reviewer_name }}</h5>
                                    <div class="job">{{ $value->reviewer_designation }}</div>
                                </div>
                            </div>
                        </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ ended======================-->
    
    <!--================ start======================-->
    <section class="">
        <div class="container">
        </div>

    </section>

    <!--***********ended***********-->
    </section>
    </div>

    <!-- #page -->
    <div id="tiptip_holder" style="max-width:200px;">
        <div id="tiptip_arrow">
            <div id="tiptip_arrow_inner"></div>
        </div>
        <div id="tiptip_content"></div>
    </div>
</body>
@endsection 
@section('scripts') 
<script type="text/javascript" src="{{ URL::asset('js/kingcomposer.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/rating.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjYIJDSpRo90YUDZNtLnSCTmuMHfLMAlo&libraries=places"></script>
<script type="text/javascript" src="{{ asset('js/jquery.address.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/maps_lib.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript">
   $(document).ready(function() {
    $(".js-example-basic-single").select2({
        placeholder: 'Select an option...',
         
          allowClear: true
    });
});
</script>
<script type="text/javascript" src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
<script>
    var owl = $('.owl-carousel');
    owl.owlCarousel({
            margin: 10,
            //loop: true,
            autoplay: true,
            smartSpeed: 500,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 3
                },
                992: {
                    items: 4
                },
                1200: {
                    items: 6
                }
            }

        })
     $('.trending-categories').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});
       
    // function initialize() {
    //     var address = (document.getElementById('pac-input'));
    //     var autocomplete = new google.maps.places.Autocomplete(address);
    //     autocomplete.setComponentRestrictions(
    //             {'country': 'in'});
    //     google.maps.event.addListener(autocomplete, 'place_changed', function() {
    //         var place = autocomplete.getPlace();
    //         var latitude = place.geometry.location.lat(); // latitude of location
    //         alert(latitude)
    //         $('#lat').val() = latitude;
    //         var longitude = place.geometry.location.lng(); // longitude of location
    //        $('#long').val() = latitude;
    //     });
    // }

    // google.maps.event.addDomListener(window, 'load', initialize);
    // //----------------location search in only india ended---------------
    $(function(){
        document.getElementById('search_address').value = "";
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('search_address'));
        autocomplete.setComponentRestrictions(
                    {'country': 'in'});
        google.maps.event.addListener(autocomplete, 'place_changed',function(){
             var place = autocomplete.getPlace();
             var address = place.address_components;
             document.getElementById('lat').value = "";
             document.getElementById('long').value = ""; 
             var latitude = place.geometry.location.lat(); // latitude of location
             document.getElementById('lat').value = latitude;
             var longitude = place.geometry.location.lng(); // longitude of location
             document.getElementById('long').value = longitude; 
        }); 
    });
     

    jQuery(document).ready(function($) {
        $('.counterup').counterUp({
            delay: 10,
            time: 1000
        });
    });
</script>

@endsection