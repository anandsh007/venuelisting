<div class="form-group required {{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Venue Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Venue Name']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group required{{ $errors->has('max_capacity') ? ' has-error' : ''}}">
    {!! Form::label('max_capacity', 'Maximum Capacity', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::number('max_capacity', old('max_capacity'), ['class' => 'form-control', 'required' => 'required','placeholder'=>'Maximum Capacity']) !!}
        {!! $errors->first('max_capacity', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6 required {{ $errors->has('mob_number') ? 'has-error' : ''}}">
        {!! Form::label('mob_number', 'Mobile Number', ['class' => 'col-md-4 form-align control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('mob_number', old('mob_number'), ['class' => 'form-control', 'required' => 'required','placeholder'=>'Mobile Number']) !!}
            {!! $errors->first('mob_number', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group col-md-5 {{ $errors->has('phone_number') ? 'has-error' : ''}}">
        {!! Form::label('phone_number', 'Phone Number', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-7">
            {!! Form::number('phone_number', old('phone_number'), ['class' => 'form-control', 'required' => 'required','placeholder'=>'Phone Number']) !!}
            {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('short_desc') ? 'has-error' : ''}}">
    {!! Form::label('short_desc', 'Short Description', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea('short_desc', old('short_desc'),['class' => 'form-control tinymce-editor-short-desc', 'required' => 'required','placeholder'=>'Short Description']) !!}
        {!! $errors->first('short_desc', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
<div class="row"></div>
<div class="form-group required {{ $errors->has('long_desc') ? 'has-error' : ''}}">
    {!! Form::label('long_desc', 'Long Description', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea('long_desc', old('long_desc'),['class' => 'form-control tinymce-editor-long-desc', 'required' => 'required','placeholder'=>'Long Description']) !!}
        {!! $errors->first('long_desc', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('Images') ? 'has-error' : ''}}">
    {!! Form::label('Images', 'Images(can add more than one)', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
            <!--upload form-->
        <label class="btn btn-default btn-file upload_button">
            <span>Add</span>
            <input type="file" name="files[]" multiple class="hide" id="fileupload" >
            <input type="hidden" name="image_name" id="file_ids" value="" />
        </label>
        <!--upload form-->

        <!--progress-->
        <div class="progress file_progress" style="margin-top: 10px; height: 5px;">
            <div class="progress-bar progress-bar-success" role="progressbar"
                 aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                 style="width: 0%;">
            </div>
        </div>
        <!--/progress-->

        <!--file uploaded-->
        <ul class="list-group uploaded_files" style="margin-top: 10px;">
        </ul>
        <!--/file uploaded-->
    </div>
</div>
<div class="form-group {{ $errors->has('venue_video') ? 'has-error' : ''}}">
    {!! Form::label('venue_video', 'Video', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('venue_video', old('venue_video'), ['class' => 'form-control maxwidthdropdown', 'required' => 'required','placeholder'=>'Input link for videos from Youtube.']) !!}
        {!! $errors->first('venue_video', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<h3><i class="fa fa-bars"></i>  Packages Option (At Least One)</h3>
<input type="button" class="btn btn-default" id="add" value="Add More">
<div class="row package-element">
    <div class="row element" id="div_1">
        <div class="row row-margin" id="package">
            <div class="form-group required col-md-5 {{ $errors->has('package_name') ? 'has-error' : ''}}">
            {!! Form::label('package_name', 'Package Name', ['class' => 'col-md-4 control-label form-align']) !!}
                <div class="col-md-7">
                    {!! Form::text('package_name[]', old('package_name'), ['class' => 'form-control ', 'required' => 'required','placeholder'=>'Package Name']) !!}
                    {!! $errors->first('package_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group required col-md-5 {{ $errors->has('cost_per_person') ? 'has-error' : ''}}">
                {!! Form::label('cost_per_person', 'Per Person Cost', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-5">
                    {!! Form::number('cost_per_person[]', old('cost_per_person'), ['class' => 'form-control', 'required' => 'required','placeholder'=>'Per Person Cost']) !!}
                    {!! $errors->first('cost_per_person', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>
<h3><i class="fas fa-search-plus"></i>  Choose Applicable Options</h3>
<div class="form-group {{ $errors->has('Categories') ? 'has-error' : ''}}">
    {!! Form::label('categories', 'Categories', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        @foreach ($venueCategories as $venueCategory)
        <div class="col-md-12">
            {!! Form::label ($venueCategory->name, $venueCategory->name,
            ['class' => 'labelcategory-inline']) !!}
            <div class="form-group">
                @foreach ($venueCategory->venueSubCategories as $venueSubCategory)
                <label>
                    <input type="checkbox" class="jquerycheckbox" value="{{$venueSubCategory->id}}"
                    name='venue_sub_categories[]'>
                    {{$venueSubCategory->name}}
                </label>
                @endforeach
            </div>
        </div>
        @endforeach
    </div>
</div>

<!-- ================= Google map ===========  -->
<h3><i class="fa fa-map-marker" aria-hidden="true"></i>  Address</h3>
<div class="form-group {{ $errors->has('address') ? ' has-error' : ''}}">
    {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea('address', old('address'),['class' => 'form-control textarea-style', 'required' => 'required','size' => '30x3','placeholder'=>'Address']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<h3><i class="fa fa-location-arrow" aria-hidden="true"></i>  Map Address</h3>
<div class="row">
    <div class="col-md-6">
        <p>
          <input class='form-control' id='search_address' placeholder='Enter an address or an intersection' type='text'/>
        </p>
        <p>
          <label>
            within
            <select id='search_radius'>
              <option value='400'>2 blocks</option>
              <option value='805'>1/2 mile</option>
              <option value='1610'>1 mile</option>
              <option value='3220'>2 miles</option>
            </select>
          </label>
          <a class='btn btn-default' id='reset' href='#'>
          <i class='glyphicon glyphicon-repeat'></i>
          Reset
        </a>
        </p>
        <div class="row">
             <div class="form-group col-md-6">
                {!! Form::label('state', 'State', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                    <input class="form-control" id="administrative_area_level_1" type="text" name="state" readonly value="{{old('state')}}">
                </div>
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('city', 'City', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                    <input class="form-control" id="locality" type="text" type="text" name="city" readonly value="{{old('city')}}">
                </div>
            </div>          
        </div>
        <div class="row"> 
            <div class="form-group col-md-6">
                {!! Form::label('area', 'Area', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                    <input class="form-control" id="sublocality_level_1" type="text" name="area" readonly value="{{old('area')}}">
                     <select class="form-control" id="PincodesId" name="Area"></select>
                </div>
            </div>       
            <div class="form-group col-md-6">
                {!! Form::label('street_number', 'Street No.', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                    <input class="form-control" id="street_number" type="text" name="street_number" value="{{old('street_number')}}">
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('postal_code', 'Pin Code', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                    <input class="form-control" id="postal_code" type="text" name="postal_code" readonly value="{{old('postal_code')}}">
                </div>
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('country', 'Country', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                    <input class="form-control" id="country" type="text" name="country" readonly value="{{old('country')}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('latitude', 'Latitude', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                    <input class='form-control' id='lat' type="text" name="latitude" readonly value="{{old('latitude')}}">
                </div>
            </div>
            <div class="form-group col-md-6">           
                {!! Form::label('longitude', 'Longitude', ['class' => 'col-md-6 control-label']) !!}
                <div class="col-md-6">
                    <input class='form-control' id='long' type="text" name="longitude" readonly value="{{old('longitude')}}">
                </div> 
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="map-address" id='map_canvas'></div>      
    </div>             
</div> 

<!-- ================= Google map ===========  -->

<h3><span class="fa fa-building"></span>  Occasion It can Host</h3>

<div class="form-group {{ $errors->has('Categories') ? 'has-error' : ''}}">
    {!! Form::label('Categories', 'Categories', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        @foreach ($occasions as $occasion)
        <div class="col-md-12">
            {!! Form::label($occasion->name, $occasion->name,
            ['class' => '']) !!}
            <div class="form-group">
                @foreach ($occasion->occasions as $occasionSubCategory)
                <label >
                    <input type="checkbox" class="jquerycheckbox" value="{{$occasionSubCategory->id}}" name='occasions[]'>
                    {{$occasionSubCategory->name}}
                </label>
                @endforeach
            </div>
        </div>
        @endforeach
    </div>
</div>
<h3><i class="fas fa-wine-glass"></i>  Choose Venue Features </h3>
<div class="col-md-row">
    <div class="form-group {{ $errors->has('venue_features') ? 'has-error' : ''}}">
        {!! Form::label('venue_features', 'Venue Features', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-8">
            @foreach($venueFeatures as $venueFeature)
            <label >
                <input type="checkbox" class="jquerycheckbox" value="{{$venueFeature->id}}" name='venue_features[]'>
                {{$venueFeature->name}}
            </label>
            @endforeach
        </div>
    </div>
</div>
<h3><i class="fas fa-tags"></i>  Choose Tags</h3>
<div class="col-md-row">
    <div class="form-group {{ $errors->has('tags') ? 'has-error' : ''}}">
        {!! Form::label('tags', 'Tags', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-8">
            @foreach($tags as $tag)
            <label >
                <input type="checkbox" class="jquerycheckbox" value="{{$tag->id}}" name='tags[]'>
                {{$tag->name}}
            </label>
            @endforeach
        </div>
    </div>
</div>
<h3><i class="fas fa-credit-card"></i>  Choose Payment Option</h3>
<div class="col-md-row">
    <div class="form-group {{ $errors->has('PaymentOption') ? 'has-error' : ''}}">
        {!! Form::label('payment_option', 'Payment Options', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-8">
            @foreach($paymentOptions as $paymentOption)
            <label >
                <input type="checkbox" class="jquerycheckbox" value="{{$paymentOption->id}}" name='payment_option[]'>
                {{$paymentOption->name}}
            </label>
            @endforeach
        </div>
    </div>
</div>
<h3><span class="fa fa-star"></span>  Choose Ameneties</h3>
<div class="col-md-row">
    <div class="form-group {{ $errors->has('VenueFacility') ? 'has-error' : ''}}">
        {!! Form::label('amenity', 'Amenities', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-8">
            @foreach($venueAmenities as $venueAmenity)
            <label >
                <input type="checkbox" class="jquerycheckbox" value="{{$venueAmenity->id}}" name='amenities[]'>
                {{$venueAmenity->name}}
            </label>
            @endforeach
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create',
        ['class' => 'btn btn-primarycreate  btn-default']) !!}
    </div>
</div>