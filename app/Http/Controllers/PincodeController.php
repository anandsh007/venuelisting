<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Pincode;

class PincodeController extends Controller
{

    public function getPincodes(Request $request)
	{   
	    $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $pincodes = Pincode::where('office_name','like','%'.$term.'%')->limit(5)->get();

        $formatted_pincodes = [];

        foreach ($pincodes as $pincode) {
            $formatted_pincodes[] = ['id' => $pincode->id, 'text' => $pincode->office_name];
        }

        return \Response::json($formatted_pincodes);
    }
}